# 1NSI : Utilisateurs Linux

## Utilisateurs usuels

Linux est un système multi-utilisateurs: Cela signifie que plusieurs personnes peuvent travailler simultanément sur la même machine, en s'y connectant à distance notamment. Il existe donc tout un ensemble de règles / **permissions/droits** **autorisant, ou pas,** un utilisateur à accéder à des fichiers / dossiers, par exemple :
* à ses propres documents
* à ceux d'autres utilisateurs
* à des programmes personnels non partagés
* à des programmes multi-utilisateurs partagés
* etc...

Par sécurité, Un utilisateur *usuel* sur Linux **ne dispose PAS de tous les droits sur le système**, mais seulement des droits usuels sur la manipulation de ses propres fichiers et dossiers personnels (notamment dans le répertoire /home). Il n'est pas administrateur du système.

Il est à noter que **c'est le contraire que des systèmes comme Windows, qui par défaut, donne des droits Administrateur à l'utilisateur usuel.** 

## le Super Utilisateur / Super User / Root

Outre tous les utilisateurs usuels, il existe **un utilisateur spécial**, nommé **super-utilisateur** ou **super-user**, ou **Administrateur**, ou **root** (le plus utilisé) en anglais, disposant de tous les droits sur le système.

### sudo : Exécuter UNE commande en tant que root dans un Terminal

La commande **sudo** (Super-User DO) permet d'exécuter une commande *macommande* en mode **root** dans un Terminal:
```bash
$ sudo macommande
```

>**Notation \$ :** Le ```$```, en début de ligne, signifie que c'est l'Utilisateur courant (connecté à Linux) qui a la main dans le Terminal

### su : Devenir root dans un Terminal

De manière équivalente, On peut également:
* devenir root dans le Terminal grâce à la commande ```su``` (SuperUser)
* puis, après avoir tapé le mot de passe de root, Il apparaît alors un ```#``` en début de ligne suivante
>**Notation # :** Le ```#```, en début de ligne, (qui remplace le ```$```) signifie que l'on est devenu root dans le Terminal
* on peut dès lors taper la commande ```macommande``` (par exemple)

```bash
$ su
Mot de Passe:
#
# macommande
```

### su : Redevenir l'Utilisateur courant dans un Terminal

La commande suivante permet de redevenir l'Utilisateur courant dans le même Terminal. On remplacera (nomUtilisateurCourant) par le nom de l'utilisateur courant de votre système Linux (au Lycée: l'utilisateur courant est **eleve**).

```bash
# su (nomUtilisateurCourant)
$
```