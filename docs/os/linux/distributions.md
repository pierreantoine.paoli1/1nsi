# 1NSI : Distributions Linux

## Qu'est-ce qu'une Distribution ?

Le Système d'Exploitation GNU/Linux (noyau Linux + les logiciels du projet GNU), complété par de nombreux autres logiciels libres, forme ce que l'on appelle une **Distribution Linux**. Il en existe de nombreuses.

### Quelques <b>Ressemblances</b> générales entre Distributions

* le **noyau Linux** (mais certaines proposent d'autres noyaux)
* les logiciels du **projet GNU** contenant notamment de nombeuses commandes de base (ls, grep, ..)
* un **serveur X** qui permet l'affichage graphique: généralement **X11**, mais il en existe d'autres comme  **wayland**
* Un **système de gestion de logiciels** appelés **paquets/packages** (en ligne de commande et/ou graphique) : notamment pour l'installation/désinstallation de paquets dont le code source (souvent précompilé, mais pas obligatoirmenet) est stocké sur des serveurs officiels nommés **dépôts** ou **repositories**, distincts pour chaque distribution
* une ou plusieurs interfaces graphiques, appelées **Environnement de Bureau** qui différencient le style et fonctionnalités graphiques:
  * **[XFCE](https://www.xfce.org/)**: Très simple et léger. Parfait pour les ordis anciens.
  * **[Gnome](https://www.gnome.org/)**
  * **[KDE](https://kde.org/)**, etc...
* une **suite de logiciels OpenSource** pour l'utilisateur moyen (LibreOffice, Gimp, etc..) et/ou très spécialisés (en cas d'une distribution spécialisée)
* la **gratuité** (sauf RedHat et Mandriva)

Certaines Distributions donnent naissance à des **distributions filles** qui héritent, la plupart du temps, de la compatibilité des paquets/logiciels et des tutoriels de la distribution mère.

### Quelques <b>Différences</b> entre les distributions peuvent exister

* Des **communautés** différentes de développeurs :
  * nombre très variable de développeurs (d'un seul *dev*, à des millions de *devs*)
  * des situations et rémunérations variables
  * plus ou moins ouvertes aux *newbies*
* des **modes de distributions** différents :
  * ou bien distribués gratuitement par la communauté de développeurs, **à but non lucratif**
  * ou bien, par une **société commerciale** avec un modèle économique (formation et/ou développement de logiciels)
* des **gestionnaires de paquets/packages distincts**: **apt**, **pacman**, **rpm**, etc.. En outre, chaque distribution peut avoir son propre format pour les paquets (précompilés, ou pas): **.rpm**, **.deb**, **.xz**, etc... non compatibles entre eux.
* mode de développement en **Rolling Release** (développement continu) vs en **système de versions**.
* **Environnements de Bureau** pouvant être un peu différents selon les distributions:
  * pas tous les Environnements de Bureau ne sont forcément disponibles pour une même distribution
  * Environnement de Bureau spécifique à une distribution (e.g. Cinnamon pour Mint) (du moins à un moment précis)
* **Fréquence des mises à jour** variables, ainsi que **simplicité** variable de la mise à jour générale du système, pouvant mener à une **stabilité** et une ***maintainabilité*** variables également
* Chaque paquet/logiciel dans les bases de données des dépôts est géré par un/des *mainteneur.s* de la communauté. Donc en pratique: **des bases de données de paquets/logiciels très différentes, en nombre et en qualité.**
* des **tutoriels/wikis** : en nombre et en qualité très variables.
* Certaines distributions, mais pas toutes, autorisent des paquets propriétaires sur leurs dépôts officiels, ou pas (drivers, gratuiciels/freewares, etc...)
* Certaines distributions sont très spécialisés (serveurs, sécurité, etc..), d'autres sont plus généralistes, pour le grand public


## Quelques Distributions Usuelles (CULTURE)

### ArchLinux ($\ge2002$)

Licence GNU GPL et autres. Il s'agit d'une application **en Développement/Publication Continu.e**, ou **Rolling Release**, par opposition au système de versions. Met l'accent sur la simplicité et la légèreté (principe KISS). L'installation n'est pas la plus simple pour un débutant.

**Distributions Dérivées**

* <env>**Manjaro Linux**</env> ($\ge2012$) Licences multiples (principalement GPL. Réseau OIN - Open Invention Network - ayant convenu d'un pacte de non-agression avec Linux et l'OpenSource). Il s'agit d'une **Rolling Release** également. Installation très simple. Pensée pour être conviviale pour un utilisateur débutant. Livrée avec un support multimédia clés en main, un système robuste de reconnaissance de matériel (MHWD - Manjaro Hardaware Detection) et le soutien de noyaux multiples. 2ème distribution en 2019 sur le site [Distrowatch](https://distrowatch.com).

### Debian ($\ge1993$)

Licences multiples. Distribution non commerciale et mode de gouvernance coopératif de l'association gérant la distribution. Régie par le [Contrat Social Debian](https://fr.wikipedia.org/wiki/Contrat_social_Debian). Caractérisée par le grand nombre d'architectures soutenues (même **Raspbian** sur **Raspberry** vient de là), par son cycle de développement relativement long, entraînant la stabilité des versions. Orientée serveur.

**Distributions Dérivées**

* <env>**Ubuntu**</env> ($\ge2004$) Licences Multiples (principalement GPL). Distribution commerciale (support commercial & technique, certifications Ubuntu, fournis par la société **Canonical**. Cours en présenciel et en distanciel). La plus utilisée sur les systèmes Cloud et les serveurs informatiques.
  * <env>**Linux Mint**</env> ($\ge2006$) Licence GNU GPL. Installation Simple et rapide. Développe sa propre interface (Environnement de Bureau **Cinnamon**). 2ème distribution sur Distrowatch.
* <env>**MX Linux**</env> ($\ge2014$) Licence Open Source. L'objectif déclaré est de "combiner un bureau élégant et efficace avec une configuration simple (davantage **User friendly** que Debian), une stabilité élevée, des performances solides et un encombrement moyen". Reste compatible avec des systèmes 32 bits. 1ère Distribution en 2019 sur Distrowatch.
* <env>**Kali Linux**</env> ($\ge2013$) Licence GNU GPL. Distribution retroupant l'ensemble des outils nécessaires aux **tests de sécurité**, notamment le **test d'intrusion**.
* <env>**Tails**</env> ($\ge2009$) (**T**he **A**mnesic **I**ncognito **L**ive **S**ystem): Licence GPLV3+. Distribution **portable** basée sur la **sécurité**, dont le but est préserver la **vie privée** et l'**anonymat** de l'utilisateur, et censée aider contre la censure, utilisant le **réseau TOR** de manière native. Développé (en partie) par l'équipe du projet TOR. Conçu pour être démarré à partir d'un **live CD** ou d'un **live USB** et ne pas laisser de trace numérique sur la machine, à moins qu'il soit explicitement autorisé à le faire. Connue pour être la bête noire de la NSA (article du journal allemand *Der Spiegel*, 2014) dont le programme de surveillance **XKeyScore** ciblait les personnes recherchant des informations sur Tails à l'aide d'un moteur de recherche.

### Gentoo Linux ($\ge2002$)

Licence GNU GPL. Conçue pour être modulaire, portable et optimisée pour le matériel de l'utilisateur. Sa particularité est la compilation automatique complète (ou partielle) de GNU/Linux à partir des sources, en tenant compte des optimisations possibles du jeu d'instructions du processeur. La gestion des paquets s'inspire des ports de BSD.

### RedHat Linux ($\ge 1994$)

Société multinationale d'origne américaine, filiale d'IBM, connue principalement pour son produit **RHEL (RedHat Enterprise Linux)**, basé sur un OS Open Source, mais destiné aux Entreprises. Licences multiples.

**Distributions Dérivées**

* RedHat sponsorise <env>**Fedora Linux**</env> ($\ge2003$) après Red Hat Linux 9 (RHL9) en 2003. Distribution Open Source GNU/Linux Communautaire, conçue pour être un laboratoire où développer de nouvelles fonctionnalités, qui seront plus tard incluses dans la distribution commerciale de Red Hat.
* <env>**CentOS** --> **Rocky Linux**</env> (CentOS $\ge 2004$) **CentOS** est/était une distribution principalement destinée aux serveurs, mais est en train de mourir (les investissements de RedHat se font exclusivement sur CentOS Stream -non utilisable en production-, et non plus sur CentOS Linux, depuis le 31 Décembre 2020). D'où la naissance du Projet <env>**[Rocky Linux](https://github.com/rocky-linux/rocky)**</env>, amené à le remplacer, dont l'objectif est de construire "*un système d'exploitation d'entreprise communautaire conçu pour être compatible à 100% avec Red Hat Enterprise Linux (RHEL)*"

### Slackware Linux ($\ge1993$)

Licence GNU GPL. Une des plus anciennes distributions encore maintenues. Longtemps maintenue par une seule personne. Procédure d'installation semi-graphique. Système de paquetages composé simplement d'archives **tar** (**Tape ARchiver** ou **tarballs**, $\ge1988$) sans gestion des dépendances. Processus de démarrage basée sur des scripts aisément modifiables (appréciée sur les serveurs). Pas réservée aux débutants.

<env>**Distributions Dérivées**</env>

  * **SUSE Linux ($\ge1992$)**: Société Allemande et aussi Distribution Linux, appartenant au fonds d'investissement EQT Partners. Connue pour sa distribution **SUSE Linux Entreprise** initialement basée sur Slackware. Modèle économique basé sur l'offre de services et le développement logiciel (chiffre d'affaires \$300Mds en 2017). C'est La plus ancienne distribution commerciale encore existante.

Aller plus loin : **[Liste Complète des Distributions](https://fr.wikipedia.org/wiki/Distribution_Linux)**

## Popularité/Part de marché des Distributions (CULTURE)

Certains sites comme **[Distrowatch](https://distrowatch.com)** sont connus pour, et destinés à l'actualité des distributions Linux. Distrowatch recense les distributions *les plus populaires* dans un sens à préciser (=nombre d'affichages de la page correspondant à la distribution sur Distrowatch durant les derniers mois/année, puis les classifie), méthode forcément polémique et très variable sur d'autres sites de ce genre. Ce premier classement de Distrowatch ne valide donc:
* ni la qualité des distributions, 
* ni réellement l'appréciation de chaque distribution, 
* ni les parts des marché de chacune des distributions. 

Notez aussi que Distrowatch lui-même, offre également aux visiteurs, comme deuxième méthode alternative de comparaison, de voter pour leur distribution préférée: [Résultats du Vote](https://distrowatch.com/dwres.php?resource=ranking).  
D'autres sites comme **Alexa** offrent leur propre classement des meilleures distributions, basé sur les statistiques des 100000 premiers sites (méthode également polémique forcément): [Résultats sur cette page](https://fr.wikipedia.org/wiki/Linux#Principales_distributions_Linux)