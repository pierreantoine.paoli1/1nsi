# 1NSI : Réprésentation des Nombres en Base $2$ (Binaire)

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/LnDKfkPhuM0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Rappel d'Écriture des Nombres Entiers en Base 10 (Décimal)

!!! pte
    En base 10, il y exactement 10 <bred>chiffres</bred>: $0$, $1$, $2$, $3$, $4$, $5$, $6$, $7$, $8$, $9$

Considérons un nombre entier, écrit en base 10 (en décimal), par exemple $2743$ :  

On a les transformations d'écriture suivantes : 

$$2743 = 2000 + 700 + 40 + 3$$

$$2743 = 2\times1000 + 7\times100 + 4\times10 + 3\times1$$

$$2743 = 2\times10^3 + 7\times10^2 + 4\times10^1 + 3\times10^0$$

!!! note "Résumé"
    Pour écrire un nombre entier sous forme d'une addition de puissances de $10$:  

    * Au dessus de chaque chiffre, **de droite à gauche**, on écrit des puissances croissantes de $10$ :

    $$10^3\,\,10^2\,\,10^1\,\,10^0$$
    
    $$ 2 \,\,\,\,\,\,\,\, 7 \,\,\,\,\,\,\,\, 4 \,\,\,\,\,\,\,3$$

    * On multiplie chaque chiffre par la puissance de $10$ correspondante :

    $$2743 = 2\times10^3 + 7\times10^2 + 4\times10^1 + 3\times10^0$$

## Bits et Mots machine

Dans un ordinateur, toutes les informations (données ou programmes) sont représentées grâce à des 0 et des 1.

!!! def
    * Un <bred>bit</bred> (pour <bred>BI</bred>nary digi<bred>T</bred>) est un $0$ ou bien un $1$
    * Un ensemble de $8$ bits est appelé un <bred>octet</bred> :fr: ou un <bred>byte</bred> :gb:
    * Un <bred>mot machine</bred> ou <bred>words</bred> est un ensemble de $2$, $4$ ou $8$ octets

!!! exp
    Une machine de $32$ bits est un ordinateur manipulant des mots de 4 octets ($4\times8 = 32$ bits)

C'est ce principe de regroupement en paquets de bits, ou mots machine, qui permet de donner un sens à tout le paquet de bits et ainsi pouvoir représenter et manipuler des données autres que des $0$ et des $1$ :

* des nombres entiers (positifs ou négatifs), 
* des (approximations de) nombres réels, 
* des caractères alphanumériques et 
* des textes, des images, des sons, des vidéos.

## Représentation en Base $2$ (Binaire) des Entiers Positifs

!!! def 
    **En Base $2$**, on utilise seulement **DEUX** chiffres : $0$ et $1$

!!! pte
    De manière similaire à l'encodage des nombres entiers en base 10, On peut représenter n'importe quel nombre en base 2, grâce à la notation binaire positionnelle, donc avec les seuls chiffres $0$ et $1$.

!!! exp
    $x= 1011$ est un nombre écrit en **notation binaire, sur 4 bits**  

De quel *nombre* (sous-entendu en base $10$) s'agit-il? nous allons le voir.  
Commençons par lever une ambigüité de notations.  

!!! info "Notation"
    Pour lever l'ambigüité entre les notations Décimale et Binaire (par ex. pour des nombres tels que $x=110$), on note les nombres en écriture binaire de l'une des manières équivalentes suivantes:  
    $110 = (110)_2 = 110_2 = \overline{110}_2$

Nous utiliserons principalement la notation : $110_2$  
Mais il pourra néanmoins arriver aussi que nous utilisions la *notation Python* : $0b110 = 110_2$

!!! def
    Pour un nombre binaire,

    1. le bit **le plus à gauche** dans l'écriture binaire (en base 2) s'appelle le <bred>bit de poids fort</bred> :fr: ou <bred>Most Significant Bit</bred> (MSB) :gb:
    1. le bit **le plus à droite** dans l'écriture binaire (en base 2) s'appelle le <bred>bit de poids faible</bred> :fr: ou <bred>Least Significant Bit</bred> (LSB) :gb:

<center>
![Poids Fort vs Poids Faible](./img/PoidsFort_PoidsFaible.png){width="40%"}
</center>

On s'intéresse maintenant aux Conversions Binaire $\Leftrightarrow$ Décimal:

* **Conversion Binaire $\rightarrow$ Décimal:** Etant donné un nombre $x$ donné en écriture binaire, comment écrire ce nombre en base 10?
* **Conversion Décimal $\rightarrow$ Binaire :** Etant donné un nombre $x$ donné en écriture décimale, comment écrire ce nombre en base 2?

### Conversion Binaire $\rightarrow$ Décimal

!!! mth "Conversion Binaire $\rightarrow$ Décimal"
    Pour convertir un nombre entier, donné en base $2$ (binaire), vers le décimal, il convient de :  

    * Au dessus de chaque chiffre, **de droite à gauche**, on écrit des puissances croissantes de $2$ :

    $$2^4\,\,\,\,\,2^3\,\,\,\,\,2^2\,\,\,\,\,2^1\,\,\,\,\,2^0$$

    $$1 \,\,\,\,\,\,\,\, 1 \,\,\,\,\,\,\,\, 0 \,\,\,\,\,\,\,\, 0 \,\,\,\,\,\,\,1$$

    * On multiplie chaque chiffre par la puissance de $2$ correspondante :

    $$11001 = 1\times2^4 + 1\times2^3 + 0\times2^2 + 0\times2^1 + 1\times2^0$$

    $$\quad \quad = 1\times16 + 1\times8 + 0\times4 + 0\times2 + 1\times1$$

    $$\quad \quad = 16 + 8 + 1 = 25$$

Il s'agit d'une **convention d'écriture** de nombres entiers, en base $2$ (Binaire).

!!! python "conversion Binaire $\rightarrow$ Décimal en Python"
    ```python
    >>>int('0b11011',2)
    27
    ```

### Conversion Décimal $\rightarrow$ Binaire

!!! mth "Conversion Décimal $\rightarrow$ Binaire"
    Pour convertir un nombre décimal (en base 10) en nombre binaire (en base 2) il convient de:

    * réaliser la **division euclidienne** du nombre décimal (en base 10) par 2, jusqu'à ce que le **quotient vaille 0**.
    * lire alors la suite de **tous les Restes mais à l'envers** (en commençant du bas vers la haut)

<center>
![Conversion Décimal -> Binaire](./img/Dec2Bin.png)
</center>

Dans cet exemple, on peut dire que: $83_{10} = 1010011_2$

!!! info "Conversion Décimal $\rightarrow$ Binaire"
    ```python
    >>>bin(1534)
    '0b10111111110'
    ```

### Opérations Arithmétiques sur les Nombres Binaires

On peut réaliser les Opérations Arithmétiques usuelles sur les Entiers Binaires: + , $-$ , $\times$ $\div$. Les algorithmes de Calculs que l'on réalise en "posant" les opérations, restent les mêmes qu'en base 10.

Il faut retenir qu'en Base 2:

* Addition :  
$0 + 0 = 0$   
$0 + 1 = 1$  
$1 + 0 = 1$  
$1 + 1 = 10$ c'est à dire 2 en binaire  
**on posera donc 0, et on retient 1 dans la colonne de gauche**

* Soustraction :  
$0 - 0 = 0$  
$1 - 0 = 1$  
$1 - 1 = 0$  

* Multiplication :  
$0 \times 0 = 0$  
$1 \times 0 = 0$  
$0 \times 1 = 0$  
$1 \times 1 = 1$  

* Division :  
$0 \div 1 = 0$  
$1 \div 1 = 1$  

**Exemples d'Opérations Arithmétiques "Posées" en Base 2:**
(à faire)

### Préfixes Binaires vs Décimaux

Une norme publiée en $1998$ impose que les mesures de valeurs informatiques (capacités de mémoires, débits, etc..) utilisent des **préfixes binaires** (càd des puissances de $2$ d'octets):

<center>

| Symbole | Nom <br/>(Préfixe Binaire) | Valeur<br/>Relative | Nombre Exact<br/>Octets ($o$)<br/>(puissances de $2$) | Nombre Exact<br/>d'Octets ($o$) |
| :-: | :-: | :-: | :-: | :-: |
| $Kio$ | Kibi octet | $2^{10}$ octets | $2^{10} \, o$ | $1024$ octets |
| $Mio$ | Mébi octet | $2^{10} \, Kio$ | $2^{20} \, o$ | $1 048 576$ octets |
| $Gio$ | Gibi octet | $2^{10} \, Mio$ | $2^{30} \, o$ | $1 073 741 824$ octets |
| $Tio$ | Tébi octet | $2^{10} \, Gio$ | $2^{40} \, o$ | $1 099 511 627 776$ octets |
| $Pio$ | Pébi octet | $2^{10} \, Tio$ | $2^{50} \, o$ | $1\,125\,899\,906\,842\,624$ octets |
| $Eio$ | Exbi octet | $2^{10} \, Pio$ | $2^{60} \, o$ | $1\,152\,921\,504\,606\,846\,976$ octets |
| $Zio$ | Zébi octet | $2^{10} \, Eio$ | $2^{70} \, o$ | $1\,180\,591\,620\,717\,411\,303\,424$ octets |
| $Yio$ | Yobi octet | $2^{10} \, Zio$ | $2^{80} \, o$ | $1\,208\,925\,819\,614\,629\,174\,706\,176$ octets |

</center>

Néanmoins, certains Systèmes d'Exploitation (Windows) et la grande distribution (dans le commerce) utilisent encore fréquemment des **préfixes décimaux** (puissances de $10$ d'octets) pour mesurer les valeurs informatiques. La confusion entre préfixes binaires et préfixes décimaux est souvent source de confusion pour le client, et d'erreurs (volontaires ou pas) :

<center>

| Symbole | Nom <br/>(Préfixe Décimal) | Valeur Relative | Nombre Exact<br/>d'Octets ($o$)<br/>(puissances de $10$) |Nombre Exact<br/>d'Octets ($o$) |
| :-: | :-: | :-: | :-: | :-: |
| $Ko$ | Kilo octet  | $10^3$ octets| $10^3$ octets  | $1\,000$ octets |
| $Mo$ | Mega octet  | $10^3 \, Ko$ | $10^6 \, o$    | $1\,000\,000$ octets |
| $Go$ | Giga octet  | $10^3 \, Mo$ | $10^9 \, o$    | $1\,000\,000\,000$ octets |
| $To$ | Tera octet  | $10^3 \, Go$ | $10^{12} \, o$ | $1\,000\,000\,000\,000$ octets |
| $Po$ | Péta octet  | $10^3 \, To$ | $10^{15} \, o$ | $1\,000\,000\,000\,000\,000$ octets |
| $Eo$ | Exa octet   | $10^3 \, Po$ | $10^{18} \, o$ | $1\,000\,000\,000\,000\,000\,000$ octets |
| $Zo$ | Zetta octet | $10^3 \, Eo$ | $10^{21} \, o$ | $1\,000\,000\,000\,000\,000\,000\,000$ octets |
| $Yo$ | Yotta octet | $10^3 \, Zo$ | $10^{24} \, o$ | $1\,000\,000\,000\,000\,000\,000\,000\,000$ octets |

</center>

## Représentation en Base 2 (Binaire) des Entiers Négatifs

**Introduction (première tentative naïve):**  
Une première idée simple à laquelle on pourrait penser serait de fixer la convention suivante:  

* On fixe le bit de poids fort comme **bit de signe** :
  * Les nombres binaires **commençant par 0** sont positifs : $0=$"$+$"
  * Les nombres binaires **commençant par 1** sont négatifs : $1=$"$-$"
* La *valeur (absolue)* du nombre étant déterminée par les bits restants après le premier bit de poids fort.

**Exemples:**  
Supposons que nous travaillions sur $4$ bits.  
Avec cette convention naïve:  

$$0101 = +5$$

$$0100 = +4$$

$$1101 = -5$$

$$1011 = -3$$

Cette idée simple pose néanmoins deux problèmes:  

* $0$ admet deux réprésentations distinctes:
  * $0000 = +0$ représente $0$
  * $1000 = -0$ représente également $0$
* les algorithmes des opérations arithmétiques usuelles ($+,-,\times,\div$) ne fonctionnent pas, par exemple on trouverait $3+(-4) = (-7)$ , en effet en posant l'addition:

<center>

$\,\,\,\,0011$  
$+1100$  
$----$  
$=1111$

</center>

Cette idée est donc trop simpliste, on a alors pensé à une autre convention appelée le **complément à 1** :

### Le Complément à $1$

!!! def
    Le <bred>Complément à $1$</bred> d'un nombre binaire est la valeur obtenue en inversant tous les bits de ce nombre (en permutant les $0$ par des $1$, et réciproquement, les $1$ par des $0$).

Remarque :  
Supposons que l'on travaille sur $n$ bits.
On peut voir/définir le complément à 1 d'un nombre $a$ comme le nombre $b$ tel que $a + b =2^n-1$. C'est à dire la quantité manquante (à $a$) pour faire $2^n-1$

!!! exp
    Le complément à $1$ du nombre binaire $1101_2$ est $\overline{1101}=\overline{1}\,\overline{1}\,\overline{0}\,\overline{1}=0010$

**Tableau Représentant les entiers binaires sur $4$ bits et leurs compléments à $1$:**

| Décimal | Binaire Positif	| Complément à $1$ : 1ère Tentative de Binaire Négatif	| Remarque	|
| :-: | :-: | :-: | :-: |
| $0$ | $0000$ | $1111$ | $+0\neq-0$ donc $0$<br/> admet $2$ représensations distinctes<br/>en complément à $1$ |
| $1$ | $0001$ | $1110$ | |
| $2$ | $0010$ | $1101$ | |
| $3$ | $0011$ | $1100$ | |
| $4$ | $0100$ | $1011$ | |
| $5$ | $0101$ | $1010$ | |
| $6$ | $0110$ | $1001$ | |
| $7$ | $0111$ | $1000$ | |

Remarque:  
Avec la convention de complément à 1:

* On dispose automatiquement du **bit de signe** sur le bit de poids fort :
  * $0 =$ "$+$"
  * $1 =$ "$-$"
* La convention du complément à 1 est compatible avec les opérations arithmétiques. Cela résout l'un de nos deux problèmes précédents. Par contre:
* **Problème : Le nombre entier 0 admet encore 2 représentations distinctes, ce qui est tout de même un problème important** car il force à vérifier deux fois si un résultat est nul. On a alors cherché une meilleure solution:

### Le Complément à $2$

!!! def

    * Le <bred>Complément à $2$</bred> d'un nombre binaire est le Complément à $1$, auquel **ON AJOUTE 1**.
    * **PAR DÉFINITION, LES DÉBORDEMENTS SONT IGNORÉS**.

!!! exp
    * Pour coder $-5$ sur $8$ bits:  
        * on prend le nombre positif $5 = 00000101$
        * on inverse les bits (complément à $1$) : $11111010$
        * on ajoute $1$ : $-5_2 = 11111011$ (débordement ignoré, mais ici, il n'y a pas de débordement)
    * Pour coder $-0$ sur $8$ bits:
        * on prend le nombre positif $0$ codé sur $8$ bits : $0 = 00000000$
        * on inverse les bits (complément à $1$) : $11111111$
        * on ajoute $1$ : $100000000$ qui devrait être stocké sur $9$ bits. **OR LE DÉBORDEMENT (AU DELÀ DE 8 BITS, VERS LA GAUCHE) EST IGNORÉ**, cela veut dire que le bit de poids fort (ici, le $1$) sera tout simplement ignoré/perdu/oublié, cela veut dire que le résultat final sera stocké seulement sur $8$ bits, c'est à dire $-0_2 = 00000000$. Il y a bien unicité de l'écriture de $0$ avec le convention de complément à $2$

Tableau résumant le complément à 2 des premiers entiers sur $4$ bits:

<center>

|Décimal | Binaire Positif | Complément à 2 : Binaire Négatif	| Remarque	|
| :-:	|:-: |:-:	|:--:	|
| $0$ | $0000$ | $-0 = 0000$ |$+0=-0$ donc $0$<br/> admet bien une **unique** représentation<br/> en Complément à 2	|
| $1$ | $0001$ | $-1=1111$ | |
| $2$ | $0010$ | $-2=1110$ | |
| $3$ | $0011$ | $-3=1101$ | |
| $4$ | $0100$ | $-4=1100$ | |
| $5$ | $0101$ | $-5=1011$ | |
| $6$ | $0110$ | $-6=1010$ | |
| $7$ | $0111$ | $-7=1001$ | |

</center>

**Calcul de tête du Complément à $2$:**  
Pour **calculer de tête** le complément à 2 d'un nombre binairela méthode est de :
* partir de la droite et le conserver tel quel jusqu'au premier $1$
* poursuivre vers la gauche en inversant tous les chiffres

!!! exp "sur $8$ bits"
    Le Complément à 2 de $00101000$ = $1101$**$1000$**

!!! pte
    La convention de **complément à $2$** résout nos deux problèmes initiaux:

    * $00...00$ est l'**unique représentation** de $0$ sur $n$ bits
    * le Complément à 2 **conserve les algorithmes des opérations arithmétiques**
    * le **bit de signe** du complément à $2$ est automatiquement ajouté par le (passage via le) complément à $1$
    * $11..11$ codé sur $n$ bits, représente l'entier $2^n-1$ en base 10 (et non plus $0$ comme pour le complément à 1). 
      En effet: $11..11 + 1 = 100 ..00 = (2^n)_{10}$. Le résultat $100..00$ étant codé sur $n+1$ bits.  
    * Etant donné que le complément à 2 est le complément à $1$ **plus $1$**, alors la somme d'un entier $a$ et de son complément à 2 noté $b$ vaut : $a+b = 2^n$

## Représentation Approximative des Réels

### Écriture d'un nombre décimal en base 10, sous forme de puissances de 10 ($\geq0$ ou $\leq0$)
On a les transformations d'écriture suivantes :  

$$43,589 = 40 + 3 + 0,5 + 0,08 + 0,009$$

$$43,589 = 4\times10 + 3\times1+5\times0,1+8\times0,01+9\times0,001$$

$$43,589 = 4\times10^1 + 3\times10^0+5\times10^{-1}+8\times10^{-2}+9\times10^{-3}$$

### Ecriture sous Forme Scientifique en base 10

!!! pte "Écriture sous Forme Scientifique d'un nombre décimal $x$ :**"
    Tout nombre décimal $x$ non nul peut s'écrire sous la forme **$x=c,f\times10^n$** où :

    * $c$ est un **chiffre** *mais pas zéro* donc $c\in\{1;2;3;4;5;6;7;8;9\}$
    * $f\in[0;1]$ est la **partie fractionnaire** de $x$ (encore appelée partie décimale de $x$)
    * l'exposant $n$ est un entier relatif $\in\mathbb{Z}$ (positif ou négatif)

!!! mth "Comment en pratique déterminer $c$? $f$? $n$?"
    On distingue classiquement le cas où $x<1$ et le cas où $x>1$.

    1. Soit $x=0,00458<1$ un nombre réel tel que $0<x<1$
    On réalise plusieurs **multiplications par 10 successives** jusqu'à ce que la partie entière ne soit plus égale à 0 (la première fois). On tient les comptes de combien de fois on a multiplié par 10, mais *négativement* (pour compenser l'opération $\times10$): ce nombre final sera $n$.  
    **Exemple:**  
    $0,00458\times10 = 0,0458 \,\,\,\,\,\,(n=-1)$  
    $0,0458\times10 = 0,458 \,\,\,\,\,\,(n=-2)$  
    $0,458\times10 = 4,58\,\,\,\,\,\, (n=-3)$ donc $c=4$, $\,\,f=0,58$ et $n=-3$  
    ceci prouve que $x = 0,00458 = 4,58\times10^{-3}$ on a bien écrit : $x = c,f\times10^n$ avec $c=4 \neq 0$, $f=0,58\in[0;1]$ et $n=-3$.

    1. Soit $x=368,91>1$. Dans ce cas, on réalise des **divisions par 10 successives**, jusqu'à ce qu'il ne reste plus qu'un seul chiffre non nul devant la virgule. On tient les comptes (mais *positivement* cette fois, pour compenser l'opération $\div10$).  
    **Exemple:**  
    $768,91/10 = 76,891 \,\,\,\,\,\,(n=1)$  
    $76,891/10 = 7,6891 \,\,\,\,\,\,(n=2)$ donc $c=7$, $\,\,f=0,6891$ et $n=2$  
    ceci prouve que $x = 768,91 = 7,6891\times10^2$ on a bien écrit : $x = c,f\times10^n$ avec $c=7 \neq 0$, $f=0,6891\in[0;1]$ et $n=2$

### Nombre binaire à virgule en base 2

A l'instar des notations des nombres décimaux en base 10 sous forme de puissances de 10 (positives ou négatives), on peut utiliser la notation de **nombres binaires à virgule (en base 2) sous forme de puissances de 2 (positives ou négatives)**.  
**Exemple:** $x=101,1101_2$ est le nombre binaire à virgule (en base 2) dont l'écriture décimale (en base 10) est: $x=1\times2^2+1\times2^0+1\times2^{-1}+1\times2^{-2}+1\times2^{-4}$  
donc $x=5,8125_{10}$

### Approximation d'un décimal par un flottant

L'encodage des nombres ***flottants*** est inspiré (sans être exactement identique) de l'écriture scientifique des nombres décimaux. Il est basé sur la propriété générale suivante,

> **Propriété: Notation Binaire Scientifique**
> 
> Tout nombre décimal $x$ non nul peut s'écrire de manière unique sous la forme suivante:
> $$x=(-1)^s\times 1,f \times 2^{n}$$
> où :
> 
> * $s$ représente le **bit de signe** de $x$:
>   * $s=0$ si $x>0$, ou bien, 
>   * $s=1$ si $x<0$,
> * le nombre $m = 1,f = 1 + f$ est appelé la **mantisse**. $m$ est un **nombre binaire à virgule** (représentable également en base 10, dans l'intervalle $[1;2[$).  **$f$ est la partie fractionnaire de $m$** (ATTENTION : certains auteurs choisissent d'appeler mantisse le nombre $f$, voire quelquefois utilisent le même mot mantisse pour $m$ et pour $f$)
> * $n\in\mathbb{Z}$ (positif ou négatif) est **l'exposant**

### Norme IEEE-754

La norme IEEE-754 (Institute of Electrical and Electronics Engineers, 1985) est la plus couramment utilisée dans les ordinateurs.  

Un nombre ***flottant*** est représenté/encodé par le triplet des trois nombres $(s,n,f)$ définis par la Notation Binaire Scientifique précédente, tous trois écrits en binaire, **MAIS avec un exposant *décalé* (ou *biaisé*)**:

* $s$ est un **bit de signe** (le bit de poids fort):
   * $s=0$ si $x>0$, ou bien, 
   * $s=1$ si $x<0$,
* l'**exposant $n$ est *décalé*** d'une quantité $d$ de sorte à ce que l'exposant IEEE-754 stocké soit *non signé* (positif)
* la ***partie fractionnaire*** $f$

Ajoutons que la norme fait la différence entre une représentation :

* **sur 32 bits**: appelée ***en simple précision***, ou ***binary32***
  
  On choisit un décalage $d=127$

<center>
![Pour 32 Bits](./img/32Bits_IEEE754.png){width="50%"}
</center>


* **sur 64 bits**: appelée ***en double précision***, ou ***binary64***
  
  On choisit un décalage $d=1023$

<center>
![Pour 64 Bits](./img/64Bits_IEEE754.png){width="90%"}
</center>

En pratique, pour le format 32 bits, deux sens de décalage sont donc à considérer :

* Si on se donne un nombre flottant stocké en mémoire avec la norme IEEE-754, Alors il faut **SOUSTRAIRE 127 à l'exposant** pour retrouver l'écriture décimale du nombre en base 10, grâce à la Notation Binaire Scientifique.
* Par Opposition et Réciproquement, Si on se donne un décimal en base 10, Alors pour encoder ce nombre selon la norme IEEE-754, il faut **AJOUTER 127 à l'exposant** $n$ préalablement déterminé par la Notation Binaire Scientifique.

<center>
![Décalage $d=+/-127$](./img/Décalage_IEEE754.png){width="30%"}
</center>

**Exemple 1:**  
On se donne le mot binaire de 32 bits suivant:  
1 10001010 10011011000000000000000  
Ce nombre représente le nombre décimal $x$ en base 10 calculé via la Notation Binaire Scientifique:  

* Signe $s=(-1)^1=-1$
* exposant = $n-d=(2^7+2^3+2^1)-127$  
  $= (128+8+2)-127$  
  $= 6$
* mantisse $m=1+2^{-1}+2^{-3}+2^{-4}+2^{-5}+2^{-7}+2^{-8}$
  $= 1,73046875$  

**Conclusion:** le nombre décimal représenté est  
$x=(-1)^s\times1,f\times2^{n-d}=-1,73046875\times2^6=110,75$

**Exemple 2:**  
On se donne le nombre décimal en base 10 suivant:  
$x=-48,15625$ que l'on souhaite coder sur 32 bits en simple précision, selon la norme IEEE-754.  
Commençons par déterminer la Notation Binaire Scientifique:

* $x\lt0$ donc $s=1$
* On sépare le nombre en deux: sa partie entière $48$ et sa partie fractionnaire $0,15625$.
  * On convertit la partie entière en binaire: $48_2=110000_2$
  * On convertit la partie fractionnaire/décimale (en base 10) en binaire de la manière suivante:  
  **On multiplie la partie décimale par $2$ jusqu'à obtenir $0$**:  

$$0,15625\times2=\textbf{0},3125$$
$$0,3125\times2=\textbf{0},625$$
$$0,625\times2=\textbf{1},25$$
$$0,25\times2=\textbf{0},5$$
$$0,5\times2=\textbf{1},0$$
$$0,0\times2=\textbf{0},0$$
La partie binaire fractionnaire est constituée des parties entières ainsi trouvées (lire de haut en bas), donc $0,15625_{10}=0,001010_2=0\times2^{-1}+0\times2^{-2}+1\times2^{-3}+0\times2^{-4}+1\times2^{-5}+0\times2^{-6}$ (on veillera à vérifier ce résultat)

* Le nombre décimal en base 10 s'écrit donc en base 2 :  
 $x=48,15625_{10}=110000,001010_2$  
* On décale la virgule à gauche de $n=5$ crans pour l'écrire $x=1,f\times2^n=1,10000001010\times2^5$. Donc $f=10000001010$ et $n=5$. 
* Mini-Conclusion: Forme Binaire Scientifique :  
$x=(-1)^1\times1,10000001010\times2^5$

Enfin, pour respecter la norme IEEE-754,

* on complète la partie fractionnaire $f$ avec des $0$ jusqu'à 23 bits en simple précision (32 bits), de sorte que finalement:  
$f=\textbf{10000001010}000000000000$
* on ajoute $d=127$ à l'exposant **AVANT** de le coder: $n+d=5+127=132_{10}=10000100_2$  

**Conclusion:** Le nombre décimal $x=-48,15625$ est donc encodé comme suit avec la norme IEEE-754:  
  $1 \,\,\,\,\,\,\,\,\, 10000100 \,\,\,\,10000001010000000000000$  
  signe exposant $\,\,\,\,$ (partie fractionnaire)

**Remarque :** La norme IEEE-754 n'utilise pas l'encodage par complément à 2 pour encoder des exposants positifs ou négatifs, mais une méthode consistant à stocker l'exposant de manière ***décalée*** sous la forme d'un  nombre **non signé**. On pourrait se demander pourquoi? C'est principalement parce que les comparaisons entre deux flottants ainsi représentés sont BEAUCOUP plus simples qu'en complément à 2.  

**Aller plus loin:**

1. Valeurs spéciales:  
  La norme permet de stocker deux zéros:  
* Le $0$ positif, noté $+0$, est représenté par $(s=0,n=0,m=0)$

* Le $0$ négatif, noté $-0$, est représenté par:
  * le signe 1 (le reste idem)
* deux infinis, notés $-\infty$ $(s=1,n=255,m=0)$ et $+\infty$ $(s=0,n=255,m=0)$ qui sont utilisés pour signaler un dépassement de capacité
* Une valeur spéciale notée $NaN$ (pour Not a Number) $(s=1,n=255,m\neq0)$ est utilisée pour représenter les résultats d'opérations invalides comme $0/0$, $\sqrt-1$, ou $0\times+\infty$.

2. Représentation Approximative des Décimaux sous forme de flottants: Il peut arriver que le nombre décimal $x$ de départ ne soit pas représentable *exactement* avec cet encodage sous forme de flottant. C'est pour cela que la représentation sous forme de flottants est dite ***approximative***.

**Exercice:**  
1. Montrer qu'en simple précision (32 bits), les nombres flottants positifs peuvent représenter les nombres décimaux entre $[10^{-38};10^{38}]$ environ  
2. Montrer qu'en double précision (64 bits), les nombres flottants positifs peuvent représenter les nombres décimaux entre $[10^{-308};10^{308}]$ environ  


