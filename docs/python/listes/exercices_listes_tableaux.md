# 1NSI : Exercices Listes et Tableaux

Une **liste de (sous-)listes** peut-être appelée un **Tableau** de valeurs (2D), ou **Table** de valeurs. On pourrait généraliser à plusieurs dimensions.

!!! ex "Générer un Tableau (2D) aléatoire"
    Créer une fonction `creer_tableau(n:int,a:int,b:int)` qui :
    * reçoit en entrée trois nombres entiers: `n`, `a` et `b`
    * renvoie en sortie un tableau (une liste de (sous-)listes) tel que:
        * la taille du tableau vaut $n \times n$, càd que chaque sous-liste admet une taille `n`, et il existe `n` sous-listes
        * dont les valeurs sont toutes des entiers aléatoires compris entre `a` et `b`

!!! ex "Parcourir les éléments d'un tableau (2D)"

    Soit `l = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]`

    1°) Écrire un algorithme qui parcoure chacun des éléments de chacune des sous-listes de la liste `l`, en l'affichant dans le Terminal, séparément un par un (afficher 1, puis 2, etc..). On pourra utiliser deux boucles `for` imbriquées. De sorte que les éléments de la première ligne soient d'abord affichés, puis ceux de la deuxième ligne, etc.. : on parle de **parcours "en largeur"** (d'abord)  
    2°) Modifier l'algorithme précédent pour parcourir en largeur, mais tout à l'envers par rapport à la question précédente:  

    * En commençant l'affichage par le dernier élément de la dernière sous-liste,
    * puis, en remontant chaque sous-liste de bas en haut,
    * le dernier élément affiché étant le premier élément de la première sous-liste  

    3°) Parcourir **"en profondeur"** (d'abord) les éléments de la liste de sous-listes  
    4°) Puis parcourir en profondeur la liste de sous-listes, mais à l'envers



