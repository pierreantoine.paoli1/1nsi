# 1NSI : Exercices Listes

Dans les exercices qui suivent, à chaque fois qu'il sera demandé de parcourir les éléments d'une liste dans une question, il est conseillé de s'entraîner à le faire avec chacune des deux méthodes de parcours d'une liste vues dans le cours :  

* **Méthode 1**: en utilisant la syntaxe `for el in l`
* **Méthode 2**: en utilisant la syntaxe `for i in range(len(l))` :
    * `i` désigne l'indice d'un élément
    * `l[i]` désigne l'élément de `l` situé à l'indice `i`

!!! ex "Parcourir une Liste avec la boucle `for` :rocket:"
    Soit `l=[3,7,9,"Bonjour", [1,2]]` une liste.  
    Parcourir les éléments de cette liste, en les affichant un par un dans le Terminal, avec les deux méthodes du cours.

!!! ex "La fonction `eval()` pour entrer une liste via `input`"
    La méthode `eval()` est une fonction native de Python, qui peut être utilisée pour *évaluer/convertir* la chaîne de caractère `"[1,2,3]"` (obtenue par exemple via un `input`) en la liste `[1,2,3]`.  
    Grâce à la fonction `eval()`, créer un algorithme qui:

    * demande en entrée (via `input`) la liste `l` à afficher
    * affiche en sortie, dans un Terminal, chacun des éléments de la liste, avec les deux méthodes de parcours d'une liste.

!!! ex "Nombres d'éléments dans une liste"
    Sans utiliser la fonction native `len()`, et en utilisant obligatoirement un parcours de liste avec la boucle `for`, créer une fonction `longueur(l:list)->int` qui:

    * reçoit en entrée une liste `l`
    * renvoie en sortie le nombre d'éléments de la liste `l`

!!! ex "Construire une liste, élément par élément, avec `append()`"
    La syntaxe `l.append(el)` permet d'ajouter un élément `el` à la fin de la liste `l` (à droite)  
    1°) Créer une fonction `superieurs_a(el:float, l:list)->list` qui :

    * reçoit en entrée un nombre flottant `el`, et une liste `l`
    * renvoie en sortie la liste de tous les éléments de `l` ayant une valeur **supérieure** (ou égale) à `el`  

    2°) Créer une fonction `inferieurs_a(el:float, l:list)->list` qui :

    * reçoit en entrée un nombre flottant `el`, et une liste `l`
    * renvoie en sortie la liste de tous les éléments de `l` ayant une valeur **inférieure** (ou égale) à `el`  

    3°) Créer une fonction `compris_entre(a:float, b:float, l:list)->list` qui :

    * reçoit en entrée deux nombres flottants `a` et `b`, et une liste `l`
    * renvoie en sortie la liste de tous les éléments de `l` ayant une valeur **comprise entre** `a` et `b`

!!! ex "Liste d'Entiers Aléatoires"
    Créer une fonction `creer_liste_alea(n:int,a:int,b:int)->list` qui:

    * reçoit en entrée trois entiers : `n`, `a` et `b`
    * renvoie en sortie une liste de `n` nombres entiers aléatoires compris entre `a` et `b`

!!! ex "Élément dans une liste"
    1°) Créer une fonction `est_inclus(el, l:list)->bool` qui :

    * reçoit en entrée deux paramètres: 
        * un élément `el` dont on veut tester l'appartenance, ou pas dans une liste `l`
        * la liste `l` dans laquelle il faut tester si OUI, ou NON, elle contient l'élément `el`
    * renvoie en Sortie:
        * `True` si l'élément `el` est inclus dans la liste `l`
        * `False` sinon  

    2°) Créer une fonction `premier_indice(el, l:list)->int` qui:

    * reçoit en entrée un élement `el`, et une liste `l`
    * renvoie en sortie :
        * l'indice de la **première** occurence de l'élément `el`, lorsqu'il est inclus dans la liste `l`
        * `-1` sinon  

    3°) Créer une fonction `dernier_indice(el, l:list)->int` qui:

    * reçoit en entrée un élement `el`, et une liste `l`
    * renvoie en sortie :
        * l'indice de la **dernière** occurence de l'élément `el`, lorsqu'il est inclus dans la liste `l`
        * `-1` sinon  
        
    4°) Créer une fonction `nombre_occurences(el,l: list)->int` qui:

    * reçoit en entrée un élément `el` et une liste `l`
    * qui renvoie en sortie le nombre d'occurences (combien y en a-t-il?) de cet élément `el` dans la liste `l` (la réponse vaut `0` au cas où l'élément n'existe pas dans la liste)

    5°) Créer une fonction `liste_indices(el, l:list)->int` qui:

    * reçoit en entrée un élement `el`, et une liste `l`
    * renvoie en sortie :
        * la liste de toutes les occurences de l'élément `el`dans la liste `l`, lorsqu'il y en a..(des occurences)
        * la liste vide `[]`, sinon  

!!! ex "Obtenir le Min et le Max d'une Liste"
    On se donne une liste `l = [45, 34, 12, 48, 57, 26, 13, 10, 53, 49, 25]`  
    1°) Créer une fonction `mini(l:list)->int` qui :
    
    * reçoit en entrée une liste `l`
    * renvoie en sortie le **minimum** parmi tous les éléments (entiers) de la liste  

    2°) Créer une fonction `maxi(l:list)->int` qui :
    
    * reçoit en entrée une liste `l`
    * renvoie en sortie le **maximum** parmi tous les éléments (entiers) de la liste

!!! ex "Renverser une liste"
    On se donne une liste `l = [1, 4, 5, 12, 10, 8]`  
    1°) Créer une fonction `renverse(l:list)->list` qui :
    
    * reçoit en entrée une liste `l`
    * renvoie en sortie la même liste mais renversée (dans l'ordre contraire): càd `[8, 10, 12, 5, 4, 1]` dans notre exemple  

!!! ex "Somme, Moyenne et Variance d'une liste d'éléments"
    On se donne une liste de valeurs `l=[2,6,9,14,4,7,5]` et une liste de coefficients `c=[1,2,1,3,2,1,1]`.  
    1°) Créer une fonction `somme(l:list)->float` qui :
    
    * reçoit en entrée une liste `l`  
    * renvoie en sortie la **Somme** de tous les éléments $x_i$ de la liste `l`  

    2°) Créer une fonction `moyenne(l:list)->float` qui :
    
    * reçoit en entrée une liste `l` de valeurs
    * renvoie la **Moyenne** (non coefficientée) des éléments $x_i$ de la liste `l`  

    3°) Créer une fonction `moyenne_coeff(l:list, c:list)->float` qui :
    
    * reçoit en entrée une liste `l` de valeurs et une liste `c` de coefficients
    * renvoie en sortie la **Moyenne coefficientée** $m$ des éléments de la liste `l` coefficientée par `c`  

    4°) Créer une fonction `variance(l:list, c:list)->float` qui :
    
    * reçoit en entrée une liste `l` de valeurs et une liste `c` de coefficients
    * renvoie en sortie la variance des valeurs de la liste `l`, coefficientés par la liste `c`  
    Rappel :  

    <center><enc>$V = \dfrac {\displaystyle \sum_{i=0}^{n-1} c_i \times (x_i-m)^2}{n}$<br/>
    $V = \displaystyle \dfrac {c_1 (x_1 - m)^2+c_2 (x_2 - m)^2 +...+ c_{n-1} (x_{n-1} - m)^2}{n}$
    </enc></center>

    Dans cette formule :

    * les $x_i$ désignent les éléments `l[i]` de la liste des valeurs
    * les $c_i$ désignent les éléments `c[i]` de la liste des coefficients
    * $m$ désigne la moyenne de tous les $x_i$
    * $n$ désigne le nombre d'éléments $x_i$ de la liste, donc la longueur de la liste `l`

    5. Calcule l'**Écart-Type** $\sigma= \sqrt V$ des éléments de la liste