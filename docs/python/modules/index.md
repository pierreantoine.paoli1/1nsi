# 1NSI : Modules & Packages Python

## Introduction

!!! def "Qu'est-ce qu'un *module* Python ?"
    Un <bred>module</bred> est une collection de fonctions (comprendre *de fonctionnalités*), regroupées en un même fichier `.py`, qui étendent les fonctionnalités de base du langage de programmation Python.

!!! def "Qu'est-ce qu' *importer un module* Python ?"
    1°) Dans tous les cas, pour pouvoir l'utiliser, un **module** doit être au minimum **installé** sur la machine.  
    On
    Selon le mode d'installation de Python que vous avez choisi de faire sur votre machine (seulement python sur Python.org, ou bien un pack comme Anaconda qui contient Python ainsi que de nombreux autres modules)
    2°) Même si un module python a déjà été *installé* dans votre Système d'Exploitation, **par défaut**, un module Python n'est pas chargé/*importé* (automatiquement).  
    Pour pouvoir disposer des fonctionnalités supplémentaires d'un module, il faut **obligatoirement** et **préalablement** indiquer à Python que l'on souhaite les utiliser grâce une ligne de commande : on parle dans ce cas d' <bred>importer un module Python</bred> (plusieurs syntaxes existent)

Il existe des milliers de modules pour Python : on en trouve une liste exhaustive sur le site **Pypi.org**

Il existe **plusieurs syntaxes pour importer un module en Python**. Dans la suite, nous détaillons $4$ syntaxes différentes pour importer le module `math`

## Syntaxe 1 : ```import math```

```python
>>> import math
>>> math.sqrt(2)
1.4142135623730951
>>> math.cos(60)
-0.9524129804151563
```

## Syntaxe 2 : ```import math as m```

```python
>>> import math as m
>>> m.sqrt(2)
1.4142135623730951
>>> m.cos(60)
-0.9524129804151563
```

## Syntaxe 3 : ```from math import sqrt```

### On peut importer **une seule fonction** d'un module

```python
>>> from math import sqrt
>>> sqrt(2)
1.4142135623730951
>>> cos(60)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-6-78a7de5cd17a> in <module>
----> 1 cos(60)

NameError: name 'cos' is not defined
>>> sin(30)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-7-d1dd6e003e86> in <module>
----> 1 sin(30)

NameError: name 'sin' is not defined
```

### On peut importer **plusieurs fonctions** d'un module, simultanément, et **seulement celles-là**

```python
>>> from math import sqrt, cos
>>> sqrt(2)
1.4142135623730951
>>> cos(60)
-0.9524129804151563
>>> sin(30)
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-7-d1dd6e003e86> in <module>
----> 1 sin(30)

NameError: name 'sin' is not defined
```

## Syntaxe 4 : ```from math import *```

On peut importer TOUTES les fonctions d'un module simultanément : le caractère `*` signifie TOUTES.

```python
>>> from math import *
>>> sqrt(2)
1.4142135623730951
>>> cos(60)
-0.9524129804151563
>>> sin(30)
-0.9880316240928618
```


