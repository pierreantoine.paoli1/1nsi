# 1NSI : Introduction à l'Univers Python

## Qu'est-ce que Python ?

Python est un <bred>langage de programmation</bred> servant à développer des **logiciels**, ou <bred>algorithmes</bred>.
Pour programmer en Python (en local, sur votre ordinateur), on doit **spécifiquement installer** le langage Python sur votre ordinateur. En pratique, Python est souvent déjà installé dans votre Système d'Exploitation (Windows, MacOS et/ou linux), **mais** la version installée est souvent (très? ça dépend..) ancienne car utilisée par/pour l'OS, et ce n'est pas une bonne idée d'utiliser la version de Python installée par défaut avec votre OS, pour (apprendre à?) programmer en Python. Si vous souhaitez programmer en Python, il convient d'installer une version récente et moderne de Python.

Après installation, Python contient toujours, au minimum, la **Bibliothèque/Librairie Standard de Python**.

## Qu'est-ce que la Bibliothèque/Librairie Standard Python ?

La [bibliothèque / librairie standard de Python](https://docs.python.org/fr/3/library/index.html) est un très large réunion d'outils inclus automatiquement lors de l'installation de Python, qui contient deux grands types de fonctionnalités :

* des **modules natifs** (écrits en C) permettant de travailler en Python, notamment :
* de très nombreux **modules supplémentaires** (écrits en Python), permettant de travailler de manière standardisée avec des **fonctionnalités thématiques** supplémentaires, utiles pour le quotidien des développeurs.

!!! def "Qu'est-ce qu'un Module Python ?"
    En Python, un <bred>module</bred> est un fichier ajoutant des fonctionnalités supplémentaires au Langage de programmation Python. Ils se concentrent souvent sur un thème précis assez spécialisé.

### Extrait du Contenu de la Bibliothèque/Librairie Standard de Python

Sont inclus dans la bibliothèque/librairie standard :

#### de très nombreuses **fonctionnalités natives**

!!! def "Qu'est-ce qu'une fonctionnalité native ?"
    Une fonctionnalité est dite <bred>native</bred> lorsqu'elle fait partie du langage Python lui-même, en particulier on peut l'utiliser directement (dans Python), sans devoir taper préalablement aucune autre commande/instruction pour en disposer.

* Les **types de données natifs** (nombres entiers `int`, nombres flottants `float`, chaînes de caractères `str`, listes `list`, tuples `tuple`, dictionnaires `dict`, données binaires `bytes` et `bytearray`, etc..), 
* Les **fonctions natives** de Python ([cf liste](https://docs.python.org/fr/3.9/library/functions.html)) ([cf quelques exemples d'utilisations](introduction/fonctions_natives.md))

    <center>

    | Fonctions Natives Python|||||
    |:-:|:-:|:-:|:-:|:-:|
    | abs() | delattr() | hash() | memoryview() | set() |
    | all() | dict() | help() | min() | setattr() |
    | any() | dir() | hex() | next() | slice() |
    | ascii() | divmod() | id() | object() | sorted() |
    | bin() | enumerate() | input() | oct() | staticmethod() |
    | bool() | eval() | int() | open() | str() |
    | breakpoint() | exec() | isinstance() | ord() | sum() |
    | bytearray() | filter() | issubclass() | pow() | super() |
    | bytes() | float() | iter() | print() | tuple() |
    | callable() | format() | len() | property() | type() |
    | chr() | frozenset() | list() | range() | vars() |
    | classmethod() | getattr() | locals() | repr() | zip() |
    | compile() | globals() | map() | reversed() | \_\_import\_\_() |
    | complex() | hasattr() | max() | round() |  |

    </center>

* les **mots-clés natifs** réservés du Langage Python ([cf liste](https://docs.python.org/fr/3.9/reference/lexical_analysis.html#keywords))

    <center>

    | **Mots-Clés Réservés** de Python |||||
    |:-:|:-:|:-:|:-:|:-:|
    | False      | await      | else       | import     | pass |
    | None       | break      | except     | in         | raise |
    | True       | class      | finally    | is         | return |
    | and        | continue   | for        | lambda     | try |
    | as         | def        | from       | nonlocal   | while |
    | assert     | del        | global     | not        | with |
    | async      | elif       | if         | or         | yield |

    </center>

* Les **exceptions natives** (gestion des erreurs), 
* La gestion native de fichiers (ouverture, lecture, écriture, de fichiers)
* etc...

#### des **modules supplémentaires**

!!! danger
    Bien qu'appartenant à la bibliothèque/librairie standard de Python, ces modules supplémentaires **NE sont PAS natifs** : ils **doivent donc être préalablement chargés en mémoire, grâce à une instruction,** pour pouvoir disposer des fonctionnalités supplémentaires qu'ils contiennent. On parle d'<bred>importer un module</bred>.

Voici un extrait des **modules supplémentaires** inclus dans la bibliothèque/librairie standard de Python (mais la liste ci-après est très loin d'être exhaustive) :

* **Nombres et mathématiques** :
    * le module `math` (resp. `cmath`) pour les calculs mathématiques avancés (resp. avec des nombres complexes), 
    * le module `random` pour travailler avec l'aléatoire, 
    * et d'autres modules : `numbers`, `fractions`, `decimal`, `statistics`  
* **Accès aux Fichiers et Dossiers** :
    * le module `os` pour les manipulations courantes de fichiers et dossiers
* **Traitement et Persistance de Données** :
    * le module `csv` pour lire, sauvegarder et manipuler des fichiers au format *csv*,
    * le module `json` pour lire, sauvegarder et manipuler des fichiers au format *csv*,
    * les modules `html` et `xml` pour lire, sauvegarder et manipuler des fichiers au format *html* et *xml*,
    * le module `sqlite3` pour travailler avec les bases de données *SQLite*, 
    * le module `pickle` pour lire, manipuler et sauvegarder des types de données construits dans des fichiers binaires (spécifiques à Python),
    * le module `base64` pour encoder/décoder des données entre le *binaire* et *base64*, *base16*, *base32* et *base85*, *ASCII*
    * etc..
* **Graphiques** :
    * le module `turtle` pour dessiner avec une **tortue**, 
    * le module `tkinter` pour créer des fenêtres graphiques,
* **Compression et Archivage** : les modules `zlib`, `gzip`, `bz2`, `lzma`, `zipfile`, `tarfile`
* **Environnement d'Exécution et Outils de Développement Python** : 
    * le module `sys` contrôle l'exécution d'un algorithme (pause, stop, etc..)
    * le module `time` permet la détermination des *temps d'exécution* d'un algorithme
    * les modules `cmd` et `shlex` permettent l'exécution de commandes du Système d'Exploitation
    * le module `unittest` : un module de *tests unitaires* pour le débuggage des scripts/algorithmes Python
    * *IDLE* est un Environnement de Développement Intégré (IDE) par défaut
    * etc..
* **Exécution concourante/parallèle** :
    * le module `threading` gère le parallélisme basé sur les *fils d'exécution* (*threads*)
    * le module `subprocess` gestion des sous-processus
* **Réseaux** : 
    * le module `socket` : gestion réseau de bas niveau
    * le module `ssl` : utilisation de TLS/SSL pour la sécurité des communications
* **Protocoles internet** : de nombreux modules spécifiques, un pour chaque protocole internet (cgi, url, http, ftp, pop, imap, stmp, telnet, sockets, ip, etc...)
* **services multimedia** : par ex. module `audioloop` manipulation de données audio brutes, etc..
* **internationalisation**
* etc.. (de **très** nombreux autres...)

Vous trouverez un **listing exhaustif** de ces modules supplémentaires de la bibliothèque standard, sur [cette page](https://docs.python.org/fr/3/library/index.html) de la documentation officielle.


## Qu'est-ce qu'un Module Python?

!!! def "(bis) Qu'est-ce qu'un Module Python ?"
    En Python, un <bred>module</bred> est un fichier ajoutant des fonctionnalités supplémentaires au Langage de programmation Python. Ils se concentrent souvent sur un thème précis assez spécialisé.

!!! info "Un Module intéressant (mais pas trop simple pour les débutants)"
    <bred>PyQt</bred> est un module permettant de créer des **interfaces graphiques** en Python, càd des logiciels avec des fenêtres graphiques : Ce module lie le langage Python avec une bibliothèque pour fenêtres graphiques appelée **Qt** (développé par Nokia). Il ne fait PAS partie de la bibliothèque/librairie standard de Python, et doit donc être séparément et préalablement installé convenablement.

## Qu'est-ce qu'un Package Python?

!!! def "Qu'est-ce qu'un Package Python ?"
    * En Python, un <bred>package</bred> est un ensemble de fichiers, usuellement structurés dans des dossiers, ajoutant des fonctionnalités supplémentaires au Langage de programmation Python. Ils se concentrent souvent sur un thème précis assez spécialisé.  
    * Dans les autres langages de programmation, on parle plutôt de <bred>bibliothèque</bred> (logicielle), ou de <bred>librairie</bred> :fr:, ou <bred>library</bred> :gb:.

Intuitivement et plus simplement, un **package** ou une **bibliothèque/librairie** est un ensemble de fonctionnalités supplémentaires, souvent sous forme de *fonctions* (au sens informatique du terme), ayant souvent une thématique particulière.

!!! info "Quelques bibliothèques/librairies/packages Python intéressantes"
    * <bred>Pillow</bred> : *Pillow* est une bibliothèque pour la **manipulation d'images** en Python. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>NumPy</bred> : pour les calculs mathématiques avec des **tableaux multidimensionnels** de nombres et les **matrices** (2D). Utile pour les calculs scientifiques fondamentaux et l' **Apprentissage Automatique** :fr: / le **Machine Learning** (ML) :gb:. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>SciPy</bred> : basée sur *NumPy*, *SciPy* contient différents modules pour le **calcul scientifique** : modules d'*interpolation* et d'*optimisation*, d’*algèbre linéaire*, d’*intégration* et de *statistiques*, de *traitement du signal*, et de *traitement de données financières*. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>Pandas</bred> : basée sur *SciPy* et *NumPy*, *Pandas* est très utile pour la **manipulation** et l'**analyse de données**. *Pandas* est idéal pour manipuler les *données de séries chronologiques*, par exemple pour analyser dans le temps les mouvements des prix sur les marchés financiers. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>Matplotlib</bred> : compatible avec *NumPy* et *SciPy*, *Matplotlib* est une bibliothèque pour la **visualisation (graphique) des données**. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>Plotly</bred> : compatible avec *NumPy* et *SciPy*, *Plotly* est une bibliothèque pour la **visualisation (graphique) *dynamique* des données**. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>scikit-learn</bred> : basé sur *NumPy* et *SciPy*, *scikit-learn* est une bibliothèque pour l' **Apprentissage Automatique** :fr: / le **Machine Learning** (ML) :gb:. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>PyTorch</bred> : basé sur *NumPy* et *SciPy*, *PyTorch* est une bibliothèque pour l' **Apprentissage Automatique** :fr: / le **Machine Learning** (ML) :gb:. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>TensorFlow</bred> : *TensorFlow* est une bibliothèque pour l' **Apprentissage Automatique** :fr: / le **Machine Learning** (ML) :gb:, les **Réseaux de Neurones** :fr: / **Neural Networks** :gb:, et l' **Apprentissage Profond** :fr: / le **Deep Learning** :gb:. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:
    * <bred>Keras</bred> : Basé sur *Tensorflow*, *Keras* est une bibliothèque pour les **Réseaux de Neurones** :fr: / les **Neural Networks** :gb:, et l' **Apprentissage Profond** :fr: / le **Deep Learning** :gb:. Cette bibliothèque fait partie de la **distribution Anaconda**. :+1:

    

## Qu'est-ce qu'un Framework / Cadriciel?

!!! def "Qu'est-ce qu'un Framework / Cadriciel? ([inspiré de Wikipedia](https://fr.wikipedia.org/wiki/Framework))"
    Un <bred>framework</bred> :fr: :gb: ou <bred>cadriciel</bred> :fr: (au sens: *cadre de travail*) est un ensemble cohérent de composants logiciels, *structurés* d'une certaine manière, aussi bien physiquement (dans des dossiers et sous-dossiers) que logiciellement (de sorte que ces composants logiciels puissent communiquer entre eux), et qui sert à créer la structure logicielle de tout ou d’une partie d'un logiciel (*architecture logicielle*).  
    Un **framework** se distingue d'une simple *bibliothèque logicielle* principalement par :  

    * son **caractère générique, faiblement spécialisé**, contrairement à certaines bibliothèques.  
    Un framework peut à ce titre être constitué de **plusieurs bibliothèques**, chacune spécialisée dans un domaine. Un framework peut néanmoins être spécialisé, sur un langage particulier, une plateforme spécifique, un domaine particulier : communication de données, data mapping, etc.
    * son **architecture logicielle** (comment structurer le logiciel) : cela peut aller jusqu'à devoir respecter certains <bred>patrons de conception</bred> :fr: ou <bred>design pattern</bred> :gb:, càd une manière caractéristique d'organiser des modules, reconnu comme bonne pratique en réponse à un problème de conception d'un logiciel. Les bibliothèques constituant le framework sont alors organisées selon le même paradigme/modèle.

<env>En pratique</env> Un **framework** est un **gros logiciel**, basé sur un langage de programmation, servant à fabriquer des applications/logiciels, mais d'une certaine manière très spécifique, classiquement avec :

* des **contraintes physiques** (structuration en dossiers et sous-dossiers spécifiques) et 
* des **contraintes logicielles** (composants logiciels élémentaires souvent imposés, mais communiquant nativement entre eux), 
    
Un framework utilise souvent des notions avancées de programmation :

* des **paradigmes de programmation avancée** (par ex. Programmation Orientée Objet - OOP, au programme de TNSI)
* des **architectures logicielles avancées** (par ex. MVC=Modèle Vue Controlleur= une manière de penser les responsabilités des composants logiciels, etc..)

C'est pourquoi, **un framework est peu accessible aux débutants**, et son apprentissage requiert un certain temps et aussi de surmonter des difficultés techniques et d'abstraction (on parle de *learning curve* :gb:). Par contre, il peut être très utile à des développeurs plus expérimentés pour ne pas avoir à réinventer la roue, car il dispose souvent de composants logiciels classiques usuels et prêts à l'emploi, par exemple :

* un composant pour gérer l'authentification
* un composant pour le routage d'URLs
* un composant pour des modèles (de pages, etc..)
* un composant pour gérer la communication avec les bases de données
* etc..

!!! info "Quelques frameworks Python"
    Les frameworks suivants, basés sur le Langage Python (et HTML, etc..), servent à créer des **applications/sites web**

    * <bred>Django</bred> (gros framework)
    * <bred>Flask</bred> (micro framework)
    * <bred>TurboGears</bred> (gros framework)
    * <bred>CherryPy</bred> (micro framework)
    * <bred>Pyramid</bred>, etc..



## Qu'est-ce que le dépôt PyPI ?

Au delà de la bibliothèque standard, il existe une collection grandissante de **plusieurs centaines de milliers de composants** (des *programmes*, des *modules*, des *packages*, des *frameworks*, etc..), disponibles dans le **dépôt de logiciels** appelé **[Python Package Index - PyPI](https://pypi.org/)** : https://pypi.org/

On peut installer les logiciels de ce dépôt sur votre machine grâce au gestionnaire de paquets Python nommé `pip`
