d={
    "Dumont": 20,
    "Schwarz": 4,
    "Lopez": 15,
    "Lombardo": 7,
    "Benallal": 16
}

def ventes(d:dict)->int:
  s = 0
  for valeur in d.values():
    s += valeur
  return s

print(ventes(d))

def meilleursVendeurs(d:dict)->(list,str):
     l = []
     caMax = 0
     for (nomCommercial,ca) in d.items():
          if ca > caMax:
               caMax = ca
               l.append(nomCommercial)
          elif ca == caMax:
               l.append(nomCommercial)
     return  [l , caMax]

print(meilleursVendeurs(d))

def chiffreAffaire(d:dict)->int:
  ca = 0
  for valeur in d.values():
    ca += valeur
  return ca

print(chiffreAffaire(d))

chaine = """3945U;BERNIER;JACQUES
3758K;DUPOND;LUCIE
2594L;DURAND;MARC"""

def chaine2dico(chaine:str)->dict:
  d = {}
  listeLignes = chaine.split("\n")
  for ligne in listeLignes:
    l = ligne.split(";")
    d[l[0]] = l[2] + " " + l[1]
  return d

print(chaine2dico(chaine))

notes = {
  "John": [8,10,13],
  "Pamé": [17,15,6],
  "Laura": [7,12,14],
  "Kevin": [4,8,9],
  "Juliette": [6,15,17]
}

def meilleuresNotes(notes:dict)->(list,str):
  l = []
  moyenneMax = 0
  for (nomEtudiant,listeNotes) in notes.items():
    moyenne = sum(listeNotes)/len(listeNotes)
    if moyenne > moyenneMax:
      l = []
      moyenneMax = moyenne
      l.append(nomEtudiant)
    elif moyenne == moyenneMax:
      l.append(nomEtudiant)
  return  [l , round(moyenneMax,2)]

print(meilleuresNotes(notes))