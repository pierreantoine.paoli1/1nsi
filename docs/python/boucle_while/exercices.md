# 1NSI: Exercices Boucle While

<env>Consigne</env> :warning: Les exercices suivants **DOIVENT** utiliser la boucle **while**... :warning:

!!! ex "Suites de Valeurs :rocket:"
    Écrire un algorithme qui affiche les valeurs suivantes (en orange) dans le Terminal :  
    
    === "Question 1."  
        ```python
        30
        31
        32
        33
        34
        35
        36
        ```
    === "Question 2."
        ```python
        37
        36
        35
        34
        33
        32
        ```
    
    === "Question 3."
        ```python
        34
        36
        38
        40
        42
        44
        46
        ```
    
    === "Question 4."
        ```python
        52
        50
        48
        46
        44
        42
        ```
    
    === "Question 5."
        ```python
        1
        2
        3
        5
        8
        13
        21
        34
        ```
    === "Question 6."
        a°) Tous les inverses d'entiers, strictements supérieurs à 1/10
        ```python
        1/ 1 = 1.0
        1/ 2 = 0.5
        1/ 3 = 0.3333333333333333
        1/ 4 = 0.25
        1/ 5 = 0.2
        1/ 6 = 0.16666666666666666
        1/ 7 = 0.14285714285714285
        1/ 8 = 0.125
        1/ 9 = 0.1111111111111111
        ```
        b°) Généraliser à Tous les inverses d'entiers, strictements supérieurs à 1/1000  
        c°) arrondir les résultats des fractions à $2$ chiffres après la virgule, grâce à la fonction `#!python round()` du module `#!python math`

!!! ex "Richesse :euro: :euro: :rocket:"
    Anna dispose aujourd'hui de $48€$  
    Chaque jour, elle gagne la même somme: $17€$

    Écrire un algorithme qui détermine une solution aux problèmes suivants:  

    * Dans combien de jours disposera-t-elle de $10\,000€$ ?
    * Exprimer ce délai en nombre de mois ($1$ mois $=30$ jours, pour simplifier) et de jours

!!! ex "Plan d'épargne sur Livret $A$ :rocket:"
    Paul dispose aujourd'hui d'une somme de $1200€$, qu'il place sur son livret $A$ à un taux mensuel de $1,25\%$ (intérêts versés le 1er du mois).  
    Chaque mois, il calcule son nouveau solde par rapport à celui du mois précédent, grâce à la formule suivante:  
     <center><enc>$solde_{nouveau} = (1+1,25\%) \times solde_{ancien}$</enc></center>

    Écrire un algorithme qui détermine une solution aux problèmes suivants:

    * Dans combien de mois disposera-t-il de $10\,000€$ sur son livret $A$ ?
    * Exprimer ce délai en nombre de mois (de 30 jours, pour simplifier) et de jours

!!! ex "Certifier qu'on mot de passe contient (au moins) un chiffre :rocket::rocket:"
    Écrire un algorithme qui demande en entrée un mot de passe, et qui continue de demander d'entrer un mot de passe tant que la réponse de l'utilisateur ne contient pas (au moins) un chiffre.

!!! ex "Certifier que $a>b$ :rocket::rocket:"
    Écrire un algorithme qui demande deux nombres $a$ et $b$, et qui certifie que la réponse de l'utilisateur convient bien, càd que la condition $a>b$ est vraie (avant de poursuivre, éventuellement, vers de futures instructions)  
    Plus précisément, cet algorithme doit:
    
    * demander deux nombres entiers $a$ et $b$
    * afficher "impossible" dans le Terminal, tant que l'utilisateur saisit des nombres $a$ et $b$ qui ne viennent pas, càd que la condition $a>b$ est fausse. Auquel cas, il faudra que l'algorithme redemande d'entrer de nouveau les nombres $a$ et $b$, et ce, tant que l'utilisateur n'aura pas entré des valeurs convenables.
    * afficher "Merci!" dans le Terminal, si jamais la réponse de l'utilisateur convient (càd que la condition $a>b$ est vraie)

!!! ex "Division Euclidienne :rocket::rocket::rocket:"
    On se donne deux nombres entiers positifs $a$ et $b$. On supposera par la suite que le nombre $a$ est supérieur ou égal à $b$ (cf. exercice précédent)
    Écrire un algorithme qui calcule le quotient $q$ et le reste $r$ :

    * **uniquement grâce à des soustractions** pour déterminer le quotient:  
      <env>Rappel</env> Le quotient $q$ est le nombre maximal de fois que l'on peut soustraire l'entier $b$ de l'entier $a$ tout en restant positif ($\ge 0$)
    * On s'autorise à réaliser une multiplication pour déterminer le reste, grâce à la formule:  
    <center><enc>$r=a-q\times b$</enc></center>

!!! ex "Algorithme, dit *Babylonien*, d'approximation d'une Racine Carrée :rocket::rocket::rocket::rocket:"
    L'algorithme suivant, dit *algorithme Babylonien*, détermine une approximation $r$ de $\sqrt n$ (où $n$ est un entier donné, par exemple $n=2$), obtenue par le calcul de plusieurs approximations $r$ successives, le tout en contrôlant le résultat (l'approximation $r$ finale) avec une précision donnée:  

    * On part d'une première approximation (initialisation): $r=1$  
    (cette valeur initiale $r=1$ peut être *très lointaine* de la vraie valeur de $\sqrt n$)
    * On calcule l'approximation suivante de $r$, par l'**affectation** suivante:  
    <center><enc>$r=\dfrac 12 \left( r+\dfrac nr \right)$</enc></center>
    * puis on réitère cette même affectation autant de fois que nécessaire
    * Il n'est pas (du tout) évident que les valeurs successives de $r$ ainsi obtenues s'approchent de plus en plus de la vraie valeur de $\sqrt n$ (les maths, c'est un métier :stuck_out_tongue_winking_eye:), mais nous accepterons ce résultat. Par contre, nous souhaitons contrôler à l'avance la **précision** de notre approximation : Quelle **condition d'arrêt** de la boucle while de cet algorithme proposez-vous pour que la précision de l'approximation finale $r$ de $\sqrt n$ soit (au pire) d'un millième? (on pourra utiliser la fonction valeur absolue `#!python abs()` qui est une fonctionnalité native de la Bibliothèque Standard)

!!! ex "Logarithme *Binaire* (en base 2) :rocket::rocket:"
    1. Écrire une fonction `#!python log2(n:int)->int` qui :  

        * accepte en entrée un entier `n`
        * renvoie en sortie le premier entier `p` tel que $2^p > n$
    Cette valeur $p$ sera notée <enc style="margin-right:0.5em;">$p=\lceil \log_2(n) \rceil +1$</enc> par la suite  
    2. Combien de bits sont-ils nécessaires pour coder un nombre $n$ ?  
    (ici, on attend une formule en fonction de la variable $n$)  
    3. Par exemple, combien de bits précisément sont nécessaires pour coder le nombre $1\,534\,876\,925$ ?