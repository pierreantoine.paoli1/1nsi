# 1NSI: Réseaux - Modems

!!! def "Modem"
    Un <bred>modem</bred> :fr: :gb:, ou <b>MO</b>dulateur / <b>DEM</b>odulateur, qui convertit un signal entrant, reçu via un canal non numérique (*signal analogique*: câble téléphonique ou DSL[^1], ou *signal lumineux*:fibre optique), en un signal numérique (des bits), avec le protocole Ethernet), et **permettant d'accéder à internet**. C'est un convertisseur dans les deux sens:
    
    * <bred>MO</bred>**dulation** : le **signal numérique** (les bits) de l’ordinateur vers le **signal modulé** / **signal analogique**, transmissible par un réseau analogique et 
    * <bred>DEM</bred>**odulation** (Réciproquement): le signal analogique (dit signal modulé, ou ligne téléphonique) vers un signal numérique

Les modems ont été utilisés pour la première fois dans le système américain de défense aérien *SAGE* à la fin des années $1950's$. Les premiers modèles commerciaux datent des années $1970's$. Cette technologie a été vieillissante pendant un temps, mais depuis la fin des années $1990$, de nombreuses normes de télécommunications sont apparues et, donc autant de nouveaux types de modems : RNIS / ISDN, ADSL, GSM, GPRS, Wi-Fi, Wimax... Depuis l'avènement de la **voix sur IP**, les données modulées peuvent être également transmises dans une communication voix encodée sans aucun type de compression via un modem.

En pratique, votre Box contient: un modem, un switch, un routeur.

[^1]: [ADSL / VDSL]: L’ADSL, et son évolution le VDSL est une technologie qui permet de faire passer des données numériques (des bits) par la paire de cuivre d’une ligne téléphonique depuis un répartiteur central appelé NRA.