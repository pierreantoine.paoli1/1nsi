# 1NSI : Réseaux - Câbles

Le <bred>connecteur RJ45</bred> (RJ pour **Registered Jack**) permet de relier entre eux différents appareils de communication. Il est constitué de $4$ paires de **fils torsadés en cuivre** serties dans une broche en plastique :

* deux paires de fils pour recevoir les signaux de données 
* deux paires pour émettre des signaux de données 

Les $2$ fils dans chaque paire doivent être **torsadés entre eux** sur toute la longueur du segment **pour améliorer la qualité du signal**. Le câblage RJ45 est utilisé dans les réseaux Ethernet. Il est destiné à la circulation de courants faibles, par opposition au courant électrique distribué par les fournisseurs d'électricité.

Un câble RJ45 est caractérisé par son type de blindage et sa catégorie. 

![Câble Prise RJ45]( ../../img/cable_prise_RJ45.png){width=32% align=left}
![Câble Prise RJ45]( ../../img/cableRJ45.png){width=31%}
![Câble Prise RJ45]( ../../img/cableRJ45_denude.png){width=28% align=right}

## Câbles RJ45 Droits vs Croisés

Il existe des câbles RJ45 **droits** et **croisés**. 
Les câbles croisés étaient (avant $2008$) principalement utilisés pour raccorder directement : deux hubs, deux switchs, ou bien $2$ PC. Maintenant, ces appareils n'ont plus besoin de câbles croisés car ils sont capables de croiser électroniquement. On utilisere donc désormais des câblages RJ45 droits.

## Normes $T568A$ et $T568B$

Le câble droit est désormais utilisé pour relier des équipements différents ou identiques.
Le câble droit est normé par les **normes T568A** et **T568B**. Les deux normes peuvent être utilisées au choix. Un même câble doit être câblé dans une seule norme. Dans la majorité des réseaux, le câblage est réalisé avec la norme **T568A**. 

![Deux Normes RJ45 : T568A vs T568B]( ../../img/cableRJ45_normeT568A.jpg){.center}

## Blindages de câbles

Dans le choix du câble, il est aussi important de tenir compte du **blindage**, qui protège contre les interférences électromagnétiques et offre la possibilité de passer des débits supérieurs :
Selon la norme sur laquelle l'installation est réalisée, différents types de cables peuvent utilisés:

* **Le cable RJ45 UTP** *(Unshielded Twisted Pair*) est un câble RJ45 non blindé, non écranté. = Aucun blindage.

* **Le cable RJ45 FTP** (*Foiled Twisted Pair*) est un câble RJ45 dont la gaine du câble est blindée avec de l'aluminium.
 
* **Le cable RJ45 STP** (*Shielded Twisted Pair*) est un câble RJ45  dont   chaque paire est blindée par de l'aluminium mais pas la gaine.

* **Le cable RJ45 SFTP** (*Shielded Foiled Twisted Pair*) est un câble RJ45 dont la gaine est blindée par de l'aluminium et du cuivre et chaque paire est blindée.

* **Le cable RJ45 SSTP** (*Shielded and Shielded Twisted Pair*) est un câble RJ45 blindé paire par paire avec un blindage autour.

![Blindage de Câbles]( ../../img/blindageCables.png){.center}

## Catégories de câbles

* **Catégorie 1** : Uniquement utilisé pour la téléphonie. Abandonné.
* **Catégorie 2** : Débit $4$ Mbit/s, fréquence $2$ MHz. Utilisé sur les réseaux de type Token Ring. Abandonné.
* **Catégorie 3** : Débit $4$ Mbits/s, fréquence $16$ MHz. Utilisé aujourd’hui pour la téléphonie sur le marché commercial aussi bien pour les lignes analogiques ou numériques.
Abandonné pour les réseaux. Le câblage de catégorie $3$ est déployé massivement au début des années $1990$.
* **Catégorie 4** : Débit $16$ Mbits/s, fréquence $20$ MHz. Abandonné.
* **Catégorie 5** : Débit $100$ Mbits/s, fréquence $100$ MHz. Normalisé en $1991$. Le câblage de catégorie $5$ est déployé au début des années $2000$.
* **Catégorie 5e (Classe D)** : Débit jusqu’à $1$ Gbits/s, fréquence $125$ MHz. Normalisé en $2000$. Gestion de la téléphonie et de l’informatique.
Ce type de câblage a presque entièrement remplacé la catégorie 5 dans les années $2005$-$2010$. C’est aujourd’hui la catégorie de câblage la plus présente sur les réseaux.
* **Catégorie 6 (Classe E)** : Débit $1$ Gbits/s, possibilité de $10$ Gbit/s jusqu’à $50$ m, fréquence $250$ MHz. Normalisé en $2002$. De $2010$ à $2014$, la catégorie $6$ est la référence de câblage.
A partir de la catégorie 6, le réseau est moins sensible aux interférences.
* **Catégorie 6a** : Débit $10$ Gbit/s, fréquence $500$ MHz. Normalisée en $2008$. Gestion de la téléphonie et de l’informatique.
La catégorie $6$a offre actuellement le meilleur compromis, en cas de câblage c’est la catégorie à mettre en place.
* **Catégorie 7 (Classe F)** : Débit $10$ Gbit/s, $100$ Gbits/s sur $15$ m, $40$ Gbit/s sur $50$ m, fréquence $600$ MHz. Permet l’acheminement d’un signal de télévision.
Le connecteur est désormais de type GG$4$. Très peu utilisé à cause de son incompatibilité au connecteur RJ45.
* **Catégorie 7a (Classe FA)** : Débit jusqu’à $100$ Gbit/s, fréquence1 GHz. Permet l’acheminement d’un signal de télévision. Le connecteur est de type GG4. Très peu utilisé à cause de son incompatibilité au connecteur RJ45.

La catégorie et le blindage du câble sont indiqués sur le câble, c’est une obligation de la norme.

## Aller plus loin avec les câbles:

* [Comparatif des catégories de câbles](http://customcable.ca/cat5-vs-cat6/)
* [Notions Informatiques, Câbles de Réseaux](http://notionsinformatique.free.fr/reseaux/cable.html)
* [abcreseau.blogspot.com](https://abcreseau.blogspot.com/2017/10/cables-et-prises-rj-45.html)
* [Ac Grenoble, Ecole Entreprise](http://www.ac-grenoble.fr/ecole.entreprise/CRGE/cteressources/vdi/4_Connectique.pdf)
