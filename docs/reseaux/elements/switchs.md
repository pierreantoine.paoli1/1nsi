# 1NSI: Réseaux - Switchs :gb: ou Commutateurs :fr:

Le <bred>Switch</bred> ou <bred>Commutateur</bred> est un équipement réseau largement déployé de nous jours dans les réseaux locaux (LAN) des entreprises, des établissement scolaires et domestiques : Votre box familiale est un **modem**/**routeur**/**switch**. Plus récent que (/remplaçant) le Hub et le Bridge. Il admet un nombre de ports/interfaces qui s'étale de quelques uns (switchs domestiques) à des dizaines/centaines (switchs professionnels)

![Bbox Fibre]( ../../img/bbox-fibre.png){width=80% .center}

![Bbox Fibre]( ../../img/bbox-fibre-arriere.png){width=80% .center}

<figure>
<figcaption>
Bbox fit fibre (modem+routeur+switch)
</figcaption>
</figure>

![Switch Netgear GS108]( ../../img/switch-netgear.png){width=80% .center}

<figure>
<figcaption>
Switch Domestique Netgear GS108
</figcaption>
</figure>

[![Switch Cisco]( ../../img/switch-cisco.png){width=100% .center}]( ../../img/switch-cisco.png)

<figure>
<figcaption>

Switch Professionel Cisco SG250-50<br/>
à 50 emplacements, à placer dans un Rack: <em>rackable</em>

</figcaption>
</figure>

## Définition

!!! def "Switch ou Commutateur"
    Un <bred>Switch</bred> :gb: :fr: ou <bred>Commutateur</bred> :fr: est un matériel réseau qui **analyse** toutes les informations (trames) reçues :

    * **avec filtre** : les données sont filtrées afin de les aiguiller uniquement sur les bons ports/interfaces de sortie 
    * **avec mémoire** : les adreses IP des matériels déjà connectés (au moins une fois) sont stockées dans une table

On parle de <bred>commutation</bred> (de paquets) ou de <bred>réseaux commutés</bred>.
C'est une sorte de prise multiple intelligente d'un réseau, qui peut servir dans un réseau LAN à au moins deux choses:

* soit à multiplier le nombre de prise ethernet, si vous en manquez, 
* soit à apporter des prises ethernet dans un endroit de votre domicile ou de votre bureau où il n’y en a pas.

!!! pte "Table MAC & Switch"
    Un Switch contient une base de données appelée <bred>Table MAC - Medium-Access-Control</bred> ou <bred>table CAM - Content-Addressable-Memory</bred> qui est une table de correspondance entre les ports/interfaces et les adresses MAC des sources arrivant sur ces ports.

## Switchs & Modèle OSI

!!! pte "Switch & Modèle OSI"
    Le Switch est un matériel **de niveau $1$ et $2$ du modèle OSI** (les deux couches les plus basses: la couche Physique et la couche Liaison de Données): En pratique, cela veut dire qu'un switch n'a besoin de connaître que l'**adresse MAC** du destinataire pour envoyer les trames uniquement sur le bon port/interface.

!!! def ":warning: Switchs administrables"
    ATTENTION, il existe des switchs plus évolués, dits <bred>switchs administrables</bred>, qui permettent de gérer (créer, configurer, maintenir) certains paramètres et options via une interface web, en particulier de gérer <bred>VLAN - Virtual Local Network</bred> ou <bred>Sous-réseaux Locaux Virtuels</bred>. Ces switchs administrables sont des matériels de niveau $3$ du modèle OSI: par rapport à un switch normal, ils ont besoin/accès également des paquets de la couche réseau. 
    
:warning: Dans toute la suite, nous n'utiliserons pas ces switchs de niveau $3$. Ils sont juste cités pour information. Nous nous restreindrons donc par la suite à des **switchs usuels de niveau $2$** (sauf spécifiquement indiqué dans un énoncé)

## Fonctionnement

<env>Initialisation</env>  
Au départ les tables sont vides, le switch ne sachant pas encore quels appareils sont connectés sur chaque port.

![Switch : Tables Vides]( ../../img/switch-initial.png){width=80% .center}

<env>Trame Envoyée du PC1 vers PC4</env>  

![Switch : Trame Envoyée de PC1 à PC4]( ../../img/switch-pc1-vers-pc4.png){width=80% .center}
:one: la trame sort de la carte réseau de PC1 avec:

* adresse MAC source = AAAA.AAAA.AAAA
* adresse MAC destination = DDDD.DDDD.DDDD

:two: la trame arrive sur le port E0 du switch SW1

* le switch extrait l’adresse MAC source et l’insère dans sa table (cf schéma). Maintenant le switch sait que pour joindre cette adresse MAC (AAAA.AAAA.AAAA), il doit commuter les trames vers le port E0. Cette information lui servira donc pour le retour de la trame.
* puis le switch extrait l’adresse MAC destination (DDDD.DDDD.DDDD) et la compare à sa table: aucune entrée trouvée donc ne sachant pas où envoyer la trame, il la diffuse sur tous les ports excpetés le port de réception E0.

:three: la trame arrive sur le port E2 du switch SW2

* le switch extrait l’adresse MAC source et l’insère dans sa table (cf schéma). Maintenant le switch sait que pour joindre cette adresse MAC (AAAA.AAAA.AAAA), il doit commuter les trames vers le port E2. Cette information lui servira donc pour le retour de la trame.
* puis le switch extrait l’adresse MAC destination (DDDD.DDDD.DDDD) et la compare à sa table: aucune entrée trouvée donc ne sachant pas où envoyer la trame, il la diffuse sur tous les ports excepté le port de réception E2.

:four: la trame arrive sur la carte réseau du PC4: gagné pour la trame aller!

<env>Trame Réponse du PC4 vers PC1</env>  

![Switch : Trame Réponse de PC4 à PC1]( ../../img/switch-pc4-vers-pc1.png){width=80% .center}

Le fonctionnement est le même que précédemment. On remarque que lorsque la trame arrive sur les switchs, ils insèrent l’adresse MAC source DDDD.DDDD.DDDD dans leur table.

Puis ils extraient l’adresse MAC destination (AAAA.AAAA.AAAA) et la compare à leur table et là ils savent où se situe cette adresse MAC: port E2 pour le switch SW2 et port E0 pour le switch SW1. Ils n’ont plus qu’à commuter la trame **UNIQUEMENT** sur le port en question.

<env>Trame Envoyée du PC2 vers PC4</env>

![Switch : Envoi Trame de PC2 à PC4]( ../../img/switch-pc2-vers-pc4.png){width=80% .center}

En générant petit à petit du trafic entre les différents PC, les tables MAC des switchs sont se remplir. L’objectif est de ne plus diffuser les trames vers tous les ports mais uniquement vers un seul port, celui où se situe le PC de destination.

<env>Trame Envoyée du PC2 vers PC3</env>

![Switch : Envoi Trame de PC2 à PC4]( ../../img/switch-pc2-vers-pc4.png){width=80% .center}

*L’avantage* dans un environnement Microsoft est que les PC sont très verbeux, avant même qu’on ouvre une session Windows, plusieurs trames sont envoyées dans le réseau. Donc les switchs remplissent très rapidement leur table MAC.

<env>Trame Envoyée du PC3 vers PC1</env>

![Switch : Envoi Trame de PC3 à PC1]( ../../img/switch-pc2-vers-pc4.png){width=80% .center}

On dit que <bred>le switch a convergé</bred> quand sa table MAC contient toutes les adresses MAC se trouvant dans le réseau (des PC, des imprimantes, des bornes Widi, des serveurs,…).

Dans le schéma ci dessus, on voit bien que les adresses des 4 PC sont bien dans chacune des tables de SW1 et SW2.

Au final, lorsqu’une trame arrive sur SW1 ou SW2, ils sauront exactement où commuter cette trame.

<env>Quelques remarques</env>

* la table MAC est effacée à chaque reboot du switch
* la table MAC a une taille finie. Par exemple, sur un Cisco 2950, c’est 8000 entrées. Large…
* ce fonctionnement d’apprentissage des adresses MAC est vulnérable à certaines attaques comme par exemple la saturation de table MAC

## Switchs & Domaine Diffusion / Broadcast

!!! pte "Switchs & Domaines de Diffusion"
    1. Un Switch considère par défaut que **toutes ses interfaces sont dans le même LAN** et donc dans le **même domaine de broadcast/ diffusion**
    2. :warning: On peut modifier ce comportement sur **certains Switchs** (dits *administrables*, de niveau $3$), en créant des <bred>VLAN - Virtual LAN</bred> [^1] $^ {,}$ [^2], mais ceci est une aute histoire [^3], et nous nous restreindrons  à des switchs de niveau $2$...

![Switch : Domaine de Diffusion]( ../../img/switch-domaine-diffusion.png){width=80% .center}

## Switchs & Domaine Collision

!!! pte "Switchs & Domaines de Collision"
    Un Switch considère par défaut que **chacune de ses interfaces est dans un domaine de collision différent**, en effet, il n'envoie des données que vers le bon port/interface.

![Switch : Domaine de Collision]( ../../img/switch-domaine-collision.png){width=80% .center}

## Résumé

![Switch : Domaine de Collision]( ../../img/switch-domaine-diffusion-collision.png){width=80% .center}

!!! pte "A Retenir"
    * Le Switch est un **équipement utilisé en pratique**
    * Toutes les machines connectées à un Switch sont dans un même **Domaine de Diffusion / Broadcast**
    * Chaque port/interface d'un Switch définit un **Domaine de Collision distinct/différent**

[^1]: [Qu'est-ce qu'un VLAN?](https://eu.dlink.com/fr/fr/support/faq/knowledge/qu_est_ce_qu_un_vlan)
[^2]: [Qu'est-ce qu'un VLAN?](https://blog.devensys.com/pourquoi-creer-des-vlans/)
[^3]: [A quoi ça sert des switchs avec routage de niveau 3?, bouchecousue.com](https://bouchecousue.com/blog/a-quoi-ca-sert-les-switches-avec-routage-de-niveau-3/)
