# 1NSI: Réseaux - Racks et Baies de Brassage

Ce sont des armoires techniques avec de nombreux emplacements spécifiques (alimentations, etc..) pour empiler du matériel réseau (switchs, routeurs, etc..)

![Rack Wifi France]( ../../img/rack-wifi-france.png){width=100% .center}

<figure>
<figcaption>

Rack Wifi France

</figcaption>
</figure>

![Baie de Brassage]( ../../img/baie-brassage.jpg){width=100% .center .box}

<figure>
<figcaption>

Baie de Brassage

</figcaption>
</figure>