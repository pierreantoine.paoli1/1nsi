# 1NSI: Réseaux - Le Modèle TCP/IP

## Prérequis

<iframe width="100%" height="600" src="https://www.youtube.com/embed/s18KtOLpCg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## :fa-university: Histoire : ARPANET

L’origine du **modèle TCP/ IP** remonte au **réseau ARPANET**. ARPANET est un réseau de télécommunication conçu par l’<bred>ARPA - Advanced Research Projects Agency</bred> :gb:, l’agence de recherche du ministère américain de la défense ou <bred>DoD - Department of Defense</bred> :gb:. Outre la possibilité de connecter des réseaux hétérogènes, ce réseau devait résister à une éventuelle guerre nucléaire, contrairement au réseau téléphonique habituellement utilisé pour les télécommunications mais considéré trop vulnérable. Il a alors été convenu qu’ARPANET utiliserait la technologie de **commutation par paquet** (mode **datagramme**), une technologie émergente prometteuse. C’est donc dans cet objectif et ce choix technique que les **protocoles TCP** et **IP** furent inventés en $1974$. L’ARPA signa alors plusieurs contrats avec les constructeurs (BBN principalement) et l’université de Berkeley qui développait un Unix pour imposer ce standard, ce qui fut fait.

## Qu'est-ce que TCP / IP ?

Le modèle <bred>TCP/IP</bred> **- Transmission Control Protocol / Internet Protocol** :gb: est une <bred>architecture réseau</bred>, càd une manière (en fait la manière standard) de penser et de modéliser la communication dans un réseau, et inter-réseaux (principalement internet), autour des deux protocoles **TCP** et **IP** qui y jouent un rôle prépondérant. On parle parfois *DU* protocole TCP/IP pour désigner l'ensemble de ces deux protocoles :

* un protocole de Transport (niveau $4$), <bred>TCP - Transmission Control Protocol</bred> des données  par **paquets**, documenté par la RFC 793[^1]
* un protocole Réseau (niveau $3$) <bred>IP - Internet Protocol</bred> pour identifier et nommer unifirmément les Hôtes interconnectés

Ce qu’on entend par "modèle TCP/IP", c'est en fait une architecture réseau en $4$ couches dans laquelle les protocoles TCP et IP jouent un rôle prédominant, car ils en constituent l’implémentation la plus courante. Par abus de langage, TCP/IP peut donc désigner deux choses : le modèle TCP/IP et la suite de deux protocoles TCP et IP.

Le modèle TCP/IP est (né d') une implémentation pratique, c'est pourquoi il s’est progressivement imposé comme modèle de référence en lieu et place du modèle OSI, qui est quant à lui, plutôt un modèle théorique et académique.

Il possède la particularité d'être <bred>routable</bred>, comprendre *"redirectionnable"*, via des machines appelées <bred>routeurs</bred> :fr: ou <bred>routers</bred> :gb:, grâce à des identifiants réseau appelés des <bred>adresses IP</bred> qui servent à identifier de manière unique les machines et les réseaux. Disposer de telles fonctionnalités requiert un <bred>plan d'adressage</bred> spécifique.

![TCP/IP](../img/tcp_ip_bis.gif){.center}

## Explication par Couches

### Couche Application

Elle est la couche de communication qui s’**interface avec les utilisateurs**.
Exemples de **protocoles** et **services applicatifs** : HTTP, DNS, DHCP, FTP, …
Elle s’exécute sur les machines hôtes.

### Couche Transport : TCP - UDP

Elle est responsable du dialogue entre les hôtes terminaux d’une communication.
Les applications utiliseront 

* **TCP pour un transport fiable**, et 
* **UDP pour un service peu fiable**

Les routeurs NAT et les pare-feux opèrent un filtrage au niveau de la couche transport.



Le rôle du service de transport est de transporter les messages de bout en bout, càd de la source jusqu'à la destination, donc d'un bout à l'autre du réseau, sans se préoccuper du chemin à suivre car ce problème a déjà été traité par la couche directement inférieure réseau.

Il y a plusieurs exemples de protocoles de transport. Dans le monde Internet les plus connus sont:

* <bred>TCP - Transmission Control Protocol</bred>
* <bred>UDP - User Datagram Protocol</bred>
* <bred>RTP - Realtime Transport Protocol</bred>

Le choix dépend du type d'application et des services demandés.

#### Protocole TCP

Les applications de **transfert de fichiers**, de **courrier électronique** et de **navigation sur le web** utilisent le protocole TCP.

Le protocole TCP :

* ne requière **PAS de connexion** entre les Hôtes
* est un <bred>protocole fiable</bred> : il fournit des des garanties de transmission
* prend en charge les Corrections d'Erreurs (CRC))
* de retransmission en cas d'erreur. 

Dans le cas de messages longs, le fait de découper un message en paquets plus courts peut donner lieu à la remise des paquets à l'ordinateur de destination dans le désordre. Le **protocole TCP** s'occupe de résoudre ces problèmes, au prix d'une certaine complexité du protocole.

#### Protocole UDP

D'autres applications comme les requêtes aux annuaires éléctroniques (pour obtenir la correspondance entre un nom d'ordinateur et son adresse) ou les applications de gestion de réseau préfèrent utiliser un protocole plus léger mais plus rapide car les messages sont typiquement très courts et en cas d'erreurs ou d'absence de réponse, ils peuvent être répétés sans problèmes. Le **protocole UDP** est typiquement utilisé dans ces cas.

#### Protocole RTP

D'autres applications encore comme la téléphonie et la vidéoconférence sur Internet ont des contraintes de temps réel.  La transmission de la voix et de la vidéo ne peuvent pas tolérer les variations de délais, appelées gigue, dans l'acheminement des paquets car les accélérations et ralentissements qui en résulteraient dans la restitution de la voix ou de l'images nuiraient gravement à la qualité de la transmission. Le **protocole RTP**, qui est utilisé en complément du protocole UDP, traite ces problèmes. 

### Couche Internet : IP

Elle permet de **déterminer les meilleurs chemins** à travers les réseaux en fonction des adresses IPv4 ou IPv6 à portée globale.
Les routeurs transfèrent le trafic IP qui ne leur est pas destiné.

#### Protocole ICMP

Le <bred>protocole ICMP - Internet Control Message Protocol</bred> :gb: est un protocole qui permet de gérer les informations relatives aux erreurs générées au sein d’un réseau IP. Etant donné le peu de contrôles que le protocole IP réalise, il permet, non pas de corriger ces erreurs, mais de faire part de ces erreurs.
Ainsi, le protocole ICMP est utilisé par tous les routeurs, qui l'utilisent pour reporter une erreur (appelé Delivery Problem).  
Un exemple typique d’utilisation du protocole ICMP est la commande `ping`. Lors de l’exécution de cette commande, des informations précises peuvent être obtenues : le temps mis par un paquet pour atteindre une adresse, ou bien un éventuel problème de routage pour atteindre un hôte.

### Couche Accès au réseau : LAN/WAN

TCP/IP ne s’occupe pas de la couche Accès Réseau
Elle organise le flux binaire et identifie physiquement les hôtes
Elle place le flux binaire sur les supports physiques
Les commutateurs, cartes réseau, connecteurs, câbles, etc. font partie de cette couche

Au sens du modèle TCP/IP la couche Accès Réseau est vide, car la pile des protocoles Internet (TCP/IP) est censée “inter-opérer” avec les différentes technologies qui offrent un accès au réseau.

Plus on monte dans les couches, plus on quitte les aspects matériels, plus on se rapproche de problématiques logicielles.

## Encapsulation

Pour transmettre du contenu d’un ordinateur à un autre, l’utilisateur va utiliser un programme qui construit un message enveloppé par un en-tête applicatif, HTTP par exemple. Le message subit une première encapsulation.

Le logiciel va utiliser un protocole de couche transport correspondant pour établir la communication avec l’hôte distant en ajoutant un en-tête TCP ou UDP.

Ensuite, l’ordinateur va ajouter un en-tête de couche Internet, IPv4 ou IPv6 qui servira à la livraison des informations auprès de l’hôte destinataire. L’en-tête contient les adresses d’origine et de destination des hôtes.

Enfin, ces informations seront encapsulées au niveau de la couche Accès qui s’occupera de livrer physiquement le message.

## Avantages du Modèle TCP/IP

* C'est un Standard de fait
* Il est plus ciblé que le modèle OSI
* Plus pratique
* Il s'est imposé par sa simplicité

## Notes et Références

[^1]: [RFC 793, IETF, 1981](https://datatracker.ietf.org/doc/html/rfc793)