# 1NSI: Réseaux - Adressage IP

## Vidéo Introductoire

<center>
<iframe width="100%" height="415" src="https://www.youtube.com/embed/_JuY79L8_qA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

## Adresses IP

### A quoi sert une adresse IP ?

!!! def "Qu'est-ce qu'une adresse IP?"
    Une <bred>adresse IP - Internet Protocol</bred> :gb: est une **suite de caractères** qui identifie de manière **unique** :

    * la **machine/équipement** appelé <bred>hôte</bred> :fr: ou <bred>host</bred> :gb: 
    * le **réseau** (et/ou le **sous-réseau**) dans lequel se trouve la machine

Cette adresse IP **permet à l'hôte de communiquer** ...

* avec un autre hôte appartenant **au même sous-réseau**, **sans passer par un routeur**
* éventuellement, selon les configurations du réseau (des switchs), avec des machines situées **dans d'autres sous-réseaux (du même réseau)**, **sans passer par un routeur, mais en passant par un switch (ou un hub)**
* avec des machines situées **dans d'autres réseaux (et leurs sous-réseaux)**, **en passant par un routeur**

Il existe actuellement deux versions d'adresses IP:

* IPv$4$: Internet Protocol version $4$
* IPv$6$: Internet Protocol version $6$

### Partie Réseau / Partie Sous-Réseau / Partie Hôte

!!! def "Partie Réseau & Partie Hôte d'une adresse IP"
    Pour garantir l'unicité et faciliter la routabilité/acheminement des adresses IP, on décompose l'adresse IP en deux parties:

    * La *1ère partie* (la plus à gauche) d’une adresse IP est utilisée comme **adresse IP du réseau**, ou **ID du Réseau** :fr:, ou **Net ID** :gb: (ou *network ID* ou *network prefix*). 
        * **La partie réseau est commune à l'ensemble des hôtes d'un même réseau**.
    * la *dernière partie* (la plus à droite) comme **adresse IP de la machine Hôte**, ou **ID de l'Hôte** :fr:, ou **Host ID** :gb: 
        * **La partie hôte est unique (à l'hôte) à l'intérieur d'un même réseau**.

    ![Adresse IP Réseaux vs Hôte](../img/ip_reseau_hote.png){.center}

Une bonne question étant alors : **À quelle position exactement se trouve la séparation/frontière entre la partie réseau et la partie hôte?** C'est pour répondre à ce genre de questions qu'on nous utiliserons des <bred>masques de sous-réseaux</bred> :fr: ou <bred>subnet mask</bred> :gb:.

!!! def "Partie Réseau, Partie Sous-Réseau & Partie Hôte d'une adresse IP"
    Par la suite, historiquement, on a inventé la notion de **sous-réseau**, et donc on a inséré une **partie sous-réseau** dans l'adresse IP, la partie hôte étant alors redivisée en deux.
    Toujours pour garantir l'unicité et faciliter la routabilité/acheminement des adresses IP, on décompose l'adresse IP en trois parties:

    * La *1ère partie* (la plus à gauche) d’une adresse IP est utilisée comme **adresse IP du réseau**, ou **ID du Réseau** :fr:, ou **Net ID** :gb: (ou *network ID* ou *network prefix*).
        * **La partie réseau est commune à l'ensemble des hôtes d'un même réseau**.
    * La *2ème partie* (au milieu) d’une adresse IP est utilisée comme **adresse IP du sous-réseau**, ou **ID du Sous-Réseau** :fr:, ou **SubNet ID** :gb: (ou *subnetwork ID* ou *subnetwork prefix*). 
        * **La partie sous-réseau est commune à l'ensemble des hôtes d'un même sous-réseau**.
    * la *dernière partie* (la plus à droite) comme **adresse IP de la machine Hôte**, ou **ID de l'Hôte** :fr:, ou **Host ID** :gb: 
        * **La partie hôte est unique (à l'hôte) à l'intérieur d'un même sous-réseau et aussi à l'intérieur d'un même réseau**.

    ![Adresse IP Réseaux vs Hôte](../img/ip_sous_reseau_hote.png){.center}

Une nouvelle bonne question étant alors : **À quelle position exactement se trouve la séparation/frontière entre la partie sous-réseau et la partie hôte?** Nous utiliserons encore des masques de sous-réseaux pour répondre à cette question, mais commençons par parler des IPv$4$. 

### Adresse IPv$4$

Une **adresse IPv$4$** est une suite de $4$ entiers en notation décimale, d'$1$ octet$=8$ bits chacun, chaque octet étant compris entre $0=\overline{00000000}$ et $255=\overline{11111111}$, et séparés par des `.`  
Une adresse IPv4 peut s'écrire soit en notation Binaire, soit en notation Décimale:

* `11000000.10101000.00000000.00000001` en Binaire  
  Soit au total: $4\times 8$ bits $=32$ bits
* `192.168.0.1` en Décimal  

!!! ex "Combien d'adresses IPv$4$ sur Terre?"

    === "Question"
        Combien d'adresses IPv$4$ existe-t-il en tout ?
    === "Réponse"
        Une adresse IPv$4$ est composée de $32$ bits, donc il existe 
        $2^{32}= 4\,294\,967\,296$ adresses IPv$4$, soit un peu plus de $4$ milliards d'adresses IPv$4$.  
        Cela peut sembler beaucoup, mais n'oublions pas que la Terre compte à ce jour ($2021$) environ $7,9$ milliards d'habitants...  
        Ce qui justifie que les adresses IPv$4$ soient en pénurie actuellement, donc c'est pour cela on a inventé les adresses IPv$6$ pour résoudre cela.

Aller Plus Loin avec IPv4:

* [IETF, RFC 791, pour IPv4, 1981](https://datatracker.ietf.org/doc/html/rfc791)
* [IETF, RFC 3168, pour Encapsulation et TCP, 2001](https://datatracker.ietf.org/doc/html/rfc3168)

### Adresse IPv6

Une **adresse IPv$6$** est une suite de $8$ valeurs héxadécimales de $2$ octets $=16$ bits chacune, chaque valeur (couple de $2$ octets) étant compris entre $0=\overline{00000000}$ et $\#FFFF=FFFF=\overline{1111111111111111}$, et séparés par des `:`  
Une adresse IPv6 peut s'écrire en notation Binaire, ou en notation HexaDécimale:

* `10000000100001:0000111000111011:0:1010010011000010:0:0:1100111101001010:1000000010000110` en Binaire  
  Soit au total: $8\times 16$ bits $=128$ bits, soit $16$ octets
* `2021:0e3b:0000:a4c2:0000:0000:cf4a:8086` en HexaDécimal  

!!! ex "Combien d'adresses IPv6 sur Terre?"

    === "Question"
        Combien d'adresses IPv6 existe-t-il en tout ?
    === "Réponse"
        Une adresse IPv6 est composée de $128$ bits, donc il existe 
        $2^{128}= 340\,282\,366\,920\,938\,463\,463\,374\,607\,431\,768\,211\,456$ adresses IPv6, soit un peu plus de $340$ milliards de milliards de milliards de milliards d'adresses IPv$6$, cela commence à être suffisant.. (pour le moment ??)

:warning: Dans toute la suite de ce cours, sauf mention contraire, nous n'utiliserons que des adresses IPv$4$ :warning:

Aller Plus Loin avec l'IPv6:

* [IETF, RFC 4291](https://datatracker.ietf.org/doc/html/rfc4291)

### Adresses IP Spécifiques

#### Adresse IP d'un (Sous-)Réseau

!!! def "adresse IP d'un (Sous-)Réseau"
    La **toute première adresse IP** d'un (sous-)réseau est appelée l'<bred>adresse IP du (Sous-)Réseau</bred> :fr:  
    En particulier, cette toute première adresse IP d'un réseau NE peut PAS être l'adresse IP d'un Hôte du réseau, ni l'adresse IP d'un sous-réseau (conflit).

Dans le cas d'un <bred>réseau</bred>, cette adresse IP du réseau est composée ainsi:

* une <bred>partie réseau</bred> (quelconque)
* une <bgreen>partie hôte</bgreen> : <bgreen>0000...000</bgreen>

Dans le cas d'un <bblue>sous-réseau</bblue>, c'est plus compliqué, car le tout premier sous-réseau d'un réseau, appelé <bred>sous-réseau zéro</bred> ou <bred>sous-réseau *all-zeros*</bred>, peut avoir avoir une adresse IP qui soit ambigüe entre plusieurs réseaux et sous-réseaux: Pour commencer, l'adresse IP du réseau, et l'adresse IP du sous-réseau zéro sont déja ambigües, mais nous en verrons d'autres illustrations/ambigüités sur des exemples/exercices.

Pour éviter cela, la $RFC950$ préconise de **ne pas compter le tout premier sous-réseau "*all-zeros*" comme un sous-réseau, mais de réserver plutôt cette IP comme adresse IP du réseau**.

!!! exp "Adresse de Réseau"
    L'adresse IP `178.9.134.46` d'un Hôte pourrait appartenir à un réseau d'adresse `178.9.0.0`
    L'adresse `0.0.134.46` pouvant alors signifier l'hôte `134.46` de CE réseau.  
    Les `0` dans l'adresse `0.0.134.46` signifiant "*this*", càd SOI-MÊME comme dans "CE PROPRE RÉSEAU" (cf RFC950[^4])

#### Adresse Broadcast d'un (Sous-)Réseau

!!! def "Adresse IP Broadcast d'un (Sous-)Réseau"
    La **toute dernière adresse IP** d'un (sous-)réseau est appelée l'<bred>adresse IP de Broadcast/(multi)diffusion</bred> :gb: :fr: que l'on utilise pour envoyer un message à TOUS les hôtes du (sous-)réseau.  
    En particulier, cette toute dernière adresse IP d'un réseau NE peut PAS être l'adresse IP d'un Hôte du réseau, ni l'adresse de diffusion d'un sous-réseau (conflit).

Dans le cas d'un <bred>réseau</bred>, cette adresse IP de Broadcast est composée ainsi:

* une <bred>partie réseau</bred> (quelconque)
* une <bgreen>partie hôte</bgreen> : <bgreen>1111...111</bgreen>

Dans le cas d'un <bblue>sous-réseau</bblue>, c'est plus compliqué, car le tout dernier sous-réseau d'un réseau, appelé <bred>sous-réseau *all-ones*</bred>, peut avoir avoir une adresse IP qui soit ambigüe entre plusieurs réseaux et sous-réseaux: Pour commencer, l'adresse IP de broadcast du réseau, et l'adresse IP de Broadcast du sous-réseau all-ones sont déja ambigües, mais nous en verrons d'autres illustrations/ambigüités sur des exemples/exercices.

Pour éviter cela, la $RFC950$ préconise de **ne pas compter le tout dernier sous-réseau "*all-ones*" comme un sous-réseau, mais de réserver plutôt cette IP pour (la compléter en) l'adresse IP de Broadcast du réseau**.

!!! exp "Adresse de Boradcast d'un Réseau"
    L'adresse IP `178.9.255.255` pourrait être interprétée comme l'adresse de diffusion du réseau `178.9.0.0`
    Dans ce contexte, puisque $255=\overline{11111111}$, on peut interpréter l'adresse tous-un/all-ones précédente comme signifiant (vers) TOUS/TOUS LES HÔTES (cf RFC950[^4]).


#### Nombre d'Hôtes d'un (Sous-)Réseau

!!!pte "Soustraire 2 au nombre d'Hôtes d'un sous-réseau"
    Lorsque nous dénombrerons le nombre d'hôtes d'un (sous-)réseau, **il faudra donc toujours OBLIGATOIREMENT soustraire $2$**, car:

    * la toute première adresse IP d'un réseau est **réservée** à l'adresse IP réseau
    * la toute dernière adresse IP d'un réseau est **réservée** à l'adresse IP broadcast/(multi-)diffusion

#### Nombre de Sous-réseaux d'un Réseau

!!!pte "Soustraire 2 au nombre de Sous-Réseaux d'un réseau"
    Lorsque nous dénombrerons le nombre de Sous-Réseaux d'un réseau, **il faudra donc toujours OBLIGATOIREMENT soustraire $2$**, car:

    * l'adresse IP du tout premier sous-réseau **all-zeros** est **réservée** à l'adresse IP réseau
    * l'adresse IP de broadcast du tout dernier sous-réseau **all-ones** est **réservée** à l'adresse IP de broadcast du réseau

#### L'Adresse IP de Loopback / `127.0.0.1` / `::1`

* **En notation IPv4**: l'adresse IP <bred>127.0.0.1</bred>, appelée interface de <bred>loopback</bred>, est une **adresse IP spécifique et réservée** qui désigne *soi-même*, càd l'hôte lui-même. Cette adresse IP est souvent utilisée à des fins de tests.
* **En notation IPv6**: l'adresse IP de l'interface de <bred>loopback</bred> est `::1`
* En ce qui concerne les **DNS - Domain Name Server** (tables de correspondance : nom de domaine $\Leftrightarrow$ adresse IP): le nom de domaine (qui est un *[FQDN - Fully Qualified Domain Name](https://fr.wikipedia.org/wiki/Fully_qualified_domain_name)*) <bred>localhost</bred> pointe vers cette adresse de loopback. En pratique, cela veut dire que lorsque l'on a installé et activé un serveur web sur un hôte, alors le nom de domaine **localhost** (à entrer dans la barre d'adresse du navigateur) pointe vers l'hôte lui-même, càd **vers le serveur web local**.

## Masques de Sous-Réseau

### A quoi sert un masque de Sous-Réseau ?

**Avant toute chose**, pour bien comprendre les masques de sous-réseau, **il faut commencer par écrire les adresses IP en binaire** (et non pas en Décimal).

Par exemple, disons que l'on s'intéresse à l'adresse IPv4 suivante : 

* IPv$4$ en Décimal : `192.168.46.145`, ce qui s'écrit
* IPv$4$ en Binaire : `11000000.10101000.00101110.10010001`

L'idée des <bred>masques de sous-réseau</bred> :fr: ou <bred>subnet mask</bred> :gb: est de préciser la frontière entre la partie réseau et la partie hôte d'une adresse IP

### Masque d'un Réseau

La frontière peut théoriquement se situer à n'importe quelle position (du 1er bit au 32 ème).

!!! def "Masque de Sous-Réseau"
    Le <bred>Masque de Sous-Réseau</bred> :fr: (d'un réseau) ou <bred>subnet mask</bred> :gb: est l'adresse IP de sorte qu'en binaire, on choisisse :

    * des <bred>1</bred> partout dans la <bred>partie Réseau</bred>
    * des <bgreen>0</bgreen> partout dans la <bgreen>partie Hôte</bgreen>

![Adresse IP et Masque de Sous-Réseau](../img/ip_reseau_masque_sous_reseau.png){.center}

### Masque d'un Sous-Réseau

De plus, comme son nom l'indique le masque de SOUS-réseau, lorsqsu'il y en a un, et pas seulement le réseau :

!!! def "Masque de Sous-Réseau"
    Le <bred>Masque de Sous-Réseau</bred> :fr: (d'un <b>sous-</b>réseau) ou <bred>subnet mask</bred> :gb: est l'adresse IP de sorte qu'en binaire, on choisisse :

    * des <bred>1</bred> partout dans la <bred>partie Réseau</bred>
    * des <bblue>1</bblue> partout dans la <bblue>partie Sous-Réseau</bblue>
    * des <bgreen>0</bgreen> partout dans la <bgreen>partie Hôte</bgreen>

![Adresse IP et Masque de Sous-Réseau](../img/ip_sous_reseau_masque_sous_reseau.png){.center}

>Puisque les bits qui identifient le sous-réseau sont caractérisés par un masque de sous-réseau (masque binaire), ils n'ont donc pas obligation d'être adjacents dans l'adresse IP. Cependant, nous recommendons que les bits de la partie sous-réseau soient contigüs et placés dans les bits de poids fort de l'adresse locale.
>
> RFC950, page 6

### Pourquoi le nom "Masque" ?

Dans la vraie vie, un masque (vénitien, de zorro, etc..) désigne une surcouche du visage (une sorte de calque) qui:

* laisse entrevoir certaines parties du visage (les "*trous*")
* cache de manière opaque d'autres parties (les parties *"cartonnées"*)

Par analogie avec ce sens initial, au sens numérique du terme (par ex. dans les calques des logiciels de retouches d'images, ou de montage de vidéos), un masque désigne un calque supplémentaire d'une image/donnée numérique de sorte que:

* certaines zones (de pixels) numériques passent au travers du masque sans être modifiées. 
* d'autres zones au contraire sont totalement filtrées, par exemple les couleurs des pixels sont mises à noir, ou transparent

!!! pte "Le masque de Sous-Réseau est un 'Masque'"
    Le masque de Sous-Réseau est bien un **masque** au sens où, l'adresse IP composée avec le masque de sous-réseau, via une opération binaire de *ET LOGIQUE* (&) donne (/ ne laisse passer que) la partie réseau de l'adresse :
    
    <div class="nolines">
    ```
         Adresse IP  
      &  masque de sous-réseau  
         ----------------------  
     =   partie Réseau (et sous-réseau s'il y en a un)
    ```
    </div>

Intuitivement:

* la composition des <bred>1</bred> du masque de sous-réseau, avec un ET LOGIQUE (&), laisse passer les bits de la partie réseau sans les modifier (les trous du masque qui laissent passer la lumière)
* la composition des <bgreen>0</bgreen> du masque de sous-réseau, avec un ET LOGIQUE (&), NE laisse passer AUCUN bit de la partie hôte (les zones sombres et épaisses du masque qui ne laissent rien passer)

Cela garantit que / on retrouve que:

!!! pte
    * Toutes les adresses IP des hôtes d'un même (sous-)réseau admettent la même partie (sous-)réseau
    * la seule partie de l'adresse IP qui distinguent deux hôtes d'un même (sous-)réseau sont les parties hôtes

!!! exp
    On considère l'adresse IP suivante: <bred>11000000.10101000.00101</bred><bgreen>110.10010001</bgreen> dans laquelle ou souhaite que la partie <bred>rouge</bred> corresponde à la partie réseau, et la partie <bgreen>verte</bgreen> à la partie hôte. Ainsi:  

    |||
    |:-:|:-:|
    |IPv4 (en Binaire)|<bred>11000000.10101000.00101</bred><bgreen>110.10010001</bgreen>|
    |Masque de Sous-Réseau (en Binaire)|<bred>11111111.11111111.11111</bred><bgreen>000.00000000</bgreen>|
    | Masque de Sous-Réseau (en Décimal) | 255.255.248.0|


!!! ex "Nombres de masques de sous-réseau?"
    === "Questions"
        1. Déterminer le nombre total de masques de sous-réseaux possibles.  
        2. Tous Les écrire sous forme binaire
    === "Corrigé"
        1.  
        2.  

### Que peut-on faire avec un masque de sous-réseau?

Il arrive fréquemment qu'on connaisse déjà le masque de sous-réseau, la question étant alors : que peut-on en faire? Quelles informations peut-on en déduire? Pour le moment, reprenons l'exemple précédent, et supposons qu'on dispose des informations suivantes:

* Une adresse IP : <bred>11000000.10101000.00101</bred><bgreen>110.10010001</bgreen>$=192.168.46.145$ 
* le masque de sous-réseau: <bred>11111111.11111111.11111</bred><bgreen>000.00000000</bgreen> en binaire, soit $255.255.248.0$ en Décimal

Avec ces informations, on peut par exemple répondre aux questions:

* Quelle est la partie réseau de l'adresse IP?
* Quelle est la partie hôte de l'adresse IP?

<center>

| | Notation Binaire | Notation Décimale |
|:-:|:-:|:-:|
| Adresse IP | <bred>11000000.10101000.00101</bred><bgreen>110.10010001</bgreen> | `192.168.46.145` |
| & masque de Sous-Réseau | <bred>11111111.11111111.11111</bred><bgreen>000.00000000</bgreen> | `255.255.248.0` |
| Partie Réseau | <bred>11000000.10101000.00101</bred><bgreen>000.00000000</bgreen> | `192.168.40.0` |
| Partie Hôte | <bred>00000000.00000000.00000</bred><bgreen>110.10010001</bgreen> | `0.0.6.145` |

</center>

### Masques de Sous-Réseaux Classiques

S'il est vrai que la séparation entre partie réseau, et partie hôte peut se faire à n'importe quelle position (entre le 1er et le 32ème bit) de l'adresse IP en binaire, historiquement, on a choisi certaines positions particulières comme masques de sous-réseaux

#### Masque 255.0.0.0 & Notation CIDR /8

$11111111.00000000.00000000.00000000$ Masque de Sous-Réseau (en Binaire)
255.0.0.0 Masque de Sous-Réseau (en Décimal)  

<env>Notation CIDR</env> [^2] On utilise quelquefois la notation `/8` après l'adresse IP du sous-réseau (**notation CIDR**, cf paragraphe ci-dessous) pour indiquer que seuls les $8$ premiers bits les plus à gauche (bits de poids forts) sont à $1$ , et tous les autres (les $32-8=24$ bits les plus à droite) sont à $0$

#### Masque 255.255.0.0 & Notation CIDR /16

$11111111.11111111.00000000.00000000$ Masque de Sous-Réseau (en Binaire)
255.255.0.0 Masque de Sous-Réseau (en Décimal)  

<env>Notation CIDR</env> On utilise quelquefois la notation `/16` après l'adresse IP du sous-réseau (**notation CIDR**, cf paragraphe ci-dessous) pour indiquer que seuls les $16$ premiers bits les plus à gauche (bits de poids forts) sont à $1$ , et tous les autres (les $32-16=16$ bits les plus à droite) sont à $0$

#### Masque 255.255.255.0 & Notation CIDR /24

$11111111.11111111.11111111.00000000$ Masque de Sous-Réseau (en Binaire)
255.255.255.0 Masque de Sous-Réseau (en Décimal)  

<env>Notation CIDR</env> On utilise quelquefois la notation `/24` après l'adresse IP du sous-réseau (**notation CIDR**, cf paragraphe ci-dessous) pour indiquer que seuls les $24$ premiers bits les plus à gauche (bits de poids forts) sont à $1$ , et tous les autres (les $32-24=8$ bits les plus à droite) sont à $0$

### Notation CIDR d'un masque

Cf. La paragraphe sursuivant...

## :fa-university: Classes d'Adresses IP

### Les Classes d'Adresses IP

:fa-university: Historiquement, pour alléger la taille des **tables de routage** (tables d'itinéraires) dans les routeurs des réseaux, et ainsi simplifier le cheminement/routage des *paquets* (petits blocs) de données sur les réseaux, on a créé $5$ <bred>classes d'addresses IP</bred> (des plages d'adresses IP) : chaque adresse IP appartient  ainsi forcément à l'une des $5$ classes: $A$, $B$, $C$, $D$, et $E$.

<center>

| Classe | Bits de départ | Début | Fin | Notation CIDR<br/>par défaut | Masque <br/>de sous-réseau<br/> par défaut |
| :-: | :-: | :-: | :-: | :-: | :-: |
| Classe $A$ | $0$   |  $0.0.0.0$  | $126.255.255.255$ <br/>($127$ est réservé) | $/8$ | $255.0.0.0$ |
| Classe $B$ | $10$  | $128.0.0.0$ | $191.255.255.255$ | $/16$ | $255.255.0.0$ |
| Classe $C$ | $110$ | $192.0.0.0$ | $223.255.255.255$ | $/24$ | $255.255.255.0$ |
| Classe $D$ <br/>(réservée pour </br> le multicast) | $1110$ | $224.0.0.0$ | $239.255.255.255$ |     | $255.255.255.255$ |
| Classe $E$ <br/>(réservées<br/>par l'IETF) | $1111$ | $240.0.0.0$ | $255.255.255.255$ |     | non défini |

</center>
!!! def "Adressage Classful"
    Ce mode d'adressage IP AVEC DES CLASSES est appelé un <bred>adressage classful</bred>

!!! exp "Classe d'une adresse IP"
    1. L'adresse IP `10.40.2.200` est une adresse IP de classe $A$
    2. L'adresse IP `172.24.56.147` est une adresse IP de classe $B$
    3. L'adresse IP `192.168.1.34` est une adresse IP de classe $C$

!!! ex "Nombres de Réseaux et Nombres d'Hôtes selon la Classe"
    === "Classe A"
        1. Combien d'Hôtes dans un réseau de classe $A$ ?
        2. Combien de Réseaux de Classe $A$ existe-t-il?
        ??? "Corrigé"
            1. Dans une adresse réseau de Classe $A$, le masque de sous-réseau est `255.0.0.0`, cela prouve que les $24$ derniers bits correspondent à la partie hôte: cela signifie qu'il existe $2^{24}-2=16777216-2=16\,777\,214$ hôtes distincts dans un réseau de classe $A$  
            2. Puisque le masque de sous-réseau est `255.0.0.0`, seuls les $24$ derniers bits correspondent à la partie hôte. Et parmi les $8$ premiers bits (de $0$ à $255$), seuls les nombres entiers de $0$ à $126$ sont autorisés (car $127$ est réservée), donc il existe $127$ réseaux de classe $A$
    === "Classe B"
        1. Combien d'Hôtes dans un réseau de classe $B$ ?
        2. Combien de Réseaux de Classe $B$ existe-t-il?
        ??? "Corrigé"
            1. Dans une adresse réseau de Classe $B$, le masque de sous-réseau est `255.255.0.0`, cela prouve que les $16$ derniers bits correspondent à la partie hôte: cela signifie qu'il existe $2^{16}-2=65536-2=65\,534$ hôtes distincts dans un réseau de classe $B$  
            2. Puisque le masque de sous-réseau est `255.255.0.0`, seuls les $16$ derniers bits correspondent à la partie hôte. Et parmi les $16$ premiers bits:
            * le premier nombre entier ne peut prendre que des valeurs de $128$ à $191$ inclus donc $191-128+1=64$ valeurs
            * le deuxième nombre ne peut prendre que des entiers de $0$ à $255$, donc $256$ valeurs
            * soit au total: il existe $64\times 256 =16384$ réseaux de classe $B$
    === "Classe C"
        1. Combien d'Hôtes dans un réseau de classe $C$ ?
        2. Combien de Réseaux de Classe $C$ existe-t-il?
        ??? "Corrigé"
            1. Dans une adresse réseau de Classe $C$, le masque de sous-réseau est `255.255.255.0`, cela prouve que les $8$ derniers bits correspondent à la partie hôte: cela signifie qu'il existe $2^{8}-2=256-2=254$ hôtes distincts dans un réseau de classe $C$  
            2. Puisque le masque de sous-réseau est `255.255.255.0`, seuls les $8$ derniers bits correspondent à la partie hôte. Et parmi les $24$ premiers bits :
            * le 1er nombre entier ne peut prendre que des valeurs de $192$ à $223$ inclus donc $223-192+1=32$ valeurs
            * le 2ème et le 3ème nombre peuvent prendre des valeurs entières de $0$ à $255$, donc $256$ valeurs chacun
            * soit au total: il existe $32\times 256\times 256 =2\,097\,152$ réseaux de classe $C$

:fa-university: Bien qu'en théorie, les classes ne soient plus vraiment d'actualité de nos jours, on y fait encore souvent référence pour des notions sur les réseaux. Elles ont depuis, connu plusieurs évolutions:

* Historiquement, ce sont **les classes que l'on a d'abord découpé en Sous-Réseaux** pour mieux router les adresses IP. Il s'agissait d'une amélioration des protocoles de routages, tout en conservant le principe des CLASSES.
* Mais de nos jours, les adresses IP sont routées par des protocoles Classless (SANS CLASSES) dans les routeurs, par ex. RIPv2, OSPF, BGP, etc... On utilise pour cela la <bred>notation CIDR - Classless InterDomain Routing</bred> :gb: ou **Règles de Routage InterDomaine SANS CLASSES** :fr: dont nous reparlerons un peu plus bas.

### Adresses IP Publiques vs Privées

#### Adresse IP Publique

!!! def "Adresse IP Publique"
    Une <bred>adresse IP Publique</bred> est une adresse IP **routable** (sur Internet, ou inter-réseaux).

!!! info "En pratique"
    * Les adresses IP publiques sont les adresses utilisées pour les interfaces externes des routeurs, càd pour l'adresse IP publique d'un réseau (LAN, WAN, entreprises, établissements scolaires, etc.. le tout sur internet)  
    * Une même entité peut (si elle les paye) disposer de plusieurs adresses IP publiques (voire d'une plage d'adresses IP publiques). C'est le cas par exemple pour de grandes entreprises, ou grands établissement scolaires, etc..
    * C'est l'<bred>[IANA](https://fr.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority) - Internet Assigned Numbers Authority</bred> :gb:, qui veille (au niveau mondial) à ce que deux adresses IP publiques ne soient pas attribuées à deux entités distinctes. L'**IANA** est un département de l'<bred>[ICANN](https://fr.wikipedia.org/wiki/Internet_Corporation_for_Assigned_Names_and_Numbers) - Internet Corporation for Assigned Names and Numbers</bred> qui est une autorité, à but non lucratif, de régulation d'internet.

#### Adresse IP Privée

!!! def "Adresse IP Privée"
    Une <bred>adresse IP Privée</bred> est une adresse IP **NON routable** (sur Internet, ou inter-réseaux).

!!! info "En pratique"
    En pratique, Les adresses IP privées sont les adresses utilisées pour les équipements d'un réseau local (LAN): entreprises, établissements scolaires, réseaux domestiques,...

L'IANA/ICANN a décidé que les adresses IP suivantes sont privées, donc ne sont pas routables sur Internet :

<center>

| Classe | Plage d'adresses |
| :-: | :-: |
| Classe $A$ | de $10.0.0.1$ à $10.255.255.254$ |
| Classe $B$ | de $172.16.0.1$ à $172.31.255.254$ |
| Classe $C$ | de $192.168.0.1$ à $192.168.255.254$ |

</center>

:fa-university: Bien qu'historiquement, ces adresses privées soient liées aux classes, **ces plages d'adresses privées sont encore valables de nos jours**.

!!! pte "Classes d'adresses & Adresses IP Privées / Publiques"
    * Les adresses IP de **classe $A$, $B$ et $C$ disposent donc d'adresses privées <bred>ET</bred> d'adresses publiques**
    * Les adresses IP de classe $D$ sont réservées pour le multicast / multidiffusion
    * Les adresses IP de classe $E$ sont réservées par l'IETF, et sont quelquefois utilisés pour des tests

### Découpage d'une Classe en Sous-réseaux

#### Pourquoi découper en Sous-réseaux?

Pour compenser les problèmes de distribution de l'espace d'adressage IPv$4$, la première solution utilisée a consisté à **découper** une classe d'adresses IPv$4$ $A$, $B$ ou $C$ en **sous-réseaux**. Cette technique appelée <bred>subnetting</bred> :gb: ou <bred>découpage en sous-réseaux</bred> :fr: a été formalisée en $1985$ avec le document $RFC950$.

Cette technique est ancienne, mais elle est reste efficace efficace pour les problèmes d'exploitation des réseaux contemporains. **Le découpage en réseaux ou sous-réseaux permet de cloisonner les domaines de diffusion**. Les avantages de ce cloisonnement de la diffusion réseau sont multiples:

* Au quotidien, on évite l'engorgement des liens en limitant géographiquement les annonces de services faites par les serveurs de fichiers. Les services Micro$oft :fa-trademark: basés sur **netBT** sont particulièrement gourmands en diffusion réseau. En effet, bon nombre de tâches transparentes pour les utilisateurs supposent que les services travaillent à partir d'annonces générales sur le réseau. Sans ces annonces par diffusion, l'utilisateur doit désigner explicitement le service à utiliser. Le service d'impression est un bon exemple.

* Il existe quantité de vers et/ou virus dont les mécanismes de propagation se basent sur une reconnaissance des cibles par diffusion. Le ver **Sasser** en est un exemple caractéristique. En segmentant un réseau en plusieurs domaines de diffusion, on limite naturellement la propagation de code malveillant. Le **subnetting** devient alors un élément de la panoplie des outils de sécurité.

!!! ex ""
    Soit l'adresse IP `192.168.14.9`
    Cette adresse est de classe C, donc le masque de sous-réseau *par défaut* est `255.255.255.0` 
    === "Question 1"
        Dans cette question, on choisira ce masque de sous-réseau *par défaut*, 
        a. Combien d'hôtes dans le sous-réseau?
        b. Quelle est la partie réseau de cette adresse IP?
        c. Quelle est la partie hôte de cette adresse IP?
        d. Quelle est l'adresse IP du sous-réseau ?
        e. Quelle est l'adresse IP de broadcast/diffusion ?
    === "Question 2"
        Dans cette question, on décide que cette adresse IP appartienne plutôt à sous-réseau disposant de $2^4-2=16-2=14$ hôtes ("16 hôtes" moins les 2 adresses réservées de sous-réseau et de broadcast), 
        a. Quel est le masque de sous-réseau qu'il faut choisir? (en binaire, puis en décimal)
        b. Quelle est la partie réseau de cette adresse IP?
        c. Quelle est la partie hôte de cette adresse IP?
        d. Quelle est l'adresse IP du sous-réseau ?
        e. Quelle est l'adresse IP de broadcast/diffusion ?

#### Premier (all-zeros) et Dernier (all-ones) Sous-Réseaux

!!! def "Premier (all-zeros) et Dernier (all-ones) Sous-Réseaux d'un Réseau"
    Si une adresse réseau est découpée en sous-réseaux (*subnetting*), alors:  
    1. Le tout premier sous-réseau, càd celui dont la partie sous-réseau n'est composé que de <bblue>0</bblue>, est appelé le <bred>sous-réseau zéro</bred> ou <bred>sous-réseau all-zeros</bred>.  
    L'adresse IP du sous-réseau zéro (all-zeros) est l'<bred>adresse IP du réseau</bred> : Cela crée un conflit (cf citation RFC ci-dessous).
    2. Le tout dernier sous-réseau, càd celui dont la partie sous-réseau n'est composé que de <bblue>1</bblue>, est appelé le <bred>sous-réseau all-ones</bred>.  
    L'adresse IP de broadcast du dernier sous-réseau (all-ones) est l'<bred>adresse IP de broadcast du réseau</bred> : Cela crée un conflit (cf citation RFC ci-dessous).

> Il est utile de préserver et étendre l'interprétation de ces adresses spécifiques (NDT: de réseau et de broadcast) dans le contexte des sous-réseaux. Cela veut dire que les valeurs toutes-zéros (sous-réseau all-zeros) et/ou les valeurs toutes-un (sous-réseau all-ones) ne doivent être assignées à AUCUN sous-réseau.
>
> RFC950, bas de la page 6

C'est pourquoi, le nombre de sous-réseaux obtenus en utilisant $3$ bits dans la partie de sous-réseau, vaut $2^3-2=8-2=6$ et non pas $8$. 

Aller Plus Loin:

* [IETF, RFC950, Subnetting, 1985](https://datatracker.ietf.org/doc/rfc950/)
* [IETF, RFC1878, Subnetting of Classes, 1995](https://datatracker.ietf.org/doc/rfc1878/)
* [All-zeros & all-ones subnets, CISCO](https://www.cisco.com/c/en/us/support/docs/ip/dynamic-address-allocation-resolution/13711-40.html)

## Adressage Classless

Dès $1993$, Internet a connu une croissance exponentielle, si bien qu'on pouvait prévoir que le nombre d'adresses IPv4 seraient insuffisantes, en particulier celles de classe B. Ceci est principalement dû au découpage fixe de l'espace d'adressage IPv4 en classes (Classe A, Classe B, Classe C) qui fige le nombre de réseaux possibles et le nombre d'hôtes maximum par réseau.

Lorsque l’on utilise un **adressage Classful (AVEC DES CLASSES)**, les masques de sous-réseaux ne sont pas envoyés sur le réseau. Les équipements réseaux utilisent donc des masques de sous-réseaux **par défaut** qui sont les suivants :

| Classe | Masque de Sous-Réseau</br> par Défaut (Décimal) | Masque de Sous-Réseau</br> par Défaut (Binaire) | Notation CIDR<br/> du Masque par défaut |
|:-:|:-:|:-:|:-:|
| Classe $A$ | $255.0.0.0$ | $11111111.00000000.00000000.00000000$ | $\\/8$ |
| Classe $B$ | $255.255.0.0$ | $11111111.11111111.00000000.00000000$ | $\\/16$ |
| Classe $C$ | $255.255.255.0$ | $11111111.11111111.11111111.00000000$ | $\\/24$ |

Il est dans ce cas impossible de créer des sous-réseaux et de former des groupes d’utilisateur de différentes tailles
au sein d’un réseau d’entreprise. C'est ce problème que résout l'**adressage Classless (SANS CLASSES)** car il permet d’envoyer le masque de sous-réseau utilisé aux autres équipements et de ce fait, de créer des sous-réseaux de taille variable.

Le **CIDR** et le **VLSM** sont des exemples de procédures utilisant un adressage classless. Bien que complémentaires,
celles-ci sont différentes. Le VLSM peut d’ailleurs être vu comme une extension du CIDR au niveau d’une
organisation.

Le **CIDR** permet de diminuer significativement le nombre d’entrées des tables de routage en utilisant des agrégations de routes, càd des regroupement de routes: cela signifie que les paquets de données sont envoyés vers une même destination, pour adresses IP différentes mais ayant des bits de poids fort égaux.

Le **VLSM** permet en effet d’éviter le gaspillage d’adresse au sein d’une organisation en utilisant des masques de
taille variable

### Notation CIDR - Classless InterDomain Routing

#### Pourquoi CIDR ?

L'expansion d'Internet a entraîné l'**augmentation de la taille des tables de routage** sur de nombreux routeurs,
notamment les routeurs des fournisseurs d’accès à Internet.
Le CIDR est une solution permettant d’<bred>agréger plusieurs routes</bred> en une seule, càd de résumer plusieurs lignes stockées dans une table de routage en une seule ligne, ce qui a comme conséquence d'alléger de manière considérable ces tables de routage. En outre, cette agrégation de routes a permis la création de <bred>superréseaux</bred>:fr: ou <bred>supernetting</bred> :gb:

#### Définition

Les cas particuliers précédents, de masques de sous-réseaux et leurs notations CIDR particulières, peuvent être étendus à un nombre quelconque de bits:

!!! def "Notation CIDR - Classless InterDomain Routing"
    D'une manière générale, on utilise la notation `adresse (sous-)réseau /n`, appelée <bred>notation CIDR - Classless InterDomain Routing</bred> :gb: ou règles de <bred>Routage InterDomaines sans Classes</bred> :fr:, pour indiquer que le masque de sous-réseau est composé de:

    * que des $1$ sur les $n$ premiers bits
    * que des $0$ sur les $32-n$ derniers bits

!!! exp
    L'adresse `192.168.1.0 / 17` fait référence à un réseau tel que:

    * l'adresse de réseau vaut `192.168.1.0`
    * le masque de sous-réseau vaut `255.255.128.0`

!!! exp "d'Agrégation de routes"
    L’exemple suivant illustre l’utilisation d’une agrégation de quatre adresses réseaux en une seule adresse. Supposons que l'on souhaite agréger les 4 réseaux ci-dessous :

    <center>

    | Adresse Réseau<br/>en Décimal | Adresse Réseau<br/>en Binaire | Masques de Sous-Réseau<br/>(anciens) |
    |:-:|:-:|:-:|
    | 10.7.12.0 | 00001010.00000111.00001100.00000000 | /24 |
    | 10.7.13.0 | 00001010.00000111.00001101.00000000 | /24 |
    | 10.7.14.0 | 00001010.00000111.00001110.00000000 | /24 |
    | 10.7.15.0 | 00001010.00000111.00001111.00000000 | /24 |

    </center>

    d'où le:

    | Nouvelle route agrégée<br/>Nouveau masque de sous-réseau<br/> | Nouveau masque en Binaire | Nouveau masque CIDR |
    |:-:|:-:|:-:|
    | 255.255.<red>252</red>.0 | 11111111.11111111.11111100.00000000 | /22 |

#### Conditions d'Emploi

Remarquer que l'emploi de CIDR présuppose que :

* Les routeurs implémentent un algorithme de la **correspondance la plus longue**.
* Un **plan d'adressage hiérarchique** est appliqué pour l'assignation des adresses afin que l'agrégation puisse
être effectuée.
* Le protocole de routage classless utilisé, contrairement aux protocoles de routage classful (RIPv.1, IGRP), doit transporter les préfixes étendus dans ses mises à jour.
* Les hôtes et les routeurs supportent le routage classless. 

### VLSM

L'utilisation du <bred>VLSM - Variable Length Subnet Mask</bred> :gb: ou <bred>Masque de Sous-réseau à Taille Variable</bred> :fr: permet à un réseau classless d'utiliser différents masques de sous-réseaux, de tailles distinctes, au sein d’une organisation et d’obtenir par conséquent des sous-réseaux plus appropriés aux besoins.

La technique de Masque de Sous-réseau de Longueur Variable (VLSM) permet à un client de n'acquérir (par exemple) que la moitié de cet espace ; par exemple le réseau 94.20.0.0/17 attribue la plage d'adresses allant de 94.20.0.0 à 94.20.127.0. La plage 94.20.128.0 - 94.20.254.0 peut être vendue à une autre société.

Il existe principalement deux méthodes de VLSM:

* le VLSM Symétrique
* le VLSM Asymétrique

#### VLSM Symétrique

!!! mth
    * **Étape 1: Identifier le besoin**  
    Recenser les différents niveaux hiérarchiques de l’entreprise et dessiner la topologie. 
    * **Étape 2: Au niveau utilisateur**  
    Connaître la taille du sous-réseau
    * **Étape 3: Recensement**  
    Déterminer le nombre de bits nécessaires pour recenser chaque instance du niveau hiérarchique
    * **Étape 4: Classe d'adresse utilisée :**  
    Déterminer la classe d’adresse ou l’agrégat d’adresses (le choix dépendant du contexte), en additionnant tous les bits nécessaires pour identifier chaque niveau hiérarchique de l’entreprise. 
    * **Étape 5 :**  
    On procède ensuite au découpage de la classe d’adresse de l’entreprise et de l’attribution à chaque instance du niveau hiérarchique. 

![VLSM Symétrique](../img/vlsm_symetrique.png)

* Étape 1: une entreprise dans deux villes. Deux bâtiments par ville. Deux étages par Bâtiment. 50 utilisateurs par étage
* Étape 2: 50 utilisateurs / sous-réseau. + 1 adresse de broadcast + 1 adresse pour le réseau + 1 adresse pour la passerelle = 53 adresses IP
* Étape 3: On résout $2^x>53$, on trouve $x=6$, donc il faut $6$ bits par sous-réseau donc il faut un masque `/26` (`255.255.255.192`)
* Étape 4: Dans ce contexte, on peut découper une classe $B$ (beaucoup de gaspillage) ou agréger plusieurs classes $C$
* Étape 5: Chaque instance du niveau hiérarchique se voit attribuer un préfixe et un masque

#### VLSM Asymétrique

!!! mth
    * **Étape 1: Identifier le besoin**  
    Dessiner la topologie, identifier les besoins a chaque niveau hiérarchique.
    * **Étape 2: Recensement**  
    Connaître le nombre d’utilisateurs pour chaque sous-réseau (puisqu’ils peuvent être différents à chaque niveau
    maintenant), ce qui revient à connaître la taille de chaque sous-réseau (ne pas oublier qu’on ne peut pas utiliser
    la première ni la dernière adresse et qu’il faut une adresse IP pour la passerelle).
    Si le nombre d’utilisateur n’est pas connu a chaque niveau de la hiérarchie, on peut suivre un processus
    descendant (‘top down’) : repartir équitablement le nombre d’utilisateur pour un niveau hiérarchique supérieur
    vers le niveau directement inférieur.
    * **Étape 3 : Classe d’adresse utilisée**  
    Déterminer la classe d’adresse ou l’agrégat d’adresses (le choix dépendant du contexte), en additionnant tous
    les bits nécessaires pour identifier chaque niveau hiérarchique de l’entreprise.
    * **Étape 4 :**  
    En suivant un processus remontant récursif maintenant, on va agréger les différents instances d’un niveau pour
    obtenir l’identifiant réseau du niveau hiérarchique directement supérieur jusqu’a obtenir l’adresse agrégée de
    toute l’entreprise


![VLSM Asymétrique](../img/vlsm_asymetrique.png)

* Étape 1: une entreprise dans deux villes. Deux bâtiments dans la première ville, un seul bâtiment dans la deuxième ville. Tous les bâtiments ont deux étages, sauf un, qui en a un seul. Le nombre d'utilisateurs varie d'un étage à l'autre
* Étape 2: Recensement (en vert) Ne pas oublier 1 adresse de broadcast + 1 adresse pour le réseau + 1 adresse pour la passerelle
* Étape 3: Dans ce contexte, on peut découper une classe $B$ (beaucoup de gaspillage), ou agréger plusieurs classes $C$. On choisira une classe $C$.
* Étape 4: En remontant, on adresse chaque étage, chaque bâtiment (en rouge)

#### Conditions d'Emploi

Remarquer que l'emploi du VLSM présuppose que:

* Les routeurs doivent implémenter un algorithme de la **correspondance la plus longue**. En effet, les routes
qui ont le préfixe le plus élevé sont les plus précises. Les routeurs dans leurs décisions d'acheminement
doivent être capables de déterminer la route la plus adaptée aux paquets traités. 
* Un **plan d'adressage hiérarchique** doit être appliqué pour l'assignation des adresses afin que l'agrégation
puisse être effectuée. 
* Les protocoles de routage classless, contrairement aux protocoles de routage classful (RIPv.1, IGRP), transmettent dans leurs mises à jour de routage, le masque de sous-réseau pour chaque route.

Les protocoles **RIPv2**, **OSPF** (*EIGRP*, *BGP*, etc..) qui seront étudiés plus précisément en Terminale, supportent l'utilisation du VLSM (ce n'est pas le cas de tous les protocoles, attention).

## Routage d'un message interréseaux

### Une Lettre à La Poste

Imaginez Lisa, une habitante de Montréal, qui poste une lettre en papier (oui, ça existe encore, enfin il paraît ..) pour son amie française, dont l'adresse est la suivante:

<div class="nolines">

```python
Paola DESCHAMP
25, rue des illusions
13008, Marseille
FRANCE
```

</div>

Il semble évident que Lisa de Montréal, de même que sa Poste Canadienne, ne connaît pas directement où se trouve cette adresse précisément. Que fait la Poste Canadienne pour acheminer ("*router*") sa lettre à son amie ? 

La Poste (l'équivalent d'un **routeur**, au moment de chaque nouvelle redirection) raisonne "à l'envers", **du plus général vers le plus précis** :

* La Poste Canadienne commence par envoyer la lette en FRANCE (l'équivalent d'un réseau)
* en France, la Poste Française envoie la lettre à Marseille (l'équivalent d'un sous-réseau)
* Puis, arrivée à Marseille, la Poste l'envoye dans le 8ème arrondissement (un sous-sous-réseau...)
* Enfin, arrivée dans le 8ème arrdt de Marseille, le facteur connaît précisément où se trouve cette rue, et le numéro 25 (un immeuble par exemple: un sous-sous-sous-réseau...)
* Arrivé devant la maison, le facteur sélectionne la bonne boîte aux lettres (l'équivalent de l'hôte)
* la lettre peut donc être délivrée à la bonne personne (hôte) par le facteur du 8ème à Marseille

### Adresse IP d'un (Sous-)Réseau

Pour qu’un réseau étendu WAN (Wide Area Network) TCP/IP fonctionne efficacement[^1] (comme une Lettre à la Poste :stuck_out_tongue_winking_eye:) en tant que collection de réseaux, les routeurs qui passent des paquets de données entre les réseaux ne connaissent pas l’emplacement exact d’un hôte pour lequel un paquet d’informations est destiné: 

* Les routeurs connaissent uniquement le réseau dont l’hôte est membre et utilisent les informations stockées dans les *tables de routage* (des tableaux d’itinéraires) pour déterminer le réseau de l’hôte de destination. 
* chaque routeur traversé le renvoie vers un nouveau routeur (*next hop)*, etc.. jusqu'à arriver au (dernier) routeur qui définit le réseau de l'hôte
* Une fois le paquet remis au réseau de destination, il est remis à l’hôte approprié.

**Pour que ce processus fonctionne, une adresse IP doit être décomposée en deux (ou plusieurs) parties:**

![Adresse IP Sous-Réseau, partie Hôte](../img/ip_sous_reseaux.svg.png "copyright Wikipedia"){.center}

* La *1ère partie* (la plus à gauche) d’une adresse IP est utilisée comme **adresse IP du réseau**, ou **ID du Réseau** :fr:, ou **Net ID** :gb: (ou *network ID* ou *network prefix*)
* La *2ème partie* d’une adresse IP est utilisée comme **adresse IP du sous-réseau**, ou **ID du sous-Réseau** :fr:, ou **SubNet ID** :gb:, **SI le réseau a été découpé en sous-réseaux**
* etc.. (on peut décomposer en autant de sous-réseaux que l'on veut)
* la *dernière partie* (la plus à droite) comme **adresse IP de la machine Hôte**, ou **ID de l'Hôte** :fr:, ou **Host ID** :gb: 

!!! note "Convention adresse IP Sous-Réseau"
    Par convention, l'adresse IP d'un sous-réseau comprend des bits :

    * **quelconques** pour la partie réseau et la partie sous-réseau
    * **que des $0$ pour toute la partie hôte**  
    Autrement dit, on peut dire que l'adresse IP d'un sous-réseau est la toute première adresse IP de ce sous-réseau (lorsque la partie hôte est à $0$)

Le problème c'est que nous n'avons pas défini ce qui définit:

* Qu'est-ce que la "*1ère partie*"? est-ce le premier nombre de l'IPv4? ou les deux premiers nombres ? ou les 3 premiers? ou pire: le 1er nombre et *un peu* du deuxième...
* idem: Qu'est-ce que la "*la 2ème partie*"? etc..
* idem: Qu'est-ce que la "*dernière partie*"? la dernier nombre? ou les deux derniers? etc..

C'est pour très exactement à cela que servent les masques de sous-réseaux: à établir précisément la frontière entre la partie réseau et la partie hôte d'une adresse IP.

Aller Plus Loin:

* [Adressage IPv4](https://inetdoc.net/articles/adressage.ipv4/adressage.ipv4.subnet.html)
* [RFC950, Internet Subnetting Procedure, 1985](https://www.rfc-editor.org/info/rfc950)
* [RFC1878, VLST Variable Length Subnet Table for IPv4, 1995](https://www.rfc-editor.org/info/rfc1878)




## Fonctionnement de la Pile TCP/IP

## Services DNS

## Serveur DHCP

## Notes et Références

[^1]: [Microsoft, Comprendre les principes de base de l'adressage TCP/IP et du sous-réseau](https://docs.microsoft.com/fr-fr/troubleshoot/windows-client/networking/tcpip-addressing-and-subnetting)
[^2]: [CIDR: Qu'est-ce que le Classless InterDomain Routing? par IONOS Digital Guide](https://www.ionos.fr/digitalguide/serveur/know-how/cidr/)
[^3]: [Documents RFC](https://inetdoc.net/articles/adressage.ipv4/adressage.ipv4.conclusion.html#rfc950)
[^4]: [RFC950, Internet Subnetting Procedure, 1985](https://datatracker.ietf.org/doc/rfc950/)
[^5]: [RFC1878, VLST Variable Length Subnet Table for IPv4, 1995](https://datatracker.ietf.org/doc/rfc1878/)