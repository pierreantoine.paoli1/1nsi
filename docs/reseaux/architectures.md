# 1NSI: Réseaux - Architectures de Réseaux

On catégorise classiquement les réseaux selon plusieurs critères :

## par Taille / Étendue du Réseau

### Réseau PAN Personal Area Network

Un <bred>Réseau PAN - Personal Area Network</bred> :gb:, ou <bred>Réseau Personnel</bred> :fr:, relie des machines sur quelques mètres.

### Réseau LAN Local Area Network

Un <bred>Réseau LAN - Local Area Network</bred> :gb:, ou <bred>Réseau Local</bred> :fr:, est adapté à la taille d'une famille, ou d'une entreprise ou d'un établissement scolaire.

!!! def "Domaine de Diffusion / Booadcast"
    Un **LAN** est un réseau où tous les périphériques sont dans le même **domaine de broadcast / diffusion** (adresse de diffusion vers tous les périphériques d’un réseau).  
    Dans un LAN, chaque élément du réseau peut communiquer avec l’ensemble du réseau sans passer par un routeur.

:warning: Dans la suite de ce cours, nous n'étudierons **que** des réseaux de type LAN :warning:

### Réseau MAN Metropolitan Area Network

Un <bred>Réseau MAN - Metreopolitan Area Network</bred> :gb:, ou <bred>Réseau Métropolitain</bred> :fr:, est un réseau étendu à l'échelle d'une ville.

### Réseau WAN Wide Area Network

Un <bred>Réseau WAN - Wide Area Network</bred> :gb:, ou <bred>Réseau Étendu</bred> :fr:, est un réseau étendu à une grande échelle, comme par exemple celle d'un pays ou d'un continent.

## par Mode de Diffusion

### Unicast :gb: ou Connexion $1$ vers $1$  

!!! def "Unicast" 
    Dans un réseau, Le mode de diffusion <bred>Unicast</bred> définit une **connexion réseau <bred>point à point</bred>**, c'est-à-dire d'**un hôte** vers **un (seul) autre hôte** ($1$ vers $1$).

On entend par *unicast* le fait de communiquer entre deux ordinateurs identifiés chacun par une adresse IP unique dans le réseau. 

<env>Remarque</env> Contrairement à une connexion Unicast, une transmission **liaison point à point**:

* n'est pas prévue pour appartenir à un réseau
* n'admet pas de notion native d'adresse IP pour chacun des hôtes

### Multicast ou Diffusion $1$ vers Plusieurs

!!! def "Multicast"
    Le <bred>multicast</bred> :gb: :fr: ou <bred>multidiffusion</bred> ou <bred>diffusion multipoint</bred> ou <bred>diffusion de groupe</bred> :fr: est un mode de diffusion d'un émetteur (source unique) vers un groupe de récepteurs: l'émetteur diffuse un flux réseau à un groupe d'autres récepteurs connectés au réseau. Les récepteurs intéressés par les messages adressés à ce groupe **doivent s'inscrire à ce groupe**. Ces abonnements permettent aux **switchs** et **routeurs** intermédiaires d'établir une route depuis le ou les émetteurs vers les récepteurs de ce groupe. 

#### Avantages

* Ce système est plus efficace que l'unicast pour diffuser des contenus simultanément vers une large audience
* chaque paquet n'est émis qu'une seule fois et sera routé vers toutes les machines du groupe de diffusion sans que le contenu ne soit dupliqué sur une quelconque ligne physique ; c'est donc le réseau qui se charge de reproduire les données. 
* Le multicast permet de développer des applications interactives de groupe, comme la visioconférence, le partage de tableau, etc.

#### Inconvénients

Le multicast ne permet cependant en aucune façon le contrôle de la participation au groupe par la source : la source ne peut déterminer ni qui participe, ni qui peut participer ou non au groupe.

### Broadcast ou Diffusion $1$ vers Tous

Le <bred>broadcast</bred> :gb: :fr: est un mode de diffusion d'un émetteur (source unique) vers la totalité de récepteurs d'un (sous-) réseau.

!!! pte "Broadcast via des Routeurs / Switchs / Hubs"
    * Les **Routeurs** ne transmettent **pas** les paquets « broadcast ». 
    * Un **Switch / Commutateur** recevant une trame broadcast sur l'un de ses ports la diffusera sur tous les autres ports.
    * Un **Hub / Concentrateur** recevant une trame broadcast sur l'un de ses ports la diffusera sur tous les autres ports.

!!! note "En pratique"
    En général, ces paquets sont utilisés dans un réseau LAN pour atteindre une machine dont on ne connaît pas l'adresse MAC (protocole ARP pour le protocole IPv4) ou pour des annonces faites aux clients potentiels par des machines pouvant offrir des services

!!! def "Domaine de Diffusion :fr: ou Broadcast Domain :gb:"
    Un <bred>Domaine de Diffusion</bred> :fr: ou <bred>Broadcast Domain</bred> :gb: est une zone logique d'un réseau informatique dans laquelle n'importe quel hôte connecté au réseau peut directement transmettre à tous les autres hôtes du même domaine, **sans devoir passer par un routeur**. 

!!! pte
    L'étendue de diffusion sera restreinte au domaine de diffusion. 


## par Topologie du Réseau

Les réseaux peuvent être structurés de plusieurs manières appelées <bred>Topologies Réseau</bred>.

### Topologie en Bus

<figure>

<center>

<img src="../img/topologieBus.svg"/>

<figcaption>

Un réseau avec une Topologie de Bus

</figcaption>

</center>

</figure>

### Topologie en étoile

### Topologie en anneau

### Comparaison des Architectures Réseau

#### Tolérance aux pannes

#### Répartition de charge réseau


