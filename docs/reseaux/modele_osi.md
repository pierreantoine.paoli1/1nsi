# 1NSI: Réseaux - Le Modèle OSI

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/26jazyc7VNk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Qu'est-ce que le Modèle OSI?

### Définition

Le <bred>modèle OSI - Open Systems Interconnection</bred> :gb: est une <bred>norme ISO - International Standards Organisation</bred> (Norme *ISO* $7498$) définie en $1984$, présentée par *Charles Bachman* en mars $1978$ (dans le document $ISO/TC97/SC16/N34$), permettant d'organiser la communication entre des systèmes informatiques interconnectés (directement, ou à travers un réseau):  

* La modélisation OSI se décompose en $7$ couches (par opposition au modèle TCP/IP qui en contient seulement $4$)
* C'est un **Modèle Théorique, Académique**
* Les couches sont aussi désignées par leur <bred>numéro</bred> ou <bred>niveau</bred>
* On associera à chaque couche des **rôles**, des **services**, des **protocoles** et du **matériel**
* **PDU** est le nom générique donné à l’encapsulation correspondante à une couche

Il s'agit donc d'une manière de penser, d'organiser, de **modéliser** la communication entre systèmes informatiques interconnectés, en plusieurs niveaux d'abstraction.

### Nom des $7$ Couches

<center>

<figure>

<img src="../img/modele-OSI.svg">

</figure>

</center>

### Nom des PDU par Couche

Nous avons dit que chaque couche de l'hôte source communique avec la même couche de l'hôte destination, en ayant pris soin de morcellant les données en PDU ou Unités de Données de Protocole. Les PDU de chaque couche portent des noms différents :

![OSI Chaque couche parle à sa Couche](../img/osi-pdu-par-couche.svg){.center}

## Rôle de chaque couche

### La Couche $7$. Application

La **Couche $7$ - Couche Application**, ou **niveau Applicatif**, est l'interface entre l'utilisateur et le réseau. C'est la couche la plus proche de l'utilisateur. Elle est gérée directement par les **applications/logiciels**.  
La couche $7$ est le **point d'accès aux Services** réseaux, qui implémentent les **protocoles** sur des (numéros de) **ports** associés spécifiques (à chaque service). Elle fournit par exemple, des services standards tels que le **transfert**, les opérations en Lignes de Commandes (CLI) de **terminal virtuel** (par ex. SSH, ..), de **fichiers** et de **travaux** (d'impression, etc..).
La couche $7$ est en charge d'**initier** ou **accepter** une requête réseau.  

<center>

| Types de Logiciels | Services | Ports Non Sécurisés<br/>Associés (Déconseillés) | Ports Sécurisés<br/>Associés (Conseillés) |
| :-: | :-: | :-: | :-: |
| Courrier électronique | SMTP (envoi) <br/>POP $^{*}$ (réception)<br/>IMAP $^{*}$ (réception) | $25$<br/>$110$<br/>$143$ | $465$ (SSL) ou $587$ (STARTTLS)<br/>$995$ (SSL)<br/>$993$ (SSL) |
| Transport de Fichiers | FTP</br/>SFTP | $21$ | <br/>$22$ |
| Transport de Page Web<br/>Serveur Web (Apache, ..) | HTTP</br/>HTTPS | $80$ | <br/>$443$ |
| Communication en <br/>Ligne de Commande | Telnet</br/>SSH | $23$ | <br/>$22$ |
| Système de <br/>Noms de Domaine | DNS<br/>DNSCrypt<br/>DNS over HTTPS<br/>DNS over TLS<br/>etc.. | $53$ | <br/>$443$<br/>$443$<br/>$853$<br/>etc.. |
| Serveur de<br/>Base de Données | MySQL<br/>PostgreSQL | $3306$<br/>$5432$ |  |

</center>

<div style="font-size:0.8em; color: #999999;">

(*): POP permet la réception des emails sur un hôte, mais ne synchronise pas les boîtes de réception emails sur tous les hôtes, en cas de lecture de la même boîte email sur plusieurs hôtes (ordi fixe, ordi portable, téléphone, ..).  
Contrairement à IMAP, qui lui fait la même chose, mais synchronise les boîtes de réception emails sur tous les hôtes, en cas de lecture sur plusieurs hôtes (IMAP est donc à préférer à POP)

</div>

### La Couche $6$. Présentation

La **Couche $6$ - Couche Présentation** est chargée du **codage des données** applicatives en des formats compréhensibles par les applications, et réciproquement, en octets bruts  (pour les transmettre aux couches inférieures). 

La couche $6$ est en charge de :

* **Formattage des données (Présentation)**: **Conversion** entre formats de données manipulées au niveau applicatif et chaînes d'octets bruts effectivement transmises, dont notamment les normes:
    * Les Normes des Textes/Caractères: ASCII, UTF-8, etc..
    * Les Normes de Données : CR vs CR/LF
    * Les Normes des Images: BMP, JPEG (/JPG), PNG, GIF, WEBP, etc..
    * Les Normes des Audios/Vidéos: MPEG, AVI, MP3, etc..
* **Compression** des données
* **Cryptage/Chiffrement** des données

### La Couche $5$. Session

La **Couche $5$ - Couche Session** (norme ISO 7498-1) **Contrôle le dialogue/communication** entre les divers hôtes qui communiquent. Une communication entre ordinateurs suppose de nombreuses conversations courtes (commutation de paquets) avec en plus de cela d’autres communications pour s’assurer de l’efficacité de la communication. 

La couche $5$ est en charge de:

* Gérer **(Établir, Maintenir et Terminer) les Sessions de Connexion** entre les (processus des) applications distantes entre deux (ou plus) hôtes d'un réseau :
    * Entre deux hôtes, la communication est de type **point à point**. 
    * Entre plus de deux hôtes, les communications sont dites **multipoints**
* Gérer la **Synchronisation** des échanges (en effet, n'importe quel intervenant peut émettre à tout moment) en proposant des **points de synchronisation/contrôle** dans le flux de données. 
* Gérer les **Autorisations des Transactions** ou **Droit à la parole** ou **Jeton** ou **Token** afin de gérer les conflits (et/ou les éviter) lors du partage d'un même canal de communication. Cela inclut la gestion des communications **Half-Duplex** (Dans un seul sens simultanément) et **Full-Duplex** (Dans les deux sens simultanément)
* Elle gère l'**Authentification** (qui est qui?) de l'hôte qui souhaite communiquer: L'<bred>Authentification</bred> :fr: ou <bred>Authentication</bred> :gb:, est un procédé permettant au système de s'assurer de l'**identification** d'une entité (humain ou hôte). En pratique, il existe plusieurs **facteurs d'authentification** (des mécanismes pouvant être utilisés pour authentifier). Lorsque deux (ou plusieurs..) facteurs d'authentification sont conjugés, on parle d'**Authentification à 2 facteurs (ou forte)** :fr:, ou **2FA - Two Factor Authentication** :gb:. Dans le cas d'un individu, par exemple, voici quelques facteurs d'authentification possibles/usuels:
    * (usuellement) des **identifiants** (identifiant et mot de passe) mais on peut également utiliser 
    * des **certificats électroniques** (par exemple au format **[X.509](https://fr.wikipedia.org/wiki/X.509)**), encore appelés des **certificats numériques** ou **certificat à clé publique** (sorte de carte d'identité numérique), qui est signé (par cryptographie) par un **tiers de confiance** (souvent une autorité de certification) qui atteste du lien entre identité physique et l'entité numérique. 
    * **Mot de Passe à usage unique** :fr: ou **OTP - One Time Password** :gb: qui peuvent être implémentées sous plusieurs formes:
        * par **email**: lien magique envoyé par email 
        * des **SMS**: code envoyé via SMS
        * <img src="../img/cle-OTP.png" style="width:200px; float:right;"/> des **clés OTP** (Clé USB, ou petit dispositif matériel) qui génèrent des clés aléatoires à usage unique ou **Token OTP**, ou **jeton d'authentification**, quelquefois à durée limitée.
    * des **empreintes biométriques**: un résumé numérique de (certaines) caractéristiques biologiques d'un individu

La couche $5$ a donc quelque chose à voir avec la **Sécurité des communications**. Elle coordonne les applications qui communiquent au travers des différents hôtes. Une communication entre ordinateurs suppose de nombreuses conversations courtes (commutation de paquets) avec en plus de cela d’autres communications pour s’assurer de l’efficacité de la communication. 

#### C'est quoi une Session?

!!! def "d'une Session"
    * Une <bred>Session</bred> utilise un canal de communication pour **authentifier** une entité (un individu ou un hôte) sur un hôte.

#### Exemples de Protocoles de niveau $5$

Les protocoles suivants sont des implémentations correspondent au niveau $5$ du modèle OSI:  

* **Cookies** : À la première visite d'une page web, un serveur web demande au client de conserver un **cookie de session**, qui contient un simple identifiant. Lorsque l'utilisateur passe sur une autre page du site, le cookie est envoyé au serveur web en même temps que la requête HTTP : le serveur web peut alors retrouver la session d'un utilisateur grâce à l'identifiant stocké dans le cookie.
* **HTTP - HyperText Transfer Protocol**(plusieurs RFC[^6]) qui est un protocole fiable, orienté connexion (il est basé sur le protocole TCP de niveau $4$). Les clients HTTP sont les navigateurs internet.

#### C'est quoi un protocole orienté connexion ?



### La Couche $4$. Transport

La **Couche $4$ - Couche Transport** gère les communications de bout en bout entre processus (programmes en cours d'exécution). Elle fragmente (et reconstitue) les messages en de petites **Unités de Données de Protocoles (PDU)** appelés <bred>Segments</bred> (spécifique au protocole TCP, mais aussi le terme générique pour la couche $4$) ou (quelquefois) <bred>Datagrammes</bred> (pour le protocole UDP).

La couche $4$ est en charge de:

* **Communications Point à Point** entre processus (**Transport**)
* **Fiabilité** des communications: Si on perd des données, on demande une retransmission des données manquantes
    * **Détection et Correction d'Erreurs**:
        * Détection : Repérage des PDU dont au moins un bit a changé de valeur lors du transfert. 
        * Correction : Compensation des erreurs soit par correction des données à l'aide de code correcteurs d'erreurs ou par destruction du PDU erroné et demande de retransmission.
    * **Contrôle de Flux**: Synchronisation des communications destinée à empêcher qu'un interlocuteur reçoive plus de PDU qu'il ne peut en traiter
* **Segmentation** et **Réassemblage**
* **Multiplexage (MUX)** et **Démultiplexage (DEMUX)** des connexions
![Multiplexage](../img/multiplexage.svg){.center}

La couche $4$ a certains (mais pas tous) objectifs et fonctions communs à ceux de la couche $3$. 

#### Exemples de Protocoles de niveau $4$

Les protocoles suivants sont des implémentations correspondent au niveau $4$ du modèle OSI:  

* **TCP - Transfer Control Protocol**(RFC 793[^4]) qui est un protocole fiable, **orienté connexion** (*handshaking*)
* **UDP - User Datagram Protocol** (RFC 768[^5]) qui est un protocole non fiable, **non orienté connexion**
* Chacune de ces implémentations définit la notion de (**numéro de**) <bred>port</bred> (TCP vs UDP). Cf Modèle TCP/IP.

### La Couche $3$. Réseau

La **Couche $3$ - Couche Réseau** est en charge de :

* **Fragmenter** les messages en **Unités de Données de Protocole (PDU)** appelés <bred>paquets</bred> (pour le niveau $3$ du modèle OSI)
* **Adresser les interfaces réseau** de manière globale
* **Router/Acheminer les messages (les paquets)**, de proche en proche (de routeur en routeur) jusqu'à leur destination finale, en utilisant les **adresses logiques** (les **adresse IP**) source et destination. Cette fonction est appelé le <bred>Routage</bred>. Elle fait typiquement appel à des ordinateurs spécialisés, appelés <bred>routeurs</bred> :fr: ou <bred>routers</bred> :gb:, qui sont des systèmes intermédiaires sur la route qui va de la source à la destination.
* Déterminer les <bred>meilleurs chemins</bred> pour les données à travers un inter-réseau, grâce à des <bre>protocoles de routage</bred> (RIP, OSPF, etc..) sur chaque routeur

#### Exemples de Protocoles de niveau $3$

Les protocoles suivants sont des implémentations correspondent au niveau $3$ du modèle OSI:  

* **IP - Internet Protocol**
* **ARP - Address Résolution Protocol** est un protocole de Résolution d'adresse IP d'un hôte à partir de son adresse MAC (unique) dans un réseau local. En pratique, dans un certain hôte donné, la **table ARP** est un tableau reliant les adresse IP des hôtes connus (à un instant donné) à leurs adresses MAC.
* **DNS - Domain Name System** est un <bred>S</bred>ystème hiérarchique de résolution de <bred>N</bred>oms de </bred>Domaines, qui utilise sur un protocole du même nom.

#### Matériels Usuels de niveau $3$

Les **routeurs** :fr: sont des exemples classiques de matériels opérant sur le niveau $3$ Réseau du modèle OSI.  
Certains **switchs** dits <bred>administrables</bred>, sont de niveau $3$. Les autres switchs sont de niveau $2$.

### La Couche $2$. Liaison Données

La **Couche $2$ - Couche Liaison de Données** gère les communications entre deux machines connectées entre elles dans le même réseau (connectées directement, ou interconnectées via un hub :gb: / concentrateur :fr:, ou même un switch :gb: / commutateur :fr:, de niveau 2, mais pas un routeur, car alors les hôtes ne sont pas le même réseau - de niveau 3).

La couche Liaison de Données est usuellement subdivisée en deux **sous-couches**[^2] :

* la Sous-Couche haute <bred>Contrôle de la Liaison Logique</bred> :fr: ou <bred>LLC - Logical Link Control</bred> :gb: (proche de la couche Réseau)
* la Sous-Couche basse <bred>Contrôle d'Accès au Media</bred> :fr: ou <bred>MAC - Medium Access Control</bred> :gb: (proche de la couche Physique)

#### La sous-couche haute LLC - Logicial Link Control

La sous-couche (moitié haute) <bred>Contrôle de la Liaison Logique</bred> ou <bred>LLC - Logical Link Control</bred>[^3] de la couche **Liaison de Données** admet les fonctions suivantes :

* **Multiplexer (MUX)** les protocoles supérieurs à la couche $2$ (liaison de données): cela se traduit en pratique par la segmentation des données en **Trames** (les PDU de niveau $2$) pour faciliter leur envoi mélangé avec celles d'autres protocoles. Sur la figure ci-dessous, les boules de couleur représentent les Trames pour chaque protocole.
et **Démultiplexer (DEMUX)** dans le sens contraire.. (regrouper les trames selon leur protocole initial)

    ![Multiplexage](../img/multiplexage.svg){.center}

* **Fiabiliser** le protocole MAC en y ajoutant:
    * :one: un **Contrôle d'Erreur** *orienté caractère*: des **pertes**, des **doublons**, des **inversions** d'ordre des trames, des **erreurs dans la transmission d'une trame** (support imparfait. Il peut se rompre ou générer des erreurs de transmission). Plusieurs technologies peuvent être utilisées pour le Contrôle d'Erreur:
        * Avec un **Bit de Parité** ou **Contrôle de Parité Verticale** :fr:, ou **VRC - Vertical Redundancy Check** :gb:.
        Par exemple, tous les $7$ bits on ajoute un **bit de parité**/ **bit de contrôle**)

        <center>

        | Données émises</br> + <bred>Bit de Contrôle</bred> | Données reçues<br/> + <bred>Bit de Contrôle</bred> | Contrôle d'erreurs |
        |:-:|:-:|:-:|
        | 1101101<bred>1</bred> | 1101101<bred>1</bred> | Pas d'erreurs |
        | 1101101<bred>1</bred> | 1100101<bred>1</bred> | Erreur: car parité calculée par<br/>le récepteur: 0<br/>$\ne$ Bit de Contrôle (<bred>1</bred>) |

        </center>  

        * avec des **Checksums** :gb: ou **Sommes de Contrôle** :fr: ou **fonctions de Hashages** :gb: (quelquefois **Hashs**) ou **[Empreintes (numériques)](https://fr.wikipedia.org/wiki/Somme_de_contr%C3%B4le)** :fr: sont une suite de plusieurs caractères (calculés grâce à une *fonction de Hashage* de même nom) utilisés fréquemment pour caractériser de manière unique des fichiers/données (en général et en pratique, pour "*résumer*" de grandes quantités de données/de très gros fichiers) de manière à pouvoir vérifier d'un simple coup d'oeil si les données reçues sont conformes à ce qu'elles devraient être. Ou pas. Il s'agit d'une généralisation des bits de parité. Il existe de nombreuses Checksums/Sommes de Contrôle (ou fonctions de Hashage). Parmi les plus connues ou les plus évoluées, on peut citer: [MD5](https://fr.wikipedia.org/wiki/MD5) (daté), [SHA-2](https://fr.wikipedia.org/wiki/SHA-2) dont notamment Sha-256 et Sha-512), etc.. (elles sont très nombreuses)  
        Exemple:  
        ![Checksum](../img/checksum.svg){.center}

        * **CRC - Cyclic Redundancy Check** ou **Code de Redondance Cyclique** (le plus classique), permet de détecter et corriger les Erreurs et demander leur retransmission (**ARQ** - Automatic Repeat Request)

    * :two: un **Contrôle de Flux**. Le contrôle de flux est un **asservissement du débit binaire de l'émetteur vers le récepteur**, pour éviter les congestions. La plus simple et connue étant le **Stop & Wait**: 
        * Émetteur: Transmet un segment à la fois
        * Récepteur: transmet un <bred>acquittement (ACK)</bred> pour signaler la bonne réception (segment perdu ou erreur de transmission), pour chaque segment envoyé, afin de s'assurer de la bonne réception, 
        * Émetteur: Reçoit le ACK au plus tard après une certaine période de temps, appelée <bred>Time Out</bred>. 
        * En cas de mauvaise réception du segment, il doit être retransmis, selon une procédure appelée <bred>ARQ (Automatic Repeat reQuest)</bred>
    * :three: un **Contrôle de congestion** qui est la manière de gérer le flux de données après que la congestion ait eu lieu.

La couche Liaison de Données est commune à Tous les sous-réseaux Physiques de type **IEEE 802**[^2].
Concrètement, la couche LLC peut être par exemple **un driver** d'un périphérique matériel.

#### La sous-couche basse MAC - Medium Access Control

La sous-couche (moitié basse) <bred>MAC - Medium Access Control</bred> de la couche Liaison de Données,
Le rôle de cette sous-couche MAC est de:

* **Reconnaître le début et la fin des trames**, on parle de <bred>cadrage des Trames</bred>, dans le flux binaire reçu de la couche physique
* **Délimiter les trames** envoyées en insérant des informations (comme des bits supplémentaires) dans ou entre celles-ci, afin que leur destinataire puisse en déterminer le début et la fin ;
* **Détecter les erreurs** de transmission, par exemple à l'aide d'une somme de contrôle (<bred>checksum</bred>) insérée par l'émetteur et vérifiée par le récepteur
* **Insérer** les <a href="#versdefinition"><bred>adresses MAC</bred></a>$^{*}$ de la source et de la destination dans chaque trame transmise,
* **Filtrer les trames** reçues en ne gardant que celles qui lui sont destinées, en vérifiant leur adresse MAC de destination
* **Contrôler l'accès au média physique** lorsque celui-ci est partagé, en particulier de définir le protocole d'accès au support.

<div id="versdefinition">
</div>

!!! def "Adresse MAC - Media Access Control"
    1. Une <bred>adresse MAC - Media Access Control</bred> ou <bred>adresse de Contrôle d'Accès au Media</bred>, parfois nommée <bred>adresse physique</bred>, est un identifiant unique stocké dans une **carte réseau** ou une **interface réseau**. Elle identifie donc de manière unique ce matériel, c'est pourquoi elle est quelquefois appelée une <bred>adresse physique</bred>.  
    2. En pratique, Une <bred>adresse MAC</bred> est constituée de $6$ octets, soit $48$ bitset est généralement représentée sous la forme hexadécimale en séparant les octets par un double point. Par exemple `5E:FF:56:A2:AF:15`

!!! info "Comment connaître l'adresse MAC d'une carte réseau sur un OS?"

    * Sur Windows: 
        ```bash linenums="0"
        C:> ipconfig/all
        ```
    * Sur Mac OS: 
        ```bash linenums="0"
        $ networksetup -listallhardwareports
        ```
    * Sur Linux:
        ```bash linenums="0"
        $ ip address
        ```

!!! info "Adresse MAC Constructeur"
    Les $6$ octets d'une adresse MAC sont classiquement divisées en deux parties égales:  

    * Les **$3$ premiers octets** (24 bits) sont des octets correspondent au constructeur de la carte réseau/interface réseau.
        * Cette première partie (préfixe) *contructeur* est attribuée par l'IEEE à chaque constructeur.
        * Un Constructeur peut avoir plusieurs préfixes
        * la page [maclookup](https://maclookup.app/), par exemple, est un annuaire renversé:
            * [par adresse MAC](https://maclookup.app/search)
            * [par fabricant de cartes réseaux](https://maclookup.app/search/vendors)

    <center>

    | Constructeur | Adresse réservée ($3$ Octets) |
    |:-:|:-:|
    | Cisco | $00000C$ |
    | Intel | $00AA00$, $001111$, etc.. |
    | Gigabyte | $001485$, $0013F3$, etc.. |
    | IBM | $00AA00$, $0002B3$, etc.. |
    | 3Com | $0000D8$, $0020AF$, etc.. |

    </center>

    * les **$3$ derniers octets** sont caractéristiques de la carte réseau/interface réseau

La couche MAC interface LLC avec le type de sous-réseau physique, elle est différente d'un sous-réseau physique à l'autre. On assure ainsi l'indépendance des logiciels des couches supérieures vis à vis des caractéristiques physiques du sous-réseau.

#### Exemples de Protocoles de niveau $2$

Les protocoles suivants sont des implémentations correspondent au niveau $2$ du modèle OSI:

* **PPP - Point to Point Protocol** : Protocole non ISO, mais défini par l’IETF sous une RFC, permettant l’acheminement du protocole IP (et d’autres) sur un lien série en communication synchrone ou asynchrone, équilibrée ou non, et offrant nombre de services tels que l’authentification d’accès.
* Le **Protocole Ethernet** (IEEE $802.3$) & le Wi-Fi (IEEE $802.11b$), pour le raccordement au réseau avec ou sans fil

#### Matériels Usuels de niveau $2$

Les **switchs** :gb: ou **Commutateurs** :fr: sont des exemples classiques de matériels opérant sur le niveau $2$ Liaison de Données. (attention: il existe néanmoins des switchs dits *administrables*, ou *de niveau 3*)

### La Couche $1$. Physique

La **Couche $1$ - Couche Physique** est chargée de la transmission effective et brute des signaux (bits) sur un **canal de communication**:

* sur une **paire de cuivre**, c’est :
    * un *signal électrique* qui définit le $0$ et 
    * un autre *signal électrique* qui définit le $1$
* sur une **fibre optique**, c’est la lumière envoyée dans la silice qui s’en occupe
* sur du **sans-fil**, c’est la modulation
* etc..

La définition de connecteurs, des câbles ou antennes font partie de cette couche. En général on considère que les **cartes réseau**, les **modems** et les **concentrateurs (hubs)** en font aussi partie.

Une autre fonction de cette couche est de sérialiser l'information, càd de transformer les octets en éléments binaires (bits) et vice versa pour pouvoir émettre ou recevoir sur les canaux de communication. Cette transformation doit être effectué à un rythme qui est imposé par la vitesse (débit binaire) de l'interface.
L’un des objectifs de conception de ce niveau est de s’assurer qu’un bit à $1$ 
envoyé sur une extrémité arrive aussi à $1$ de l’autre côté et non à $0$
Les questions que l’on se pose portent généralement sur :

* les signaux électriques à utiliser pour représenter un $1$ et un $0$
* le nombre de nanosecondes que doit durer un bit, 
* la possibilité de transmission bidirectionnelle simultanée (Full Duplex), ou pas (Half Duplex) 
* la façon dont une connexion initiale est établie (avec connexion, ou sans) puis libérée lorsque les deux extrémités ont fini, ou encore
* le nombre de broches d’un connecteur et 
* le rôle de chacune des broches

Les problèmes de conception concernent principalement les interfaces mécaniques et électriques et la synchronisation, ainsi que le support physique de transmission sous-jacent à la couche physique.

## Résumé

<center style="margin-top:2em;">

| N° Couche | En charge de |
| :-: | :-: |
| $7.$ Application | C'est l'Interface visible par l'Utilisateur.<br/>Fournit des Services (Applicatifs)<br/> pour les Protocoles (Applicatifs)<br/>sur des Numéros de Ports.<br/> Ex: Protocoles : SMTP, HTTP, SSH, ...<br/> sur les Ports: 25, 80, 22, .. |
| $6.$ Présentation | <ul><li>Encodage des données sous un format acceptable<br/>(ASCII, Unicode, JPEG, ..)</li><li>Compression des données</li><li>Cryptage des données</li></ul> |
| $5.$ Session | Décide d'établir ou de terminer la session de connexion (**socket**), <br/>le "*droit à la parole*" ou **jeton** :fr: ou **token** :gb:<br/>, la Synchronisation, etc.. |
| $4.$ Transport | Segmente et reconstitue le message en segments numérotés et <br/>vérifie la fiabilité de la transmission (TCP, UDP) |
| $3.$ Réseau | S'occupe du routage des paquets, <br/>du trajet entre source et destination,<br/>donne une adresse logique (IP) |
| $2.$ Liaison <br/>de données | S'occupe des adresses physiques (MAC, LLC),<br/> des trames au niveau local (LAN) |
| $1.$ Physique | Transmet de manière effective les bits (Ethernet, Wifi, etc..) |

</center>

## Avantages / Désavantages du Modèle OSI

Quelques avantages de ce modèle sont :

* **Modularité:** Une division de la communication réseau en éléments plus petits et plus simples (les couches) pour une meilleure compréhension. Chaque protocole opérant sur chaque couche pourra ainsi, si besoin, être simplement remplacé relativement aisément. On peut aussi modifier un aspect de la communication réseau sans modifier le reste (par ex. : un nouveau média).
* **Abstraction Constructeur:** L’approche et l'uniformisation purement théorique des éléments du modèle permet le développement multi-constructeur, sans être biaisé envers une technologie particulière. C'est une qualité malgré l'absence d'implémentation pratique.
* **Modèle de Référence, International:** Il s'agit d'une norme internationale, qui est un modèle de Référence très détaillé qui s'est avéré très utile, bien qu'académique. Il couvre de vastes sujets.   
* **Séparation Fonctionnelle:** Un avantage majeur du modèle OSI[^1] est qu'il différencie très clairement les notions de:
    * Services
    * Protocoles
    * Interfaces

Quelques inconvénients/imperfections seraient:

* **Des catégorisations (quelquefois) arbitraires** : L'une des conséquences de manque d'implémentation sont les décisions quelquefois arbitraires qui ont dû être prises quant à l'appartenance à une couche ou une autre, pour une certaine fonctionnalité. 
* Une **inégalité dans la description et l'importance** des couches:
  * Les couches **Présentation** et **Session** du modèle OSI, par exemple, ne font pas grand chose, tandis que 
  * la couche **Liaison de Données** a dû être sub-divisée en **deux sous-couches** (LLC et MAC). 
* Un **Manque d'Implémentation**... : L' insuccès du modèle OSI, par comparaison au succès de l'implémentaion TCP/IP. Les imperfections du modèle OSI, cumulés au succès de la Pile de Protocoles TCP/IP, ont contribué au manque de succès des tentatives suivantes d'implémentation d'une Pile de protocoles basés sur le modèle OSI.

## Notes et Références

[^1]: [OSI Reference Model, technologyuk.net](https://www.technologyuk.net/telecommunications/telecom-principles/osi-reference-model.shtml)
[^2]: En fait, l'implémentation de la couche $2$ se fera plus précisément dans certains réseaux: les réseaux MAN - réseaux métropolitains , et les réseaux LAN - réseaux locaux. Cf le cours sur le Modèle TCP/IP) :  
cf. [Normes LAN et MAN, IEEE 802, wikipedia](https://fr.wikipedia.org/wiki/IEEE_802)
[^3]: [Norme LLC, IEEE 802.2, wikipedia](https://en.wikipedia.org/wiki/IEEE_802.2)
[^4]: [TCP : RFC 793](https://datatracker.ietf.org/doc/html/rfc793))
[^5]: [UDP : RFC 768](https://datatracker.ietf.org/doc/html/rfc768)
[^6]:
* En $1996$: [RFC 1945](https://datatracker.ietf.org/doc/html/rfc1945) pour HTTP/$1.0$ 
* ~~En $1997$: [RFC 2068](https://datatracker.ietf.org/doc/html/rfc2068) pour HTTP/$1.1$~~
* ~~En $1999$: [RFC 2616](https://datatracker.ietf.org/doc/html/rfc2616) pour HTTP/$1.1$ (rend obsolète la RFC 2068)~~
* En $2014$: [RFC 7230](https://datatracker.ietf.org/doc/html/rfc7230) à [7237](https://datatracker.ietf.org/doc/html/rfc7237)  pour HTTP/$1.1$ (rend obsolètes les RFC 2145 et 2616) 
* En $2015$: [RFC 7540](https://datatracker.ietf.org/doc/html/rfc7540) pour HTTP/$2.0$

