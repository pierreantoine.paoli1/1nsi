# 1NSI : Réseaux - Gouvernance Internet

## L'ICANN ($\ge 1998$) & L'IANA

<clear></clear>

<div style="float:right; width:30%;">

<figure>

<img src="../img/ICANN.jpg">

<figcaption>

Le siège de l'ICANN,  
à Playa Vista, Californie

</figcaption>

</figure>

</div>

Historiquement, depuis les origines d'Internet jusqu'aux années $1990$, c'était l’<bred>[IANA](https://fr.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority) - Internet Assigned Numbers Authority</bred> qui a été l'autorité suprême, à but non lucratif, de régulation d'Internet, dans le cadre d'un contrat avec le **United States Department of Commerce** du gouvernement fédéral américain.  
Depuis $1998$, l'IANA est géré par l'<bred>[ICANN](https://fr.wikipedia.org/wiki/Internet_Corporation_for_Assigned_Names_and_Numbers) - Internet Corporation for Assigned Names and Numbers</bred> (:fa-globe: [icann.org](https://www.icann.org/)) qui est devenu l'autorité suprême, à but non lucratif, de régulation d'Internet. La gestion de l'IANA par l'ICANN a été :
<clear></clear>

* directe de $1998$ à $2016$, puis
* indirecte depuis $2016$: c'est la société <bred>PTI - Public Technical Identifiers</bred> qui est affiliée à l'ICANN qui gère l'IANA encore à ce jour

Les activités principales de l'**ICANN** (depuis $1998$) consistent en :

### Administrer les ressources numériques d'Internet

Voici une liste de ressources gérées par l'ICANN :

#### Gérer les **adresses IP** 

(on dit l'**adressage IP**)

#### les **noms de domaines de premier niveau** :fr: ou <bred>TLD - Top Level Domain</bred> :gb:

* les **noms de domaines de premier niveau** (cf. liste [^1]) :fr: ou <bred>TLD - Top Level Domain</bred> :gb: :
    <img src="../img/TLD.svg"/> 
    * noms de **domaines de premier niveau générique** (cf. liste [^2] $^{,}$ [^3]) :fr: ou <bred>gTLD - Generic Top Level Domain</bred> :gb: : Pour cela, l'ICANN passe des accords de délégation avec des sociétés et organismes, commerciaux ou pas, dont par exemple:
        * **Verisign** (anciennement Networks Solutions Inc. - NSI ::) pour les gTLD `.com`, `.net` ($170,6$ millions de noms de domaines, fin $2021$ [^4] ) mais aussi `.cc`, `.edu`, `.name`, `.tv`, etc..
        * **PIR - Public Interest Registry** :gb: pour le gTLD `.org`
        * les compagnies en charge des nouveaux gTLD non sponsorisés `.biz`, `.info`, `.name`,
        * les sponsors des gTLDs sponsorisés `.aero`, `.coop`, `.museum`
    * noms de **domaines par code de pays de premier niveau** :fr: ou <bred>ccTLD - Country Code Top Level Domain</bred> :gb: : `.fr`, `.es`, ...
    * des <bred>RIR - Registres Internet Régionaux</bred> :fr: ou <bred>Regional internet Registry</bred> :gb: pour des grandes zones géographiques, qui délèguent quant à eux à des <bred>LIR - Local Internet Registry</bred> :gb: ou <bred>Registre Internet Local</bred> :fr:
    * Plus de $150$ <bred>Bureaux d'Enregistrements</bred> ou <bred>Registraires</bred> :fr: ou <bred>Registrar</bred> :gb: officiellement accrédités directement auprès de l'ICANN (cf liste [^5]), pour enregistrer les TLDs, génériques ou nationaux. Leurs accréditations sont renouvelables tous les $5$ ans. Ce sont des sociétés commerciales (ou pas) auprès desquelles le client final peut louer des **noms de domaines**. Quelques exemples:
        * En France : **AFNIC** pour le `.fr`, **OVH**, **Gandi**, **GoDaddy**, etc..
        * En Allemagne : **IONOS (by 1&1)** en Allemagne, ..
        * En Espagne : **Red.es** pour le `.es`, ..
        * aux USA : **name.com**, **domain.com**, **dreamhost.com**, **internet.bs**, **Go Daddy**,..

#### Serveurs Racines DNS

Assurer les fonctions de gestion du système de **serveurs racines du DNS**, qui sont des tables de conversion des **noms de domaines** en **adresses IP**, et réciproquement.

#### Gérer les **numéros d'Autonomous Systems (ASN)**

Gérer les **numéros d'Autonomous Systems (ASN)** pour le routage entre opérateurs.

#### gérer les **numéros de protocoles** 

Gérer les **numéros de protocoles** de nombreux protocoles différents sur IP, tels que **ARP**, **DHCP**, **EAP**, **HTTP**, **ICMP**, **IMAP**, **LDAP** ou encore **BGP**.

#### gérer les **numéros de ports TCP** et **UDP**

Cette liste est reprise par les différents systèmes d'exploitation (Windows, Mac Os, Unix5, Linux, etc.). 

### Coordonner les **acteurs techniques**

* l'<bred>[IETF](https://fr.wikipedia.org/wiki/Internet_Engineering_Task_Force) - Internet Engineering Task Force</bred> ($\ge 1986$) (Cf Présentation [^6]) :gb: produit, maintient et promeut la plupart des **Standards Internet** de communication, en particulier les standards qui composent la suite de **Protocoles Internet (TCP/IP)**. Le but du groupe est généralement la rédaction d'une ou plusieurs <bred>RFC - Request For Comments</bred> :gb: (cf. [wikipedia RFC](https://fr.wikipedia.org/wiki/Request_for_comments)), nom donné aux documents de spécification à la base d’Internet. Peu de RFC sont des standards, mais tous les documents publiés par l'IETF sont des RFC. Sa mission : *"to make the Internet work better"* est détaillée dans la RFC 3935[^7]. Ce sont des individus, pas des compagnies: *"We reject kings, presidents and voting. We believe in rough consensus and running code"* (Dave Clark, $1992$). Les Rôles et Champs d'applications de l'IETF sont divers:

    * IP, TCP, courriel, routage, IPsec, HTTP, FTP, ssh, LDAP, Peer2Peer
    * SIP, mobile IP, ppp, RADIUS, Kerberos, courriel sécurisé, 
    * Streaming video & audio, ... mais aussi: 
    * MPLS, GMPLS, pwe3, VPN, ...

    L'IETF n'est pas une entité légale à proprement parler, ce sont des activités organisées par l'<bred>ISOC - Internet SOCiety</bred> ($\ge 1992$) :gb:, association à but non lucratif, non-gouvernementale et internationale, de plusieurs dizianes de milliers de membres individuels, ainsi qu'une centaine de membres corporatifs. Elle sert fondamentalement à fournir une couverture légale à l'IETF.

* l'<bred>[IEEE](https://fr.wikipedia.org/wiki/Institute_of_Electrical_and_Electronics_Engineers) - Institute of Electrical and Electronics Engineers</bred> ($\ge 1963$) :gb: est une association de droit américain, à but non lucratif, regroupant des centaines de milliers de professionnels du domaine de l'informatique et des télécommunications à travers le monde. Son objectif principal se résume à la promotion de la connaissance dans le domaine de l'ingénierie électrique et électronique. **L'IEEE joue un rôle très important dans l'établissement de normes**. Ceci est fait par la **IEEE Standards Association**. Elle assure la publication de ses propres normes et des autres textes rédigés par des membres de son organisation, dont par exemple, parmi les plus connues:

    * IEEE 754 : représentation décimale des nombres en virgule flottante
    * IEEE 802: LAN en haut débit.
        * 802.1: Gestion des Réseaux Locaux
        * 802.11: Gestion des Réseaux Locaux Wifi (Sans Fil)
        * 802.15: Bluetooth, ZigBee
    * IEEE 1284: Port Parallèle
    * IEEE 1394: Bus Série (Firewire)
    * IEEE P1901: CPL - Courant Porteur de Ligne

    Certains Critiquent la position de l'IEEE et l'envisagent comme abus de position dominante.

* le <bred>[W3C](https://fr.wikipedia.org/wiki/World_Wide_Web_Consortium) - World Wide Web Consortium</bred> ($\ge 19994$) :gb: est un consortium international, à but non lucratif, créé par *Tim Berners-Lee*, qui développe et maintient des standards pour le (World Wide) Web et ses technologies (non exhaustivement: HTML, XML, CSS, PNG, SVG, MathML, etc..)

### La question est aussi Politique

La question semble simplement très technique, mais elle est aussi très politique, en effet, il faut concilier les intérêts d’une multitude d’acteurs mondiaux, un peu comme à l’ONU… :

* Faut-il autoriser des adresses Internet en caractères cyrilliques ou chinois ? 
* Faut-il permettre l’usage d’accents ?
* Doit-on accepter la création d’adresses qui se terminent en «.xxx » renvoyant vers des sites *"interdits aux moins de $18$ ans"*?
* etc..

## Les RIR - Regional Internet Registry

Depuis les années $1990$ l'IANA (puis l'ICANN depuis $1998$) a découpé l'espace d'adressage IPv4 en $256$ blocs correspondant à des réseaux de zones géographiques. Chacun de ces blocs est libre, réservé, assigné dans le passé ou alloué à un <bred>RIR - Regional Internet Registry</bred> :gb: ou <bred>Registre Internet Régional</bred> :fr:

<figure>

<img src="../img/RIR.svg">

<figcaption>

Répartition Géographique des  
différents Registres RIR

</figcaption>

</figure>

Il existe $5$ RIR, par ordre chronologique de date de création:

* **RIPE - NCC (Réseaux IP Européens - Network Coordination Centre)** (:fa-globe: [ripe.net](https://www.ripe.net/)): Créé en $1992$ , pour l'Europe et le Moyen-Orient
* **APNIC (Asia Pacific Network Information Center)**: Créé en $1993$ , pour l'Asie et le Pacifique
* **ARIN (American Registry for Internet Numbers)**: Créé en $1997$, pour l'Amérique du Nord (entre $1993$ et $1997$, ce rôle était attribué à **InterNIC**)
* **LACNIC (Latin America and Caribbean Network Information Center)**: Créé en $1999$ , pour l'Amérique latine et les îles des Caraïbes
* **AFRINIC (African Network Information Center)**: Créé en $2005$ , pour l'Afrique.

Les RIR se sont regroupés pour former la <bred>NRO - Number Resource Organisation</bred> afin de coordonner leurs activités ou projets communs et mieux défendre leurs intérêts auprès de l'**IANA** et de l'**ICANN**, mais aussi auprès des organismes normalisateurs (notamment l'**IETF** ou l'**ISOC**). 

## Les LIR - Local Internet Registry

À leur tour, les RIR distribuent des blocs d'adresses à des <bred>LIR - Local Internet Registries</bred> :gb: ou <bred>Registres Internet Locaux</bred> :fr: qui les distribuent aux utilisateurs finaux dans leur zone d'opération. Les LIR sont habituellement :

* des **opérateurs de réseau**
* des **fournisseurs d'accès Internet**
* des <bred>bureaux d'enregistrements</bred> ou <bred>registraires</bred> :fr: ou <bred>registrars</bred> :gb:
* Plus généralement, des (plutôt grandes) sociétés et organismes, commerciaux ou pas

!!! exp "de LIRs pour la France, l'Espagne, et aussi pour l'Europe"
    Les pages suivantes listent certains LIR proposant des services :

    * En France: [Liste des LIR en France](https://www.ripe.net/membership/indices/FR.html)
        * l'**AFNIC** (Network Information Center) gère le domaine `.fr`
    * En Espagne: [Liste des LIR en Espagne](https://www.ripe.net/membership/indices/ES.html)
        * l'**ESNIC**, dite **Red.es** gère le domaine `.es`
    * En Europe: [Liste des LIR par pays en Europe](https://www.ripe.net/participate/member-support/list-of-members/europe)

## Notes et Références

[^1]: source [IANA, Liste des TLD](https://data.iana.org/TLD/tlds-alpha-by-domain.txt)
[^2]: source [ICANN, ccTLDs](https://www.icann.org/resources/pages/cctlds-21-2012-02-25-en)
[^3]: source [ICANN, gTLDs Agreements](https://www.icann.org/en/registry-agreements?first-letter=a&sort-column=top-level-domain&sort-direction=asc&page=1)
[^4]: source [Verisign, Executive Summary](https://www.verisign.com/en_US/domain-names/dnib/index.xhtml)
[^5]: source [ICANN, Liste des 150 Registrars](https://www.icann.org/en/accredited-registrars?filter-letter=a&sort-direction=asc&sort-param=name&page=1)
[^6]: source [Présentation IETF, en Français](https://www.ietf.org/slides/slides-edu-newcomers-training-french-01.pdf)
[^7]: source [IETF, sa mission dans RFC 3935](https://datatracker.ietf.org/doc/rfc3935/)
