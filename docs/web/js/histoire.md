# 1NSI : Repères Historiques du JS

ECMAScript est noté ES

<center>

| Version | Date | Nouveautés |
|:-:|:-:|:-:|
| ES$1$ | $1997$ | 1ère Edition: `var`, etc.. |
| ES$5$ | $2009$ | var, Callbacks |
| ES$6$ | $2015$ | Gros Changement<br/>`let`, `const`, fonctions flèchées `=>`, promesses |
| ES$8$ | $2017$ | `async`, `await` |

</center>
